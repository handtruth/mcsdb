import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode

kotlin {
    explicitApi = ExplicitApiMode.Strict
}

dependencies {
    commonMainApi(project(":mcsdb-orm"))
    commonMainApi(project(":mcsdb-static-api"))
    jvmMainImplementation(kotlin("reflect"))
    commonTestApi(project(":mcsdb-test-schema"))
}

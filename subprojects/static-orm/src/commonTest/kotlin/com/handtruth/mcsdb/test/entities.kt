package com.handtruth.mcsdb.test

import com.handtruth.mcsdb.orm.*

@Entity(GroupEvent::class)
class Group : Shadow() {
    @Entity.Property
    val name: String by property()
    @Entity.Property(GroupDescriptionEvent::class)
    val description: String by property()
    @Entity.Property(GroupMemberEvent::class)
    val members: ShadowSet<User> by property()
    @Entity.Property(GroupOwnerEvent::class)
    val owner: User? by property()

    companion object : ShadowCompanion<Group>()
}

@Entity(UserEvent::class)
class User : Shadow() {
    @Entity.Property(UserEvent::class)
    val name: String by property()
    @Entity.Property
    val groups: ShadowSet<Group> by property()

    companion object : ShadowCompanion<User>()
}

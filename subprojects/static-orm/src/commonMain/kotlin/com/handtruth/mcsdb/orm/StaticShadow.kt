package com.handtruth.mcsdb.orm

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

internal expect object Properties : ReadOnlyProperty<Shadow, Any?> {
    override fun getValue(thisRef: Shadow, property: KProperty<*>): Any?
}

public fun <S : Shadow, V> Shadow.property(): ReadOnlyProperty<S, V> {
    @Suppress("UNCHECKED_CAST")
    return Properties as ReadOnlyProperty<S, V>
}

public abstract class ShadowCompanion<S : Shadow> {
    public open val shadowClass: KClass<S> by lazy { getShadowClass(this) }
    public open val factory: Shadow.Factory<S> by lazy { getShadowFactory(this) }
    protected open fun FormBuilderContext.describe() {}
    internal fun FormBuilderContext.invokeDescribe() {
        describe()
    }
    public open val lazyForm: Lazy<Form> = lazy { buildForm(this) }
    public val form: Form get() = lazyForm.value
}

public expect fun <S : Shadow> shadowCompanionOrNull(shadowClass: KClass<S>): ShadowCompanion<S>?

public fun <S : Shadow> shadowCompanion(shadowClass: KClass<S>): ShadowCompanion<S> {
    return shadowCompanionOrNull(shadowClass) ?: error("no shadow companion for $shadowClass")
}

public inline fun <reified S : Shadow> shadowCompanionOrNull(): ShadowCompanion<S>? {
    return shadowCompanionOrNull(S::class)
}

public inline fun <reified S : Shadow> shadowCompanion(): ShadowCompanion<S> {
    return shadowCompanion(S::class)
}

internal expect fun <S : Shadow> getShadowClass(companion: ShadowCompanion<S>): KClass<S>

internal expect fun <S : Shadow> getShadowFactory(companion: ShadowCompanion<S>): Shadow.Factory<S>

internal expect fun <S : Shadow> buildForm(companion: ShadowCompanion<S>): Form

package com.handtruth.mcsdb.context

import com.handtruth.mcsdb.orm.InterfaceProjection
import com.handtruth.mcsdb.orm.Shadow
import com.handtruth.mcsdb.orm.ShadowCompanion
import kotlin.reflect.KClass

public suspend fun <S : Shadow> ShadowCompanion<S>.selectOrNull(projection: InterfaceProjection): S? {
    val shadows = getShadows()
    @Suppress("UNCHECKED_CAST")
    return shadows.selectOrNull(form, projection) as S?
}

public suspend fun <S : Shadow> ShadowCompanion<S>.select(projection: InterfaceProjection): S {
    val shadows = getShadows()
    @Suppress("UNCHECKED_CAST")
    return shadows.select(form, projection) as S
}

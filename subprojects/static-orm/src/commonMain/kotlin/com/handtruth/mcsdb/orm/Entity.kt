package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.time.Event
import kotlin.reflect.KClass

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
public annotation class Entity(
    val mainstay: KClass<out Event>,
    val factory: KClass<out Shadow.Factory<*>> = Shadow.Factory::class
) {
    @Retention(AnnotationRetention.RUNTIME)
    @Target(AnnotationTarget.PROPERTY)
    @MustBeDocumented
    public annotation class Property(
        val matter: KClass<out Event> = Event::class,
        val field: String = "",
        val required: Boolean = true
    )
}

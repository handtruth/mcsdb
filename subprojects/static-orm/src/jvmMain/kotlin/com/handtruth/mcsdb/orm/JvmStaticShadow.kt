package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.schema.*
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.EventCompanion
import com.handtruth.mcsdb.util.decodeFromArbitrary
import com.handtruth.mcsdb.util.decodeFromStructure
import com.handtruth.mcsdb.util.outer
import kotlinx.serialization.serializer
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty
import kotlin.reflect.full.*
import kotlin.reflect.jvm.jvmErasure

internal actual object Properties : ReadOnlyProperty<Shadow, Any?> {
    actual override fun getValue(thisRef: Shadow, property: KProperty<*>): Any? {
        val member = thisRef.properties[property.name]
        val memberProperty = member.property
        val value = member.value
        if (value != null && memberProperty is Form.Property.Attribute) {
            val type = memberProperty.member.field.type
            when (type as Types) {
                Types.Structure -> {
                    val returnType = property.returnType
                    val returnClass = returnType.jvmErasure
                    if (returnClass != Any::class) {
                        val serializer = serializer(returnType)
                        return decodeFromStructure(serializer, value)
                    }
                }
                Types.Arbitrary -> {
                    val returnType = property.returnType
                    val returnClass = returnType.jvmErasure
                    if (returnClass != ByteArray::class) {
                        val serializer = serializer(returnType)
                        return decodeFromArbitrary(serializer, value as ByteArray)
                    }
                }
                else -> {
                }
            }
        }
        return value
    }
}

public actual fun <S : Shadow> shadowCompanionOrNull(shadowClass: KClass<S>): ShadowCompanion<S>? {
    val companion = shadowClass.companionObjectInstance as? ShadowCompanion<*>
    @Suppress("UNCHECKED_CAST")
    return companion as ShadowCompanion<S>?
}

internal actual fun <S : Shadow> getShadowClass(companion: ShadowCompanion<S>): KClass<S> {
    val companionClass = companion::class
    val shadowClass = companionClass.outer
    checkSchema(shadowClass.isSubclassOf(Shadow::class)) { "$shadowClass is not a shadow class" }
    @Suppress("UNCHECKED_CAST")
    return shadowClass as KClass<S>
}

internal class SimpleStaticShadowFactory<S : Shadow>(private val constructor: KFunction<S>) : Shadow.Factory<S> {
    override fun create(): S {
        return constructor.callBy(emptyMap())
    }
}

internal actual fun <S : Shadow> getShadowFactory(companion: ShadowCompanion<S>): Shadow.Factory<S> {
    val shadowClass = companion.shadowClass
    val proposedFactoryClass = shadowClass.findAnnotation<Entity>()?.factory
    if (proposedFactoryClass != null && proposedFactoryClass != Shadow.Factory::class) {
        val factory = proposedFactoryClass.objectInstance
            ?: (proposedFactoryClass.primaryConstructor?.callBy(emptyMap())
                ?: error("$proposedFactoryClass should have no argument primary constructor"))
        @Suppress("UNCHECKED_CAST")
        return factory as Shadow.Factory<S>
    } else {
        val constructor = shadowClass.primaryConstructor ?: error("$shadowClass should have primary constructor")
        return SimpleStaticShadowFactory(constructor)
    }
}

private fun getDescription(eventClass: KClass<out Event>): EventDescription {
    val eventCompanion = eventClass.companionObjectInstance as? EventCompanion
        ?: error("mainstay event companion object not found")
    return eventCompanion.description
}

private fun componentProperty(
    propertyName: String,
    propertyClass: KClass<*>,
    eventClass: KClass<out Event>,
    variant: Form.Property.Variants
): Form.Property {
    @Suppress("UNCHECKED_CAST")
    propertyClass as KClass<out Shadow>
    val propertyShadowCompanion = shadowCompanion(propertyClass)
    val filter = filter {
        if (eventClass != Event::class) {
            attached(getDescription(eventClass))
        } else {
            U
        }
    }
    return Form.Property.Component(propertyName, variant, propertyShadowCompanion.lazyForm, filter)
}

internal actual fun <S : Shadow> buildForm(companion: ShadowCompanion<S>): Form {
    val shadowClass = companion.shadowClass
    val mainstay = shadowClass.findAnnotation<Entity>()?.mainstay
        ?: error("Entity annotation not declared for shadow $shadowClass")
    val mainstayDescription = getDescription(mainstay)
    return form(mainstayDescription) {
        factory = companion.factory
        val shadowProperties = shadowClass.memberProperties
        for (shadowProperty in shadowProperties) {
            val annotation = shadowProperty.findAnnotation<Entity.Property>() ?: continue
            val propertyType = shadowProperty.returnType
            val propertyName = shadowProperty.name
            val propertyNullable = propertyType.isMarkedNullable
            val eventClass = annotation.matter
            when (val propertyClass = propertyType.jvmErasure) {
                Iterable::class, Collection::class, Set::class, ShadowSet::class -> {
                    // collections here
                    check(!propertyNullable) {
                        "there is no any sense to make collection type nullable (property \"$propertyClass\")"
                    }
                    val elementType = propertyType.arguments.first().type
                        ?: error("set type should be declared for property $propertyName")
                    val elementClass = elementType.jvmErasure
                    val isNullable = elementType.isMarkedNullable
                    if (elementClass.isSubclassOf(Shadow::class)) {
                        check(!isNullable) {
                            "there is no any sense to make elements of shadow set nullable (property \"$propertyClass\")"
                        }
                        val variant = Form.Property.Variants.Multivalued
                        properties += componentProperty(propertyName, propertyClass, eventClass, variant)
                    } else {
                        when (propertyClass) {
                            ShadowSet::class, Set::class ->
                                error("attributes are not elements of ShadowSet type (property \"$propertyClass\")")
                        }
                        val mcsdbType = kTypeToMcsdbType(elementType)
                        val description = if (eventClass == Event::class) {
                            mainstayDescription
                        } else {
                            getDescription(eventClass)
                        }
                        val member = if (annotation.field.isEmpty()) {
                            val field = description.allFields.find {
                                it.type == mcsdbType && isNullable == (Modifier.Nullable in it.modifiers)
                            } ?: error(
                                "unable to guess field name, because particle does " +
                                        "not have field with such type (property \"$propertyClass\")"
                            )
                            Member(description, field)
                        } else {
                            val member = description[annotation.field]
                            check(member.field.type == mcsdbType) {
                                "field type does not match declared property type (property \"$propertyClass\")"
                            }
                            member
                        }
                        check(Modifier.Nullable in member.field.modifiers == isNullable) {
                            "element collection nullability must be the same as " +
                                    "particle field nullability (property \"$propertyClass\")"
                        }
                        property(propertyName, member, Form.Property.Variants.Multivalued)
                    }
                }
                else -> {
                    if (propertyClass.isSubclassOf(Shadow::class)) {
                        val variant = if (propertyNullable) {
                            Form.Property.Variants.Optional
                        } else {
                            Form.Property.Variants.Required
                        }
                        properties += componentProperty(propertyName, propertyClass, eventClass, variant)
                    } else {
                        val mcsdbType = kTypeToMcsdbType(propertyType)
                        val description = if (eventClass == Event::class) {
                            mainstayDescription
                        } else {
                            getDescription(eventClass)
                        }
                        val member = if (annotation.field.isEmpty()) {
                            val field = description.allFields.find {
                                it.type == mcsdbType
                            } ?: error(
                                "unable to guess field name, because particle does " +
                                        "not have field with such type (property \"$propertyClass\")"
                            )
                            Member(description, field)
                        } else {
                            val member = description[annotation.field]
                            check(member.field.type == mcsdbType) {
                                "field type does not match declared property type (property \"$propertyClass\")"
                            }
                            member
                        }
                        val variant = if (propertyNullable) {
                            if (Modifier.Nullable in member.field.modifiers) {
                                if (annotation.required) {
                                    Form.Property.Variants.Required
                                } else {
                                    Form.Property.Variants.Optional
                                }
                            } else {
                                Form.Property.Variants.Optional
                            }
                        } else {
                            Form.Property.Variants.Required
                        }
                        property(propertyName, member, variant)
                    }
                }
            }
        }
        with(companion) {
            invokeDescribe()
        }
    }
}

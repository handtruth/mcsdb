import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode

plugins {
    id("kotlinx-atomicfu")
}

kotlin {
    explicitApi = ExplicitApiMode.Strict
}

dependencies {
    commonMainApi(project(":mcsdb-api"))
    commonTestImplementation(project(":mcsdb-test-schema"))

    val atomicfuVersion: String by project
    commonMainImplementation("org.jetbrains.kotlinx:atomicfu:$atomicfuVersion")
}

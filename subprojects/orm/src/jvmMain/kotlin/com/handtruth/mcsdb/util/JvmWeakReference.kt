package com.handtruth.mcsdb.util

import java.lang.ref.WeakReference as JWeakReference

internal actual open class WeakReference<T> actual constructor(value: T) : JWeakReference<T>(value)

package com.handtruth.mcsdb.orm

import com.handtruth.mc.types.Dynamic
import com.handtruth.mc.types.buildDynamic
import com.handtruth.mc.types.contentDeepEquals
import com.handtruth.mc.types.contentDeepHashCode
import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.Member
import com.handtruth.mcsdb.schema.Types
import com.handtruth.mcsdb.space.Particle
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.dataOf
import com.handtruth.mcsdb.time.descriptionOf

public data class InterfaceProjection internal constructor(
    val description: EventDescription,
    val payload: List<Any?>
) {
    init {
        require(payload.size == description.allRealFields.size)
    }

    internal val filter: Filter
        get() = filter {
            val fields = description.allRealFields
            fields.foldIndexed(source(description) as Filter) { i, acc, field ->
                acc and (Member(description, field) eq payload[i])
            }
        }

    public constructor(description: EventDescription, data: Dynamic) : this(
        description,
        description.allRealFields.map { data.getOrNull(it.name) }
    )

    override fun equals(other: Any?): Boolean {
        if (other === this) {
            return true
        }
        if (other !is InterfaceProjection) {
            return false
        }
        if (other.description != description) {
            return false
        }
        for (i in description.allRealFields.indices) {
            val field = description.allRealFields[i]
            val a = payload[i]
            val b = other.payload[i]
            if (a != b) {
                val compare = when (field.type as Types) {
                    Types.Structure ->
                        (buildDynamic { "value" assign a }).contentDeepEquals(buildDynamic { "value" assign b })
                    Types.Arbitrary -> (a as ByteArray?) contentEquals (b as ByteArray?)
                    else -> false
                }
                if (!compare) {
                    return false
                }
            }
        }
        return true
    }

    override fun hashCode(): Int = description.hashCode() xor description.allRealFields.indices.sumBy { i ->
        val field = description.allRealFields[i]
        val value = payload[i] ?: return@sumBy 0
        when (field.type as Types) {
            Types.Structure -> buildDynamic { "value" assign value }.contentDeepHashCode()
            Types.Arbitrary -> (value as ByteArray).contentHashCode()
            else -> value.hashCode() + 1
        }
    }
}

private fun EventDescription.projectionOf(description: EventDescription, data: Dynamic): InterfaceProjection {
    require(description.isAttachedTo(this)) { "datum should be attached to the specified interface" }
    return InterfaceProjection(this, data)
}

public fun EventDescription.projectionOf(event: Event): InterfaceProjection {
    val description = descriptionOf(event)
    val data = dataOf(event)
    return projectionOf(description, data)
}

public fun EventDescription.projectionOf(particle: Particle): InterfaceProjection {
    return projectionOf(particle.description, particle.data)
}

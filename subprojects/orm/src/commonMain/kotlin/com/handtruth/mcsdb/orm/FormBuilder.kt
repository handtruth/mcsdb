package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.Member
import kotlinx.collections.immutable.toImmutableList

internal class PropertyCollection : AbstractMutableCollection<Form.Property>() {
    private val properties = hashMapOf<String, Form.Property>()

    override val size: Int by properties::size

    override fun contains(element: Form.Property): Boolean {
        val contained = properties[element.name]
        return element == contained
    }

    override fun isEmpty(): Boolean = properties.isEmpty()

    override fun add(element: Form.Property): Boolean {
        val old = properties.put(element.name, element)
        return old != element
    }

    override fun clear() {
        properties.clear()
    }

    override fun iterator(): MutableIterator<Form.Property> = properties.values.iterator()

    override fun remove(element: Form.Property): Boolean {
        val old = properties.remove(element.name)
        return if (old == element) {
            true
        } else {
            properties[element.name] = old!!
            false
        }
    }
}

private object DefaultShadowFactory : Shadow.Factory<Shadow> {
    override fun create() = Shadow()
}

public class FormBuilderContext(public val mainstay: EventDescription) {
    public val properties: MutableCollection<Form.Property> = PropertyCollection()
    public var factory: Shadow.Factory<*> = DefaultShadowFactory

    public fun property(
        name: String,
        member: Member,
        variant: Form.Property.Variants,
        filter: Filter = Filter.Universe
    ): Form.Property.Attribute {
        val property = Form.Property.Attribute(name, variant, member, filter)
        properties += property
        return property
    }

    public inline fun property(
        name: String,
        member: Member,
        variant: Form.Property.Variants,
        expression: FilterBuilderContext.() -> Filter
    ): Form.Property.Attribute {
        return property(name, member, variant, filter(expression))
    }

    public fun property(
        name: String,
        form: Form,
        variant: Form.Property.Variants,
        filter: Filter = Filter.Universe
    ): Form.Property.Component {
        val property = Form.Property.Component(name, variant, form, filter)
        properties += property
        return property
    }

    public inline fun property(
        name: String,
        form: Form,
        variant: Form.Property.Variants,
        expression: FilterBuilderContext.() -> Filter
    ): Form.Property.Component {
        return property(name, form, variant, filter(expression))
    }

    public fun property(
        name: String,
        form: Lazy<Form>,
        variant: Form.Property.Variants,
        filter: Filter = Filter.Universe
    ): Form.Property.Component {
        val property = Form.Property.Component(name, variant, form, filter)
        properties += property
        return property
    }

    public inline fun property(
        name: String,
        form: Lazy<Form>,
        variant: Form.Property.Variants,
        expression: FilterBuilderContext.() -> Filter
    ): Form.Property.Component {
        return property(name, form, variant, filter(expression))
    }

    public infix fun String.required(member: Member): Form.Property.Attribute {
        return property(this, member, Form.Property.Variants.Required)
    }

    public infix fun String.required(form: Form): Form.Property.Component {
        return property(this, form, Form.Property.Variants.Required)
    }

    public infix fun String.required(form: Lazy<Form>): Form.Property.Component {
        return property(this, form, Form.Property.Variants.Required)
    }

    public infix fun String.required(form: () -> Form): Form.Property.Component {
        return property(this, lazy(LazyThreadSafetyMode.PUBLICATION, form), Form.Property.Variants.Required)
    }

    public infix fun String.optional(member: Member): Form.Property.Attribute {
        return property(this, member, Form.Property.Variants.Optional)
    }

    public infix fun String.optional(form: Form): Form.Property.Component {
        return property(this, form, Form.Property.Variants.Optional)
    }

    public infix fun String.optional(form: Lazy<Form>): Form.Property.Component {
        return property(this, form, Form.Property.Variants.Optional)
    }

    public infix fun String.optional(form: () -> Form): Form.Property.Component {
        return property(this, lazy(LazyThreadSafetyMode.PUBLICATION, form), Form.Property.Variants.Optional)
    }

    public infix fun String.multivalued(member: Member): Form.Property.Attribute {
        return property(this, member, Form.Property.Variants.Multivalued)
    }

    public infix fun String.multivalued(form: Form): Form.Property.Component {
        return property(this, form, Form.Property.Variants.Multivalued)
    }

    public infix fun String.multivalued(form: Lazy<Form>): Form.Property.Component {
        return property(this, form, Form.Property.Variants.Multivalued)
    }

    public infix fun String.multivalued(form: () -> Form): Form.Property.Component {
        return property(this, lazy(LazyThreadSafetyMode.PUBLICATION, form), Form.Property.Variants.Multivalued)
    }

    public inline infix fun Form.Property.Attribute.filter(
        expression: FilterBuilderContext.() -> Filter
    ): Form.Property.Attribute {
        properties.remove(this)
        return property(name, member, variant, FilterBuilderContext.expression())
    }

    public inline infix fun Form.Property.Component.filter(
        expression: FilterBuilderContext.() -> Filter
    ): Form.Property.Component {
        properties.remove(this)
        return property(name, form, variant, FilterBuilderContext.expression())
    }

    public fun build(): Form {
        return Form(mainstay, properties.toImmutableList(), factory)
    }
}

public inline fun form(connection: EventDescription, block: FormBuilderContext.() -> Unit): Form {
    return FormBuilderContext(connection).apply(block).build()
}

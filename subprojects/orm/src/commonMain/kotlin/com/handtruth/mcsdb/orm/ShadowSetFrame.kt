package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.Filter
import kotlin.coroutines.CoroutineContext

internal class ShadowSetFrame internal constructor(
    resource: ShadowSetResource,
    filter: Filter,
    coroutineContext: CoroutineContext
) : Frame<ShadowSetFrame, ShadowSet<*>>(resource, coroutineContext) {

}

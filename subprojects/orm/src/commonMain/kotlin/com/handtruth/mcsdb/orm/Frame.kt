package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.util.Cleanable
import com.handtruth.mcsdb.util.getValue
import com.handtruth.mcsdb.util.weak
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

internal abstract class Frame<F : Frame<F, S>, S>(
    resource: Resource<F, S>,
    coroutineContext: CoroutineContext
) : CoroutineScope {

    init {
        @Suppress("UNCHECKED_CAST")
        resource.provideFrame(this as F)
    }

    override val coroutineContext = coroutineContext + Job(coroutineContext[Job])

    internal val resource by weak(resource)

    val shadow: S? get() = resource?.shadow

    internal fun cleaner(block: Runnable): Cleanable = Cleaner(block)

    private inner class Cleaner(private val block: Runnable) : Cleanable {
        override val requireClean: Boolean = resource === null

        override fun clean() {
            cancel()
            block.run()
        }
    }
}

package com.handtruth.mcsdb.orm

import com.handtruth.mc.types.get
import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.context.ShadowContext
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.space.ask
import com.handtruth.mcsdb.time.dataOf
import com.handtruth.mcsdb.time.infoOf
import com.handtruth.mcsdb.time.listen
import com.handtruth.mcsdb.time.succeeded
import com.handtruth.mcsdb.util.ResourceManager
import com.handtruth.mcsdb.variability.State
import kotlinx.atomicfu.locks.SynchronizedObject
import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext
import kotlinx.atomicfu.locks.synchronized
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.first

public class Shadows internal constructor(
    public val database: Database,
    public val state: State,
    override val coroutineContext: CoroutineContext
) : CoroutineScope {
    private val context = ShadowContext(database, state, this)

    private val repositories = hashMapOf<Form, Repository>()

    private val repositoriesLock = SynchronizedObject()

    private fun repositoryOf(form: Form): Repository = synchronized(repositoriesLock) {
        repositories.getOrPut(form) { Repository(form, coroutineContext + Job(coroutineContext[Job])) }
    }

    internal fun getFrameResource(form: Form, projection: InterfaceProjection): ShadowResource {
        require(form.mainstay == projection.description) {
            "projected interface type should be the same for form"
        }
        return repositoryOf(form).get(projection)
    }

    public suspend fun selectOrNull(form: Form, projection: InterfaceProjection): Shadow? {
        while (true) {
            val resource = getFrameResource(form, projection)
            val frame = resource.frame
            val actual = frame.awaitStage(Stage.Synchronised)
            if (actual > Stage.Destroyed || !frame.exists) {
                return null
            }
            val constructed = frame.awaitStage(Stage.Constructed)
            if (constructed == Stage.Constructed) {
                frame.shadow!!
            } else {
                return null
            }
        }
    }

    public suspend fun select(form: Form, projection: InterfaceProjection): Shadow {
        while (true) {
            val resource = getFrameResource(form, projection)
            val frame = resource.frame
            when (frame.awaitStage(Stage.Constructed)) {
                Stage.Constructed -> return frame.shadow!!
                Stage.Destroyed -> {
                    val destroyed = frame.stage.first {
                        it == Stage.Constructed || it == Stage.Disposed
                    }
                    when (destroyed) {
                        Stage.Constructed -> frame.shadow!!
                        else -> error("unreachable")
                    }
                }
                else -> error("unreachable")
            }
        }
    }

    public suspend fun selectMultiple(form: Form, filter: Filter): ShadowSet<*> {
        TODO()
    }

    internal inner class Repository(val form: Form, override val coroutineContext: CoroutineContext) : CoroutineScope {
        val manager get() = this@Shadows

        private val frames = hashMapOf<InterfaceProjection, ShadowFrame>()

        private val framesLock = SynchronizedObject()

        fun get(projection: InterfaceProjection): ShadowResource {
            val newResource = synchronized(framesLock) {
                val oldFrame = frames[projection]
                if (oldFrame != null) {
                    @Suppress("UNUSED_VARIABLE") // keep reference
                    val resource = oldFrame.resource
                    if (resource !== null) {
                        return resource
                    }
                }
                val resource = ShadowResource()
                val newFrame = ShadowFrame(
                    resource = resource,
                    repository = this,
                    projection = projection,
                    coroutineContext = coroutineContext + Job(coroutineContext[Job])
                )
                frames[projection] = newFrame
                resource
            }
            val newFrame = newResource.frame
            newFrame.coroutineContext[Job]!!.invokeOnCompletion {
                synchronized(framesLock) {
                    val frame = frames[projection]
                    if (frame === newFrame) {
                        frames.remove(projection)
                    }
                }
            }
            ResourceManager.register(newFrame.cleanable)
            return newResource
        }
    }

    public companion object
}

public suspend fun Shadows.selectOrNull(form: Form, filter: Filter): Shadow? {
    val particle = database.space.ask(state) {
        select { attached(form.mainstay) and filter }
    }.firstOrNull() ?: return null
    val projection = form.mainstay.projectionOf(particle)
    return selectOrNull(form, projection)
}

public suspend fun Shadows.select(form: Form, filter: Filter): Shadow {
    val event = database.time.listen(state, actual = true) {
        attached(form.mainstay) and filter
    }.first { event ->
        dataOf(event)["direction"] as Boolean && infoOf(event).succeeded
    }
    val projection = form.mainstay.projectionOf(event)
    return select(form, projection)
}

public suspend inline fun Shadows.selectOrNull(form: Form, expression: FilterBuilderContext.() -> Filter): Shadow? {
    return selectOrNull(form, filter(expression))
}

public suspend inline fun Shadows.select(form: Form, expression: FilterBuilderContext.() -> Filter): Shadow {
    return select(form, filter(expression))
}

public suspend inline fun Shadows.selectMultiple(
    form: Form,
    expression: FilterBuilderContext.() -> Filter
): ShadowSet<*> {
    return selectMultiple(form, filter(expression))
}

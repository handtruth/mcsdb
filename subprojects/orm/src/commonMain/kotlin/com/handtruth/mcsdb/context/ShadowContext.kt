package com.handtruth.mcsdb.context

import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.orm.*
import com.handtruth.mcsdb.variability.State
import kotlin.coroutines.coroutineContext

public interface ShadowContext : Context {
    public val shadows: Shadows
}

internal class ShadowContextImpl(
    override val database: Database,
    override val state: State,
    override val shadows: Shadows
) : ShadowContext {
    override fun toString(): String = "ShadowContext(database=$database, state=$state, shadowManager=$shadows)"
}

public fun ShadowContext(database: Database, state: State, shadows: Shadows): ShadowContext =
    ShadowContextImpl(database, state, shadows)

public suspend inline fun getShadowContext(): ShadowContext = coroutineContext[Context] as? ShadowContext
    ?: error("MCSDB shadow context not attached to the coroutine context")

public suspend inline fun getShadows(): Shadows = getShadowContext().shadows

public suspend fun Shadows.Companion.selectOrNull(form: Form, projection: InterfaceProjection): Shadow? {
    val shadows = getShadows()
    return shadows.selectOrNull(form, projection)
}

public suspend fun Shadows.Companion.select(form: Form, projection: InterfaceProjection): Shadow {
    val shadows = getShadows()
    return shadows.select(form, projection)
}

public suspend fun Shadows.Companion.selectOrNull(form: Form, filter: Filter): Shadow? {
    val shadows = getShadows()
    return shadows.selectOrNull(form, filter)
}

public suspend fun Shadows.Companion.select(form: Form, filter: Filter): Shadow {
    val shadows = getShadows()
    return shadows.select(form, filter)
}

public suspend fun Shadows.Companion.selectMultiple(form: Form, filter: Filter): ShadowSet<*> {
    val shadows = getShadows()
    return shadows.selectMultiple(form, filter)
}

public suspend inline fun Shadows.Companion.selectOrNull(
    form: Form,
    expression: FilterBuilderContext.() -> Filter
): Shadow? {
    return Shadows.selectOrNull(form, filter(expression))
}

public suspend inline fun Shadows.Companion.select(
    form: Form,
    expression: FilterBuilderContext.() -> Filter
): Shadow {
    return Shadows.select(form, filter(expression))
}

public suspend inline fun Shadows.Companion.selectMultiple(
    form: Form,
    expression: FilterBuilderContext.() -> Filter
): ShadowSet<*> {
    return Shadows.selectMultiple(form, filter(expression))
}

package com.handtruth.mcsdb.orm

import com.handtruth.mc.collections.CopyOnWriteSet
import com.handtruth.mc.types.get
import com.handtruth.mcsdb.space.ask
import com.handtruth.mcsdb.space.Particle
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.dataOf
import com.handtruth.mcsdb.time.toParticle
import com.handtruth.mcsdb.util.*
import com.handtruth.mcsdb.util.Cleanable
import com.handtruth.mcsdb.util.weak
import kotlinx.atomicfu.AtomicRef
import kotlinx.atomicfu.atomic
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.coroutines.CoroutineContext
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

internal class ShadowFrame internal constructor(
    resource: ShadowResource,
    internal val repository: Shadows.Repository,
    val projection: InterfaceProjection,
    coroutineContext: CoroutineContext
) : Frame<ShadowFrame, Shadow>(resource, coroutineContext) {
    init {
        resource.provideFrame(this)
    }

    val manager: Shadows get() = repository.manager

    val form: Form get() = repository.form

    private val privateStage = MutableStateFlow(Stage.Initiated)

    val stage: StateFlow<Stage> get() = privateStage

    suspend fun awaitStage(stage: Stage): Stage {
        return this.stage.first { it >= stage }
    }

    private val privateAttributesPrepared = MutableStateFlow(false)

    val attributesPrepared: StateFlow<Boolean> get() = privateAttributesPrepared

    internal val consistencyChanged = MutableSharedFlow<Unit>()

    val members: Shadow.Members = Shadow.Members(this)

    var exists: Boolean = false
        private set

    private val privateParticles = CopyOnWriteSet<Particle>()

    val particles: Set<Particle> get() = privateParticles

    val mainInterfaceExists: Boolean get() = privateParticles.isNotEmpty()

    private fun recalculateExistence() {
        exists = mainInterfaceExists && members.all {
            it.exists || it.property.variant != Form.Property.Variants.Required
        }
        val prepared = mainInterfaceExists && members.all {
            (it.property is Form.Property.Component && it.property.variant == Form.Property.Variants.Required)
                    || it.exists || it.property.variant != Form.Property.Variants.Required
        }
        privateAttributesPrepared.compareAndSet(!prepared, prepared)
    }

    private fun supply(particle: Particle, direction: Boolean) {
        if (particle.description.isAttachedTo(form.mainstay)) {
            if (direction) {
                privateParticles += particle
            } else {
                privateParticles -= particle
            }
        }
    }

    internal fun apply(event: Event) {
        val particle = event.toParticle()
        val data = dataOf(event)
        supply(particle, data["direction"] as Boolean)
        recalculateExistence()
    }

    private val constructMutex = Mutex()

    private suspend fun construct() {
        if (shadow != null) {
            return
        }
        constructMutex.withLock {
            if (shadow != null) {
                return
            }
            val resource = resource ?: run { return }
            _currentFrameResource.use(resource) {
                form.factory.create()
            }
            privateStage.compareAndSet(Stage.Synchronised, Stage.Constructed)
        }
    }

    init {
        launch {
            manager.database.space.ask(manager.state) {
                select { projection.filter and form.filter }
            }.forEach { particle ->
                supply(particle, direction = true)
            }
            update()
            TODO("fetch member shadows")
            if (!privateStage.compareAndSet(Stage.Initiated, Stage.Synchronised)) {
                error("unreachable")
            }
            TODO()
            if (exists) {
                privateStage.compareAndSet(Stage.Synchronised, Stage.Constructed)
            }
        }
    }
}

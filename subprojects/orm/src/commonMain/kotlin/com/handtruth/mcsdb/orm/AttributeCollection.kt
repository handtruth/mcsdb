package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.space.Particle

internal class AttributeCollection<E>(
    private val property: Form.Property.Attribute,
    private val particles: Set<Particle>
) : AbstractCollection<E>() {
    private val attributes = particles.asSequence().filter {
        it.description.isAttachedTo(property.member.description) && applyFilter(it, property.filter)
    }

    private val values = attributes.map {
        @Suppress("UNCHECKED_CAST")
        it.data.getOrNull(property.member.field.name) as E
    }

    override val size: Int get() = attributes.count()

    override fun contains(element: E): Boolean = values.any { it == element }

    override fun containsAll(elements: Collection<E>): Boolean = values.toSet().containsAll(elements)

    override fun isEmpty(): Boolean = particles.isEmpty()

    override fun iterator(): Iterator<E> = values.iterator()
}

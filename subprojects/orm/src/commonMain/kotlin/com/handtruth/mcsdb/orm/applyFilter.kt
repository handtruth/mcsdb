package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.space.Particle

internal fun applyFilter(particle: Particle, filter: Filter): Boolean {
    return when (filter) {
        Filter.Empty -> false
        Filter.Universe -> true
        is Filter.Role -> {
            val role = filter.role
            val description = filter.description
            if (role == null) {
                particle.description == description
            } else {
                particle.description.interfaces[description]?.first == role
            }
        }
        is Filter.Attached -> particle.description.isAttachedTo(filter.description)
        is Filter.Unary.Negation -> !applyFilter(particle, filter.filter)
        is Filter.Binary.Intersection -> applyFilter(particle, filter.a) && applyFilter(particle, filter.b)
        is Filter.Binary.Union -> applyFilter(particle, filter.a) || applyFilter(particle, filter.b)
        is Filter.Binary.Exception -> applyFilter(particle, filter.a) && !applyFilter(particle, filter.b)
        is Filter.Expression -> {
            val actual = particle.data.getOrNull(filter.member.field.name)
            when (filter) {
                is Filter.Expression.Equal -> actual == filter.value
                is Filter.Expression.NotEqual -> actual != filter.value
                is Filter.Expression.IsNull -> actual === null
                is Filter.Expression.IsNotNull -> actual !== null
            }
        }
        is Filter.Via -> true // impossible to resolve :(
    }
}

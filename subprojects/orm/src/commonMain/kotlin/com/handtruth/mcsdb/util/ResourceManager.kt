package com.handtruth.mcsdb.util

import kotlinx.atomicfu.atomic
import kotlinx.coroutines.channels.Channel

internal object ResourceManager {
    private val busy = atomic(false)
    private val channel = Channel<Cleanable>(Channel.UNLIMITED)

    fun proceed() {
        if (busy.getAndSet(true)) {
            return
        }
        try {
            val first: Cleanable
            while (true) {
                val next = channel.poll() ?: return
                if (next.requireClean) {
                    next.clean()
                } else {
                    first = next
                    break
                }
            }
            channel.offer(first)
            while (true) {
                val next = channel.poll()!!
                if (next === first) {
                    return
                }
                if (next.requireClean) {
                    next.clean()
                } else {
                    channel.offer(next)
                }
            }
        } finally {
            check(busy.getAndSet(false))
        }
    }

    fun register(cleanable: Cleanable) {
        channel.offer(cleanable)
    }
}

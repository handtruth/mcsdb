package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.util.ThreadLocal
import kotlinx.atomicfu.AtomicRef
import kotlinx.atomicfu.atomic
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

internal val _currentFrameResource = ThreadLocal<ShadowResource>()

internal val currentFrameResource
    get() = _currentFrameResource.get() ?: error("shadows must be created by MCSDB framework")

public open class Shadow {
    internal val resource = currentFrameResource

    init {
        resource.provideShadow(this)
    }

    public val properties: Members get() = resource.frame.members

    public fun interface Factory<S : Shadow> {
        public fun create(): S
    }

    public class Members internal constructor(frame: ShadowFrame) : Iterable<Members.Member> {
        private val membersByPosition = frame.form.properties.map { TODO() as Member }

        private val membersByName = membersByPosition.associateBy { it.name }

        public operator fun get(name: String): Member = membersByName[name] ?: error("no member \"$name\"")

        public operator fun get(index: Int): Member = membersByPosition[index]

        override fun iterator(): Iterator<Member> {
            return membersByPosition.iterator()
        }

        public sealed class Member : ReadOnlyProperty<Any?, Any?> {
            public abstract val property: Form.Property

            public val name: String get() = property.name

            public abstract val exists: Boolean

            public abstract val value: Any?

            final override fun getValue(thisRef: Any?, property: KProperty<*>): Any? = value

            internal open suspend fun construct() {}

            internal open fun update() {}

            internal class SingleAttribute(
                frame: ShadowFrame,
                override val property: Form.Property.Attribute
            ) : Member() {
                private val values = frame.particles.asSequence().filter {
                    it.description.isAttachedTo(property.member.description) && applyFilter(it, property.filter)
                }.map {
                    it.data.getOrNull(property.member.field.name)
                }

                override val exists get() = values.iterator().hasNext()

                override val value get() = if (property.variant == Form.Property.Variants.Required) {
                    values.first()
                } else {
                    values.firstOrNull()
                }
            }

            internal class MultivaluedAttribute(
                frame: ShadowFrame,
                override val property: Form.Property.Attribute
            ) : Member() {
                override val exists get() = true

                override val value = AttributeCollection<Any?>(property, frame.particles)
            }

            internal class SingleComponent(
                private val parent: ShadowFrame,
                override val property: Form.Property.Component
            ) : Member() {
                internal var frame: ShadowFrame? = null

                private val atomicShadow: AtomicRef<Shadow?> = atomic(null)

                override var value by atomicShadow::value
                    private set

                override val exists: Boolean get() = value != null

                private var projection: InterfaceProjection? = null

                override fun update() {
                    val newParticle = parent.particles.find {
                        it.description.isAttachedTo(property.form.mainstay) && applyFilter(it, property.filter)
                    }
                    val newProjection = newParticle?.let {
                        property.form.mainstay.projectionOf(newParticle)
                    }
                    if (projection != newProjection) {
                        val resource = parent.resource ?: return
                        val newFrameResource = parent.manager.getFrameResource(property.form, newProjection)
                        val oldFrame = frame
                        if (oldFrame !== null) {
                            resource.removeDependency(oldFrame.resource!!)
                        }
                        projection = newProjection
                        frame =
                    }
                }
            }
        }
    }
}

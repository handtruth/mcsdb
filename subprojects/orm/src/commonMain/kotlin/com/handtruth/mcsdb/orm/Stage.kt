package com.handtruth.mcsdb.orm

/**
 * Frame stage machine stages.
 *
 * ```
 *   +------------+
 *   | Initiated  |
 *   +------------+
 *         ||
 *         \/
 *   +------------+
 *   |Synchronised|
 *   +------------+
 *   ...
 * ```
 */
public enum class Stage {
    Initiated, Synchronised, Constructed, Destroyed, Disposed
}

package com.handtruth.mcsdb.util

internal interface Cleanable {
    val requireClean: Boolean
    fun clean()
}

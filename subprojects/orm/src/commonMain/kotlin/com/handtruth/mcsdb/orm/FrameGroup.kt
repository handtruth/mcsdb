package com.handtruth.mcsdb.orm

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

internal class FrameGroup(private val main: ShadowFrame) {

    private val privateStage = MutableStateFlow(Stage.Initiated)

    val stage: StateFlow<Stage> get() = privateStage

    class Record(val frame: ShadowFrame, val optional: Boolean)

    private fun recollect() {

    }
}

package com.handtruth.mcsdb.orm

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.Kind
import com.handtruth.mcsdb.schema.Member
import kotlinx.collections.immutable.ImmutableList

public data class Form(
    val mainstay: EventDescription,
    val properties: ImmutableList<Property>,
    val factory: Shadow.Factory<*>
) {
    init {
        require(mainstay.kind != Kind.Fact) { "facts cannot be the main shadow interface" }

        val propertyNames = hashSetOf<String>()
        properties.forEach { property ->
            if (property is Property.Attribute) {
                val description = property.member.description
                require(
                    description == mainstay
                            || description.interfaces.any { mainstay == it.second.description }
                ) {
                    "attribute $property is not attached to main form interface"
                }
                val propertyName = property.name
                require(propertyName !in propertyNames) {
                    "property name \"$propertyName\" already used"
                }
                propertyNames += propertyName
            }
        }
    }

    public sealed class Property(public val name: String, public val variant: Variants, public val filter: Filter) {
        public enum class Variants {
            Required, Optional, Multivalued
        }

        public class Attribute(
            name: String,
            variant: Variants,
            public val member: Member,
            filter: Filter
        ) : Property(name, variant, filter) {

            override fun toString(): String =
                "property(name = $name, variant = $variant, attribute = $member, filter = $filter)"

            override fun equals(other: Any?): Boolean =
                other is Attribute && other.name == this.name && other.member == this.member
                        && other.variant == this.variant && other.filter == this.filter

            override fun hashCode(): Int = name.hashCode() + member.hashCode() + variant.hashCode() + filter.hashCode()
        }

        public class Component(
            name: String,
            variant: Variants,
            lazyForm: Lazy<Form>,
            filter: Filter
        ) : Property(name, variant, filter) {
            public constructor(
                name: String,
                variant: Variants,
                form: Form,
                filter: Filter
            ) : this(name, variant, lazyOf(form), filter)

            public val form: Form by lazyForm

            override fun toString(): String = "property(name = $name, form = $form, filter = $filter)"

            override fun equals(other: Any?): Boolean =
                other is Component && other.name == this.name && other.form == this.form
                        && other.variant == this.variant && other.filter == this.filter

            override fun hashCode(): Int = name.hashCode() + form.hashCode() + variant.hashCode() + filter.hashCode()
        }
    }

    public val filter: Filter = run {
        val start = filter { primary(mainstay) }
        properties.fold(start) { acc, property ->
            val append = filter {
                when (property) {
                    is Property.Attribute -> property.filter and primary(property.member.description)
                    is Property.Component -> property.filter and primary(property.form.mainstay)
                }
            }
            filter { acc or append }
        }
    }
}

package com.handtruth.mcsdb.orm

import com.handtruth.mc.collections.CopyOnWriteSet
import com.handtruth.mc.types.get
import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.time.dataOf
import com.handtruth.mcsdb.time.infoOf
import com.handtruth.mcsdb.time.succeeded
import com.handtruth.mcsdb.util.Cleanable
import com.handtruth.mcsdb.util.getValue
import com.handtruth.mcsdb.util.weak
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

public interface ShadowSet<S : Shadow> : Set<S> {
    public enum class Actions {
        Add, Remove
    }

    public val manager: Shadows

    public val form: Form

    public val filter: Filter

    public val actions: SharedFlow<Pair<Actions, S>>
}

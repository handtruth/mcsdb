package com.handtruth.mcsdb.util

import kotlin.reflect.KProperty

internal expect open class WeakReference<T>(value: T) {
    fun get(): T?
}

internal fun <T> weak(value: T) = WeakReference(value)

internal operator fun <T> WeakReference<T>.getValue(thisRef: Any?, property: KProperty<*>): T? {
    return get()
}

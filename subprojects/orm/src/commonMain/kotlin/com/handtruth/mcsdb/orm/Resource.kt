package com.handtruth.mcsdb.orm

import kotlinx.atomicfu.locks.SynchronizedObject
import kotlinx.atomicfu.locks.synchronized

internal abstract class Resource<F : Frame<F, S>, S> {
    lateinit var frame: F
        private set

    var shadow: S? = null
        private set

    fun provideFrame(frame: F) {
        check(!this::frame.isInitialized) { "frame already provided, bug" }
        this.frame = frame
    }

    fun provideShadow(shadow: S) {
        check(this.shadow === null) { "shadow already provided, bug" }
        this.shadow = shadow
    }

    private val dependencies = hashSetOf<Resource<*, *>>()

    private val dependenciesLock = SynchronizedObject()

    fun addDependency(resource: Resource<*, *>) {
        synchronized(dependenciesLock) {
            dependencies += resource
        }
    }

    fun removeDependency(resource: Resource<*, *>) {
        synchronized(dependenciesLock) {
            dependencies -= resource
        }
    }
}

internal typealias ShadowResource = Resource<ShadowFrame, Shadow>

internal typealias ShadowSetResource = Resource<ShadowSetFrame, ShadowSet<*>>

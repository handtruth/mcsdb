package com.handtruth.mcsdb

import com.handtruth.mcsdb.orm.form
import com.handtruth.mcsdb.test.groupDescriptionEvent
import com.handtruth.mcsdb.test.groupEvent
import kotlin.test.Test

class OrmTest {
    @Test
    fun formBuild() {
        val groupForm = form(groupEvent) {
            "group" required groupEvent["group"]
            "description" required groupDescriptionEvent["description"] filter {
                groupDescriptionEvent["description"] neq null
            }
        }
    }
}

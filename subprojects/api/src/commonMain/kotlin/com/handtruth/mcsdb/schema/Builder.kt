package com.handtruth.mcsdb.schema

import kotlinx.collections.immutable.toImmutableList

public class InheritanceDescriptionBuilder(public val description: EventDescription) {
    public val modifiers: MutableList<Modifier.InheritanceBound> = mutableListOf()

    public operator fun Modifier.InheritanceBound.unaryPlus() {
        modifiers += this
    }

    public fun build(): InheritanceDescription = InheritanceDescription(description, ModifierSet(modifiers))
}

public class FieldDescriptionBuilder(public val name: String, public val type: Types) {
    public val modifiers: MutableSet<Modifier.FieldBound> = mutableSetOf()

    public operator fun Modifier.FieldBound.unaryPlus() {
        modifiers += this
    }

    public fun build(): FieldDescription = FieldDescription(name, type, ModifierSet(modifiers))
}

@PublishedApi
internal fun <T> MutableCollection<T>.addAndGet(value: T): T {
    this += value
    return value
}

public open class EventDescriptionBuilder(public val name: String, public val kind: Kind) {
    public val modifiers: MutableSet<Modifier.EventBound> = mutableSetOf()
    public val inherits: MutableList<InheritanceDescription> = mutableListOf()
    public val fields: MutableList<FieldDescription> = mutableListOf()

    public operator fun Modifier.EventBound.unaryPlus() {
        modifiers += this
    }

    public inline fun inherit(
        description: EventDescription,
        block: InheritanceDescriptionBuilder.() -> Unit
    ): InheritanceDescription = inherits.addAndGet(InheritanceDescriptionBuilder(description).apply(block).build())

    public fun inherit(
        description: EventDescription,
        modifiers: Collection<Modifier.InheritanceBound>
    ): InheritanceDescription = inherits.addAndGet(InheritanceDescription(description, ModifierSet(modifiers)))

    public fun inherit(description: EventDescription): InheritanceDescription =
        inherits.addAndGet(InheritanceDescription(description, ModifierSet()))

    public inline fun field(name: String, type: Types, block: FieldDescriptionBuilder.() -> Unit): FieldDescription =
        fields.addAndGet(FieldDescriptionBuilder(name, type).apply(block).build())

    public fun field(name: String, type: Types, modifiers: Collection<Modifier.FieldBound>): FieldDescription =
        fields.addAndGet(FieldDescription(name, type, ModifierSet(modifiers)))

    public fun field(name: String, type: Types): FieldDescription =
        fields.addAndGet(FieldDescription(name, type, ModifierSet()))

    public fun build(): EventDescription =
        EventDescription(name, kind, ModifierSet(modifiers), inherits.toImmutableList(), fields.toImmutableList())
}

public class DirectEventDescriptionBuilder(name: String, kind: Kind) : EventDescriptionBuilder(name, kind) {
    public inline fun source(
        description: EventDescription,
        block: InheritanceDescriptionBuilder.() -> Unit
    ): InheritanceDescription = inherits.addAndGet(
        InheritanceDescriptionBuilder(description).apply {
            block()
            +Modifier.Source
        }.build()
    )

    public fun source(
        description: EventDescription,
        modifiers: Collection<Modifier.InheritanceBound>
    ): InheritanceDescription =
        inherits.addAndGet(InheritanceDescription(description, ModifierSet(modifiers + Modifier.Source)))

    public fun source(description: EventDescription): InheritanceDescription =
        inherits.addAndGet(InheritanceDescription(description, ModifierSet(Modifier.Source)))
}

public inline fun describeEvent(name: String, kind: Kind, block: EventDescriptionBuilder.() -> Unit): EventDescription =
    EventDescriptionBuilder(name, kind).apply(block).build()

public inline fun directEvent(name: String, block: DirectEventDescriptionBuilder.() -> Unit): EventDescription =
    DirectEventDescriptionBuilder(name, Kind.Direct).apply(block).build()

public inline fun interfaceEvent(name: String, block: EventDescriptionBuilder.() -> Unit): EventDescription =
    describeEvent(name, Kind.Interface, block)

public inline fun factEvent(name: String, block: EventDescriptionBuilder.() -> Unit): EventDescription =
    describeEvent(name, Kind.Fact, block)

package com.handtruth.mcsdb

public interface DataSource {
    public val protocol: String

    public suspend fun connect(location: String): Database
}

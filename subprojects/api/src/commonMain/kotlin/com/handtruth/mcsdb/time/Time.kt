package com.handtruth.mcsdb.time

import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.variability.State
import kotlinx.coroutines.flow.Flow

public interface Time {
    public val database: Database

    public suspend fun ask(state: State, timeQuery: TimeQuery): List<Event>

    public suspend fun raise(state: State, prototype: Event): Event

    public fun listen(state: State, actual: Boolean = false, stream: Boolean = true, filter: Filter): Flow<Event>

    public suspend fun rewind(state: State, timeQuery: TimeQuery)

    public suspend fun forget(state: State, timeQuery: TimeQuery)

    public suspend fun subgraph(state: State, timeQuery: TimeQuery): History

    public companion object
}

public suspend inline fun Time.ask(
    state: State,
    expression: TimeQueryBuilderContext.() -> TimeQuery
): Collection<Event> {
    return ask(state, timeQuery(expression))
}

public inline fun Time.listen(
    state: State,
    actual: Boolean = false,
    stream: Boolean = true,
    expression: FilterBuilderContext.() -> Filter
): Flow<Event> {
    return listen(state, actual, stream, filter(expression))
}

public suspend fun Time.rewind(state: State, expression: TimeQueryBuilderContext.() -> TimeQuery) {
    return rewind(state, timeQuery(expression))
}

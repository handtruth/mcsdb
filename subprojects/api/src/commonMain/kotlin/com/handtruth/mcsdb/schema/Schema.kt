package com.handtruth.mcsdb.schema

import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.time.Event

public interface Schema {
    public val database: Database

    public suspend fun register(descriptions: Iterable<EventDescription>)

    public suspend fun react(description: EventDescription, reactor: Reactor?)
    public fun reactorOf(description: EventDescription): Reactor?

    public companion object
}

public suspend inline fun Schema.register(vararg schemaModule: EventDescription) {
    register(schemaModule.asIterable())
}

public suspend fun Schema.react(description: EventDescription, block: suspend (Event) -> Event) {
    val reactor = Reactor(block)
    react(description, reactor)
}

public suspend fun Schema.reactBefore(description: EventDescription, reactor: Reactor) {
    val oldReactor = reactorOf(description)
    if (oldReactor != null) {
        val newReactor = Reactor { oldReactor.react(reactor.react(it)) }
        react(description, newReactor)
    } else {
        react(description, reactor)
    }
}

public suspend fun Schema.reactAfter(description: EventDescription, reactor: Reactor) {
    val oldReactor = reactorOf(description)
    if (oldReactor != null) {
        val newReactor = Reactor { reactor.react(oldReactor.react(it)) }
        react(description, newReactor)
    } else {
        react(description, reactor)
    }
}

public suspend inline fun Schema.reactBefore(
    description: EventDescription,
    crossinline block: suspend (Event) -> Event
) {
    reactBefore(description, Reactor(block))
}

public suspend inline fun Schema.reactAfter(
    description: EventDescription,
    crossinline block: suspend (Event) -> Event
) {
    reactAfter(description, Reactor(block))
}

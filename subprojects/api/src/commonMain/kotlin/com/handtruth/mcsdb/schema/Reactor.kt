@file:Suppress("FunctionName")

package com.handtruth.mcsdb.schema

import com.handtruth.mcsdb.time.Event

public interface Reactor {
    public suspend fun react(event: Event): Event
}

public inline fun Reactor(crossinline reactor: suspend (event: Event) -> Event): Reactor =
    object : Reactor {
        override suspend fun react(event: Event) = reactor(event)
        override fun toString() = "reactor"
    }

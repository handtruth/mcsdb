package com.handtruth.mcsdb.context

import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.schema.*
import com.handtruth.mcsdb.space.*
import com.handtruth.mcsdb.time.*
import com.handtruth.mcsdb.variability.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

public interface Context : CoroutineContext.Element {
    public val database: Database
    public val state: State

    override val key: Key get() = Key

    public companion object Key : CoroutineContext.Key<Context>
}

public operator fun Context.component1(): Database = database
public operator fun Context.component2(): State = state

private class ContextImpl(override val database: Database, override val state: State) : Context {
    override fun toString(): String = "Context(database=$database, state=$state)"
}

public fun Context(database: Database, state: State): Context = ContextImpl(database, state)

public suspend fun Context(database: Database): Context = Context(database, database.variability.getDefaultState())

public suspend inline fun getContext(): Context = coroutineContext[Context]
    ?: error("MCSDB context not attached to the coroutine context")

public suspend fun Schema.Companion.register(descriptions: Iterable<EventDescription>) {
    val context = getContext()
    return context.database.schema.register(descriptions)
}

public suspend fun Schema.Companion.register(vararg description: EventDescription) {
    val context = getContext()
    return context.database.schema.register(description.asIterable())
}

public suspend fun Schema.Companion.react(description: EventDescription, reactor: Reactor?) {
    val context = getContext()
    context.database.schema.react(description, reactor)
}

public suspend inline fun Schema.Companion.react(
    description: EventDescription,
    crossinline block: suspend (Event) -> Event
) {
    react(description, Reactor(block))
}

public suspend fun Schema.Companion.reactBefore(description: EventDescription, reactor: Reactor) {
    val context = getContext()
    context.database.schema.reactBefore(description, reactor)
}

public suspend fun Schema.Companion.reactAfter(description: EventDescription, reactor: Reactor) {
    val context = getContext()
    context.database.schema.reactAfter(description, reactor)
}

public suspend inline fun Schema.Companion.reactBefore(
    description: EventDescription,
    crossinline block: suspend (Event) -> Event
) {
    reactBefore(description, Reactor(block))
}

public suspend inline fun Schema.Companion.reactAfter(
    description: EventDescription,
    crossinline block: suspend (Event) -> Event
) {
    reactAfter(description, Reactor(block))
}

public suspend fun Time.Companion.ask(timeQuery: TimeQuery): List<Event> {
    val context = getContext()
    return context.database.time.ask(context.state, timeQuery)
}

public suspend inline fun Time.Companion.ask(expression: TimeQueryBuilderContext.() -> TimeQuery): List<Event> {
    return ask(timeQuery(expression))
}

public suspend fun Time.Companion.raise(prototype: Event): Event {
    val context = getContext()
    return context.database.time.raise(context.state, prototype)
}

public suspend fun Time.Companion.listen(actual: Boolean = false, stream: Boolean = true, filter: Filter): Flow<Event> {
    val context = getContext()
    return context.database.time.listen(context.state, actual, stream, filter)
}

public suspend fun Time.Companion.listen(
    actual: Boolean = false,
    stream: Boolean = true,
    expression: FilterBuilderContext.() -> Filter
): Flow<Event> {
    return listen(actual, stream, filter(expression))
}

public suspend fun Time.Companion.subgraph(timeQuery: TimeQuery): History {
    val context = getContext()
    return context.database.time.subgraph(context.state, timeQuery)
}
public suspend inline fun Time.Companion.subgraph(expression: TimeQueryBuilderContext.() -> TimeQuery): History {
    return subgraph(timeQuery(expression))
}

public suspend fun Space.Companion.ask(spaceQuery: SpaceQuery): Set<Particle> {
    val context = getContext()
    return context.database.space.ask(context.state, spaceQuery)
}

public suspend inline fun Space.Companion.ask(expression: SpaceQueryBuilderContext.() -> SpaceQuery): Set<Particle> {
    return ask(spaceQuery(expression))
}

public suspend fun Space.Companion.subgraph(spaceQuery: SpaceQuery): Matter {
    val context = getContext()
    return context.database.space.subgraph(context.state, spaceQuery)
}

public suspend inline fun Space.Companion.subgraph(expression: SpaceQueryBuilderContext.() -> SpaceQuery): Matter {
    return subgraph(spaceQuery(expression))
}

public suspend fun Variability.Companion.ask(variabilityQuery: VariabilityQuery): Set<State> {
    val context = getContext()
    return context.database.variability.ask(variabilityQuery)
}

public suspend inline fun Variability.Companion.ask(
    expression: VariabilityQueryBuilderContext.() -> VariabilityQuery
): Set<State> {
    return ask(variabilityQuery(expression))
}

public suspend fun Variability.Companion.fork(name: String, state: State? = null): State {
    val context = getContext()
    return context.database.variability.fork(name, state ?: context.state)
}

public suspend fun Variability.Companion.forget(state: State? = null) {
    val context = getContext()
    return context.database.variability.forget(state ?: context.state)
}

public suspend fun <R> Variability.Companion.fork(
    name: String,
    autoDelete: Boolean = false,
    state: State? = null,
    block: suspend CoroutineScope.() -> R
): R {
    contract {
        callsInPlace(block, InvocationKind.EXACTLY_ONCE)
    }
    val context = getContext()
    val database = context.database
    val variability = database.variability
    val newState = variability.fork(name, state ?: context.state)
    try {
        return withContext(Context(database, newState), block)
    } finally {
        if (autoDelete) {
            variability.forget(newState)
        }
    }
}

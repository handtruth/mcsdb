package com.handtruth.mcsdb.variability

import com.handtruth.mcsdb.Database

public interface Variability {
    public val database: Database

    public suspend fun ask(variabilityQuery: VariabilityQuery): Set<State>

    public suspend fun fork(name: String, state: State): State

    public suspend fun create(name: String): State

    public suspend fun forget(state: State)

    public suspend fun setReactive(state: State, reactive: Boolean)

    public companion object
}

public suspend inline fun Variability.ask(
    expression: VariabilityQueryBuilderContext.() -> VariabilityQuery
): Set<State> = ask(variabilityQuery(expression))

public suspend fun Variability.getDefaultState(): State = ask { default }.first()

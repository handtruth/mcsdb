package com.handtruth.mcsdb

import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.Member
import com.handtruth.mcsdb.schema.Roles
import kotlin.jvm.JvmName

public object FilterBuilderContext {
    public inline val O: Filter.Empty get() = Filter.Empty

    public inline val U: Filter.Universe get() = Filter.Universe

    public fun type(description: EventDescription): Filter.Role =
        Filter.Role(description, null)

    public fun source(description: EventDescription): Filter.Role =
        Filter.Role(description, Roles.Source)

    public fun fantom(description: EventDescription): Filter.Role =
        Filter.Role(description, Roles.Fantom)

    public fun target(description: EventDescription): Filter.Role =
        Filter.Role(description, Roles.Target)

    public fun attached(description: EventDescription): Filter.Attached =
        Filter.Attached(description)

    public fun primary(description: EventDescription): Filter = source(description) union type(description)

    public fun not(filter: Filter): Filter.Unary.Negation = Filter.Unary.Negation(filter)
    public infix fun Filter.or(other: Filter): Filter.Binary.Union = Filter.Binary.Union(this, other)
    public infix fun Filter.and(other: Filter): Filter.Binary.Intersection = Filter.Binary.Intersection(this, other)
    public infix fun Filter.intersect(other: Filter): Filter.Binary.Intersection =
        Filter.Binary.Intersection(this, other)

    public infix fun Filter.union(other: Filter): Filter.Binary.Union = Filter.Binary.Union(this, other)
    public infix fun Filter.sub(other: Filter): Filter.Binary.Exception = Filter.Binary.Exception(this, other)
    public infix fun Filter.except(other: Filter): Filter.Binary.Exception = Filter.Binary.Exception(this, other)

    public infix fun Member.eq(value: Any): Filter.Expression.Equal =
        Filter.Expression.Equal(this, value)

    public infix fun Member.eq(@Suppress("UNUSED_PARAMETER") value: Nothing?): Filter.Expression.IsNull =
        Filter.Expression.IsNull(this)

    @JvmName("nullableEq")
    public infix fun Member.eq(value: Any?): Filter.Expression = if (value != null) eq(value) else eq(null)

    public infix fun Member.neq(value: Any): Filter.Expression.NotEqual =
        Filter.Expression.NotEqual(this, value)

    public infix fun Member.neq(@Suppress("UNUSED_PARAMETER") value: Nothing?): Filter.Expression.IsNotNull =
        Filter.Expression.IsNotNull(this)

    @JvmName("nullableNeq")
    public infix fun Member.neq(value: Any?): Filter.Expression =
        if (value != null) neq(value) else neq(null)

    public infix fun <T> Member.le(value: Comparable<T>): Filter.Expression.Lesser =
        Filter.Expression.Lesser(this, value)

    public infix fun <T> Member.bg(value: Comparable<T>): Filter.Expression.Bigger =
        Filter.Expression.Bigger(this, value)

    public infix fun <T> Member.le(value: ByteArray): Filter.Expression.Lesser =
        Filter.Expression.Lesser(this, value)

    public infix fun <T> Member.bg(value: ByteArray): Filter.Expression.Bigger =
        Filter.Expression.Bigger(this, value)

    public infix fun Member.like(wildcard: String): Filter.Expression.Like =
        Filter.Expression.Like(this, wildcard)

    public infix fun Member.match(regex: String): Filter.Expression.Match =
        Filter.Expression.Match(this, regex)

    public inline fun via(description: EventDescription, block: FilterBuilderContext.() -> Filter): Filter.Via =
        Filter.Via(description, FilterBuilderContext.block())
}

public inline fun filter(block: FilterBuilderContext.() -> Filter): Filter {
    return FilterBuilderContext.block()
}

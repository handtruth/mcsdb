package com.handtruth.mcsdb

import com.handtruth.kommon.BeanContainer
import com.handtruth.mcsdb.schema.Schema
import com.handtruth.mcsdb.space.Space
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.variability.Variability
import io.ktor.utils.io.core.*

public interface Database : BeanContainer, Closeable {
    public val schema: Schema

    public val time: Time
    public val space: Space
    public val variability: Variability

    public companion object
}

public expect suspend fun Database.Companion.open(location: String): Database

package com.handtruth.mcsdb.space

import com.handtruth.mc.types.toMutableDynamic
import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.variability.State

public interface Space {
    public val database: Database

    public suspend fun ask(state: State, spaceQuery: SpaceQuery): Set<Particle>

    public suspend fun subgraph(state: State, spaceQuery: SpaceQuery): Matter

    public companion object
}

public suspend inline fun Space.ask(
    state: State,
    expression: SpaceQueryBuilderContext.() -> SpaceQuery
): Set<Particle> {
    return ask(state, spaceQuery(expression))
}

public fun Particle.toPrototype(): Event =
    description.prototype(data.toMutableDynamic().also { it["direction"] = true })

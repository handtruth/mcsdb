package com.handtruth.mcsdb.variability

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.time.TimeQuery
import com.handtruth.mcsdb.time.TimeQueryBuilderContext
import com.handtruth.mcsdb.time.timeQuery

public object VariabilityQueryBuilderContext {
    public inline val O: VariabilityQuery.Empty get() = VariabilityQuery.Empty

    public inline val U: VariabilityQuery.Universe get() = VariabilityQuery.Universe

    public inline val default: VariabilityQuery.Default get() = VariabilityQuery.Default

    public val reactive: VariabilityQuery.Reactive = VariabilityQuery.Reactive(true)

    private val notReactive = VariabilityQuery.Reactive(false)

    public fun contains(filter: Filter): VariabilityQuery.Contains {
        return VariabilityQuery.Contains(filter)
    }

    public inline fun contains(block: FilterBuilderContext.() -> Filter): VariabilityQuery.Contains {
        return contains(filter(block))
    }

    public fun happened(timeQuery: TimeQuery): VariabilityQuery.Happened {
        return VariabilityQuery.Happened(timeQuery)
    }

    public inline fun happened(block: TimeQueryBuilderContext.() -> TimeQuery): VariabilityQuery.Happened {
        return happened(timeQuery(block))
    }

    public object NameObject {
        public fun eq(value: String): VariabilityQuery.Named.Equals {
            return VariabilityQuery.Named.Equals(value)
        }

        public fun like(value: String): VariabilityQuery.Named.Like {
            return VariabilityQuery.Named.Like(value)
        }

        public fun match(value: String): VariabilityQuery.Named.Match {
            return VariabilityQuery.Named.Match(value)
        }
    }

    public inline val name: NameObject get() = NameObject

    public fun not(variabilityQuery: VariabilityQuery): VariabilityQuery.Unary.Negation {
        return VariabilityQuery.Unary.Negation(variabilityQuery)
    }

    public fun not(variabilityQuery: VariabilityQuery.Reactive): VariabilityQuery.Reactive {
        return if (variabilityQuery.reactive) notReactive else reactive
    }

    public infix fun VariabilityQuery.intersect(other: VariabilityQuery): VariabilityQuery.Binary.Intersection {
        return VariabilityQuery.Binary.Intersection(this, other)
    }

    public infix fun VariabilityQuery.and(other: VariabilityQuery): VariabilityQuery.Binary.Intersection {
        return this intersect other
    }

    public infix fun VariabilityQuery.union(other: VariabilityQuery): VariabilityQuery.Binary.Union {
        return VariabilityQuery.Binary.Union(this, other)
    }

    public infix fun VariabilityQuery.or(other: VariabilityQuery): VariabilityQuery.Binary.Union {
        return this union other
    }

    public infix fun VariabilityQuery.except(other: VariabilityQuery): VariabilityQuery.Binary.Exception {
        return VariabilityQuery.Binary.Exception(this, other)
    }

    public infix fun VariabilityQuery.sub(other: VariabilityQuery): VariabilityQuery.Binary.Exception {
        return this except other
    }
}

public inline fun variabilityQuery(block: VariabilityQueryBuilderContext.() -> VariabilityQuery): VariabilityQuery {
    return VariabilityQueryBuilderContext.block()
}

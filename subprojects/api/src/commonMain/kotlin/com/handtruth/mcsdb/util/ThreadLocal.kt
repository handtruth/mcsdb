package com.handtruth.mcsdb.util

import com.handtruth.mcsdb.InternalMcsdbApi

@InternalMcsdbApi
public expect class ThreadLocal<T>() {
    public fun set(value: T)
    public fun get(): T?
    public fun remove()
}

@InternalMcsdbApi
public inline fun <T, R> ThreadLocal<T>.use(value: T, block: () -> R): R {
    val old = get()
    set(value)
    try {
        return block()
    } finally {
        if (old === null) {
            remove()
        } else {
            set(old)
        }
    }
}

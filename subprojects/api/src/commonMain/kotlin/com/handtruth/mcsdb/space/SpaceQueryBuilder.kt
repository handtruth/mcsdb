package com.handtruth.mcsdb.space

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.filter

public object SpaceQueryBuilderContext {
    public val U: SpaceQuery = select { U }

    public val O: SpaceQuery = select { O }

    public fun select(filter: Filter): SpaceQuery.Select = SpaceQuery.Select(filter)

    public inline fun select(block: FilterBuilderContext.() -> Filter): SpaceQuery.Select {
        return SpaceQuery.Select(filter(block))
    }
}

public inline fun spaceQuery(block: SpaceQueryBuilderContext.() -> SpaceQuery): SpaceQuery {
    return SpaceQueryBuilderContext.block()
}

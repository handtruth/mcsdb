package com.handtruth.mcsdb.time

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.filter
import kotlinx.datetime.Instant
import kotlin.jvm.JvmName

public object TimeQueryBuilderContext {
    public inline val O: TimeQuery.Empty get() = TimeQuery.Empty

    public inline val U: TimeQuery.Universe get() = TimeQuery.Universe

    public fun first(timeQuery: TimeQuery): TimeQuery.Order {
        return TimeQuery.Order(TimeQuery.Order.Orders.First, timeQuery)
    }

    public fun last(timeQuery: TimeQuery): TimeQuery.Order {
        return TimeQuery.Order(TimeQuery.Order.Orders.Last, timeQuery)
    }

    public fun before(timeQuery: TimeQuery): TimeQuery.Cause {
        return TimeQuery.Cause(TimeQuery.Cause.Causes.Before, timeQuery)
    }

    public fun after(timeQuery: TimeQuery): TimeQuery.Cause {
        return TimeQuery.Cause(TimeQuery.Cause.Causes.After, timeQuery)
    }

    public fun from(filter: Filter): TimeQuery.From {
        return TimeQuery.From(filter)
    }

    public inline fun from(block: FilterBuilderContext.() -> Filter): TimeQuery.From {
        return from(filter(block))
    }

    public object TimestampObject {
        public infix fun eq(value: Instant): TimeQuery.Expression.Equals {
            return TimeQuery.Expression.Equals(TimeQuery.Expression.TimeProperties.Timestamp, value)
        }

        public infix fun eq(value: String): TimeQuery.Expression.Equals {
            return this eq Instant.parse(value)
        }

        public infix fun neq(value: Instant): TimeQuery.Expression.NotEquals {
            return TimeQuery.Expression.NotEquals(TimeQuery.Expression.TimeProperties.Timestamp, value)
        }

        public infix fun neq(value: String): TimeQuery.Expression.NotEquals {
            return this neq Instant.parse(value)
        }

        public infix fun le(value: Instant): TimeQuery.Expression.Lesser {
            return TimeQuery.Expression.Lesser(TimeQuery.Expression.TimeProperties.Timestamp, value)
        }

        public infix fun le(value: String): TimeQuery.Expression.Lesser {
            return this le Instant.parse(value)
        }

        public infix fun bg(value: Instant): TimeQuery.Expression.Bigger {
            return TimeQuery.Expression.Bigger(TimeQuery.Expression.TimeProperties.Timestamp, value)
        }

        public infix fun bg(value: String): TimeQuery.Expression.Bigger {
            return this bg Instant.parse(value)
        }
    }

    public inline val timestamp: TimestampObject get() = TimestampObject

    public object ErrorObject {
        public infix fun eq(value: String): TimeQuery.Expression.Equals {
            return TimeQuery.Expression.Equals(TimeQuery.Expression.TimeProperties.Error, value)
        }

        public infix fun neq(value: String): TimeQuery.Expression.NotEquals {
            return TimeQuery.Expression.NotEquals(TimeQuery.Expression.TimeProperties.Error, value)
        }

        public infix fun eq(@Suppress("UNUSED_PARAMETER") value: Nothing?): TimeQuery.Expression.IsNull {
            return TimeQuery.Expression.IsNull(TimeQuery.Expression.TimeProperties.Error)
        }

        public infix fun neq(@Suppress("UNUSED_PARAMETER") value: Nothing?): TimeQuery.Expression.IsNotNull {
            return TimeQuery.Expression.IsNotNull(TimeQuery.Expression.TimeProperties.Error)
        }

        @JvmName("nullableEq")
        public infix fun eq(value: String?): TimeQuery.Expression {
            return when (value) {
                null -> this eq null
                else -> this eq value
            }
        }

        @JvmName("nullableNeq")
        public infix fun neq(value: String?): TimeQuery.Expression {
            return when (value) {
                null -> this neq null
                else -> this neq value
            }
        }

        public infix fun like(value: String): TimeQuery.Expression.LikeError {
            return TimeQuery.Expression.LikeError(value)
        }

        public infix fun match(value: String): TimeQuery.Expression.MatchError {
            return TimeQuery.Expression.MatchError(value)
        }
    }

    public inline val error: ErrorObject get() = ErrorObject

    public inline val success: TimeQuery.Expression.IsNull get() = error eq null

    public inline val failure: TimeQuery.Expression.IsNotNull get() = error neq null

    public object DirectionObject {
        public infix fun eq(value: Boolean): TimeQuery.Expression.Equals {
            return TimeQuery.Expression.Equals(TimeQuery.Expression.TimeProperties.Direction, value)
        }

        public infix fun neq(value: Boolean): TimeQuery.Expression.NotEquals {
            return TimeQuery.Expression.NotEquals(TimeQuery.Expression.TimeProperties.Direction, value)
        }

        public infix fun eq(@Suppress("UNUSED_PARAMETER") value: Nothing?): TimeQuery.Expression.IsNull {
            return TimeQuery.Expression.IsNull(TimeQuery.Expression.TimeProperties.Direction)
        }

        public infix fun neq(@Suppress("UNUSED_PARAMETER") value: Nothing?): TimeQuery.Expression.IsNotNull {
            return TimeQuery.Expression.IsNotNull(TimeQuery.Expression.TimeProperties.Direction)
        }

        public infix fun eq(value: Boolean?): TimeQuery.Expression {
            return when (value) {
                null -> this eq null
                else -> this eq value
            }
        }

        public infix fun neq(value: Boolean?): TimeQuery.Expression {
            return when (value) {
                null -> this neq null
                else -> this neq value
            }
        }
    }

    public inline val direction: DirectionObject get() = DirectionObject

    public inline val fact: TimeQuery.Expression.IsNull get() = direction eq null

    public inline val direct: TimeQuery.Expression.IsNotNull get() = direction neq null

    public fun not(timeQuery: TimeQuery): TimeQuery.Unary.Negation {
        return TimeQuery.Unary.Negation(timeQuery)
    }

    public infix fun TimeQuery.intersect(other: TimeQuery): TimeQuery.Binary.Intersection {
        return TimeQuery.Binary.Intersection(this, other)
    }

    public infix fun TimeQuery.and(other: TimeQuery): TimeQuery.Binary.Intersection {
        return this intersect other
    }

    public infix fun TimeQuery.union(other: TimeQuery): TimeQuery.Binary.Union {
        return TimeQuery.Binary.Union(this, other)
    }

    public infix fun TimeQuery.or(other: TimeQuery): TimeQuery.Binary.Union {
        return this union other
    }

    public infix fun TimeQuery.except(other: TimeQuery): TimeQuery.Binary.Exception {
        return TimeQuery.Binary.Exception(this, other)
    }

    public infix fun TimeQuery.sub(other: TimeQuery): TimeQuery.Binary.Exception {
        return this except other
    }
}

public inline fun timeQuery(block: TimeQueryBuilderContext.() -> TimeQuery): TimeQuery {
    return TimeQueryBuilderContext.block()
}

package com.handtruth.mcsdb.test

import com.handtruth.mc.types.buildDynamic
import kotlin.test.Test
import kotlin.test.assertEquals

class StaticTest {

    @Test
    fun staticAndDynamicSameness() {
        val events = listOf(
            SystemActorEvent,
            UserContextEvent,
            UserActorEvent,
            UserCreationEvent,
            GroupContextEvent,
            GroupActorEvent,
            GroupCreationEvent,
            GroupMemberEvent
        )
        val static = events.map { it.description }

        println(static)

        val dynamic = listOf(
            systemActorEvent,
            userContextEvent,
            userActorEvent,
            userCreationEvent,
            groupContextEvent,
            groupActorEvent,
            groupCreationEvent,
            groupMemberEvent
        )

        assertEquals(static.size, dynamic.size)
        static.zip(dynamic) { staticOne, dynamicOne ->
            assertEquals(staticOne, dynamicOne)
        }
    }

    @Test
    fun staticFromDynamicWrappingTest() {
        val userCreationParameters = buildDynamic {
            "user" assign "Xydgiz"
            "actor" assign 23
        }
        val userCreationStatic = UserCreationEvent.wrap(userCreationParameters)
        val userCreationExpected = UserCreationEvent("Xydgiz", 23)
        assertEquals(userCreationExpected, userCreationStatic)

        val userContextParameters = buildDynamic {
            "user" assign "Xydgiz"
            "actor" assign 23
            "direction" assign true
        }
        val userContextStatic = UserContextEvent.wrap(userContextParameters)
        val userContextExpected = UserContextEvent("Xydgiz", 23, true)
        assertEquals(userContextExpected, userContextStatic)
    }
}

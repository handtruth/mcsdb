package com.handtruth.mcsdb.test

import com.handtruth.mc.types.UUID
import com.handtruth.mcsdb.types.Serial
import kotlin.test.Test
import kotlin.test.assertEquals

class TypesTest {

    @Test
    fun uuidTest() {
        val origin = "123e4567-e89b-12d3-A456-4266141740Cf"
        val uuid = UUID.parse(origin)
        assertEquals(0x123e4567e89b12d3, uuid.most)
        assertEquals(0xA4564266141740Cfu.toLong(), uuid.least)
        assertEquals(origin.toLowerCase(), uuid.toString())
    }

    @Test
    fun serialTest() {
        val serial = Serial(0x0f4351)
        assertEquals(3, serial.size)
        assertEquals("0F4351", serial.toString())
        val other = Serial.parse("0f4351")
        assertEquals(serial, other)
    }
}

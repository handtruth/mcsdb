package com.handtruth.mcsdb

import com.handtruth.mcsdb.util.loadObjects
import java.net.URI

private val dataSources by lazy {
    loadObjects<DataSource>().associateBy { it.protocol }
}

public actual suspend fun Database.Companion.open(location: String): Database {
    val uri = URI(location)
    val scheme = requireNotNull(uri.scheme) { "no URI schema provided" }
    val dataSource = dataSources[scheme] ?: error("data source with protocol $scheme not found")
    return dataSource.connect(location)
}

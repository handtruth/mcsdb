// ktlint-disable filename

package com.handtruth.mcsdb.util

import com.handtruth.mcsdb.InternalMcsdbApi

@InternalMcsdbApi
public actual class ThreadLocal<T> : java.lang.ThreadLocal<T>()

import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode

plugins {
    kotlin("plugin.serialization")
}

kotlin {
    explicitApi = ExplicitApiMode.Strict
}

dependencies {
    fun kotlinx(name: String) = "org.jetbrains.kotlinx:kotlinx-$name"
    fun handtruth(name: String) = "com.handtruth.$name"
    fun mc(name: String) = handtruth("mc:$name")
    fun kommon(name: String) = handtruth("kommon:kommon-$name")

    commonMainApi(project(":mcsdb-base"))
    jvmMainImplementation(kotlin("reflect"))
    commonTestImplementation(project(":mcsdb-test-schema"))
    commonTestImplementation("io.ktor:ktor-test-dispatcher")
    commonTestRuntimeOnly("ch.qos.logback:logback-classic")
}

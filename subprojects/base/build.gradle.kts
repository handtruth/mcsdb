import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode

plugins {
    kotlin("plugin.serialization")
}

kotlin {
    explicitApi = ExplicitApiMode.Strict
}

dependencies {
    fun kotlinx(name: String) = "org.jetbrains.kotlinx:kotlinx-$name"
    fun handtruth(name: String) = "com.handtruth.$name"
    fun mc(name: String) = handtruth("mc:$name")
    fun kommon(name: String) = handtruth("kommon:kommon-$name")

    commonMainApi(kotlinx("collections-immutable"))
    commonMainApi(kotlinx("serialization-core"))
    commonMainApi(kotlinx("coroutines-core"))
    commonMainApi(kotlinx("datetime"))
    commonMainApi(mc("tools-types"))
    commonMainApi("io.ktor:ktor-io")
    commonMainApi(mc("tools-graph"))
    commonMainImplementation(mc("tools-nbt"))
    commonMainApi(kommon("state"))
}

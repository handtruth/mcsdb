package com.handtruth.mcsdb.variability

import com.handtruth.mcsdb.MCSDBException

public class VariabilityException(
    public val code: Codes,
    public val explanation: String
) : MCSDBException() {
    public enum class Codes(public val message: String) {
        Unknown("unknown error"),
        ReadOnlyState("state cannot be modified"),
        NotReactiveState("state should be reactive"),
        ExistingName("state with such name already exists"),
        ;
    }

    override val message: String get() = "${code.message}: $explanation"
}

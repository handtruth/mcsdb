package com.handtruth.mcsdb

public open class MCSDBException : RuntimeException {
    public constructor() : super()
    public constructor(message: String?) : super(message)
    public constructor(cause: Throwable?) : super(cause)
    public constructor(message: String?, cause: Throwable?) : super(message, cause)
}

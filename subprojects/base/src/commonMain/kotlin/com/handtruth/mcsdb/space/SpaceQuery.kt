package com.handtruth.mcsdb.space

import com.handtruth.mcsdb.Filter

public sealed class SpaceQuery {
    public data class Select(public val filter: Filter) : SpaceQuery() {
        override fun toString(): String = "select($filter)"
    }
}

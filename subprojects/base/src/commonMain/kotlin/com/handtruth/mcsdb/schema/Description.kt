package com.handtruth.mcsdb.schema

import com.handtruth.kommon.BeanContainer
import com.handtruth.kommon.BeanJar
import com.handtruth.mc.types.Dynamic
import com.handtruth.mc.types.MutableDynamic
import com.handtruth.mc.types.buildDynamic
import com.handtruth.mcsdb.InternalMcsdbApi
import com.handtruth.mcsdb.time.AnyEvent
import com.handtruth.mcsdb.time.DynamicEventInfo
import com.handtruth.mcsdb.time.EventInfo
import kotlinx.collections.immutable.* // ktlint-disable no-wildcard-imports
import kotlinx.datetime.Instant

/**
 * A set of [Modifier] objects with specified bound type [M].
 * This set implies that 2 modifiers of the same class can not be contained there.
 * This set is immutable.
 *
 * @param M modifier bound type can be ether [Modifier.FieldBound],
 * [Modifier.InheritanceBound] or [Modifier.EventBound]
 */
public class ModifierSet<M : Modifier.BoundType> private constructor(
    private val modifiers: ImmutableSet<M>
) : ImmutableSet<M> by modifiers {

    /**
     * @param modifiers modifiers that would be included in this set
     */
    public constructor(modifiers: Iterable<M>) :
        this(modifiers.associateBy { it::class }.values.toImmutableSet())

    /**
     * @param modifiers modifiers that would be included in this set
     */
    public constructor(vararg modifiers: M) : this(modifiers.asIterable())

    override fun hashCode(): Int = modifiers.hashCode()
    override fun equals(other: Any?): Boolean = other is ModifierSet<*> && modifiers == other.modifiers
    override fun toString(): String = modifiers.toString()
}

/**
 * Type that provides a full information about specific field in [com.handtruth.mcsdb.time.Event]
 *
 * @param name field name should contain only latin alphabet letters in both registers,
 * arabic digits, '_' or '-'
 */
public data class FieldDescription(
    val name: String,
    val type: Types,
    val modifiers: ModifierSet<Modifier.FieldBound>
) {
    init {
        require(name.all { it in '0'..'9' || it in 'a'..'z' || it in 'A'..'Z' || it == '-' || it == '_' }) {
            "illegal characters in field name"
        }
    }
}

/**
 * Type that describes modifiers and interface type that event is connected to.
 *
 * @param description event description of interface
 * @param modifiers modifiers that will influence on event connection to interface
 */
public data class InheritanceDescription(
    val description: EventDescription,
    val modifiers: ModifierSet<Modifier.InheritanceBound>
) {
    init {
        require(description.kind == Kind.Interface) {
            "not an interface kind"
        }
    }
}

private fun getRoleOf(inheritanceDescription: InheritanceDescription): Roles {
    val inRole = inheritanceDescription.modifiers.find { it is Modifier.Role }
    if (inRole == null) {
        val role = inheritanceDescription.description.modifiers.find { it is Modifier.Role }
            ?: return Roles.Target
        role as Modifier.Role
        return role.role
    } else {
        inRole as Modifier.Role
        return inRole.role
    }
}

internal val emptyEventInfo = DynamicEventInfo(null, null, null)

/**
 * Description of any event.
 */
public data class EventDescription(
    val name: String,
    val kind: Kind,
    val modifiers: ModifierSet<Modifier.EventBound>,
    val inherits: ImmutableList<InheritanceDescription>,
    val fields: ImmutableList<FieldDescription>
) : BeanContainer {
    init {
        val preselected = ArrayList<EventDescription>(inherits.size)
        for (inheritance in inherits) {
            val description = inheritance.description
            checkSchema(description !in preselected) {
                "event cannot inherit one interface type several times at the same level"
            }
            preselected += description
        }
    }

    public val allFields: ImmutableList<FieldDescription> = persistentListOf<FieldDescription>().mutate { list ->
        val inheritedFieldsNames = hashSetOf<String>()
        for (inheritance in inherits) {
            val inheritedFields = inheritance.description.allFields
            inheritedFields.mapTo(inheritedFieldsNames) { it.name }
            list += inheritedFields
        }
        for (field in fields) {
            checkSchema(field.name !in inheritedFieldsNames) {
                "event declares several fields with the same name"
            }
            inheritedFieldsNames += field.name
        }
        list += fields
    }

    public val interfaces: Interfaces = Interfaces()

    override val beanJar: BeanJar = BeanJar.Simple()

    public val allRealFields: ImmutableList<FieldDescription> = if (kind == Kind.Interface) {
        allFields
    } else {
        val names = hashSetOf<String>()
        allFields.mapTo(names) { it.name }
        for (eventInterface in interfaces[Roles.Fantom]) {
            eventInterface.description.fields.forEach { field ->
                names -= field.name
            }
        }
        allFields.filter { it.name in names }.toImmutableList()
    }

    public inner class Interfaces internal constructor() : Iterable<Pair<Roles, InheritanceDescription>> {
        private val byRole: Array<ImmutableList<InheritanceDescription>>
        private val byName = hashMapOf<String, Pair<Roles, InheritanceDescription>>()

        init {
            for (inheritance in inherits) {
                byName += inheritance.description.interfaces.byName
            }
            for (inheritance in inherits) {
                val description = inheritance.description
                val role = getRoleOf(inheritance)
                byName[description.name] = role to inheritance
            }
            val groupedByRole = byName.values.groupBy { it.first.ordinal }
            byRole = Array(Roles.values().size) { i ->
                (groupedByRole[i] ?: emptyList()).map { it.second }.toImmutableList()
            }
        }

        public operator fun get(role: Roles): ImmutableCollection<InheritanceDescription> = byRole[role.ordinal]

        public operator fun get(description: EventDescription): Pair<Roles, InheritanceDescription>? {
            val result = byName[description.name] ?: return null
            if (result.second.description != description) {
                return null
            }
            return result
        }

        override fun iterator(): Iterator<Pair<Roles, InheritanceDescription>> = byName.values.iterator()
    }

    public fun prototype(parameters: Dynamic): AnyEvent =
        AnyEvent(this, parameters, emptyEventInfo)

    public inline operator fun invoke(block: (MutableDynamic) -> Unit): AnyEvent {
        return prototype(buildDynamic(block))
    }

    @InternalMcsdbApi
    public fun event(extras: EventInfo, parameters: Dynamic): AnyEvent =
        AnyEvent(this, parameters, extras)

    public fun event(timestamp: Instant, error: Throwable?, parameters: Dynamic): AnyEvent =
        event(DynamicEventInfo(timestamp, error, error?.stackTraceToString()), parameters)

    public fun event(timestamp: Instant, error: String?, parameters: Dynamic): AnyEvent =
        event(DynamicEventInfo(timestamp, null, error), parameters)

    public fun event(timestamp: Instant, parameters: Dynamic): AnyEvent =
        event(DynamicEventInfo(timestamp, null, null), parameters)

    private fun InheritanceDescription.getNextRealFields(result: MutableSet<String>) {
        if (getRoleOf(this) != Roles.Fantom) {
            for (field in description.allFields) {
                if (field.name !in result) {
                    result += field.name
                }
            }
        }
        for (eventInterface in description.inherits) {
            eventInterface.getNextRealFields(result)
        }
    }

    public operator fun get(field: String): Member = Member(this, allFields.first { it.name == field })

    public infix fun isAttachedTo(description: EventDescription): Boolean =
        description == this || interfaces[description] != null

    public infix fun isPrimaryOn(description: EventDescription): Boolean =
        description == this || interfaces[description]?.first == Roles.Source
}

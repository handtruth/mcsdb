package com.handtruth.mcsdb

@RequiresOptIn(
    "You should not use this API. It is internal and may change in future versions.",
    RequiresOptIn.Level.ERROR
)
public annotation class InternalMcsdbApi

package com.handtruth.mcsdb.time

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.schema.Types

public sealed class TimeQuery {
    public object Empty : TimeQuery() {
        override fun toString(): String = "∅"
    }

    public object Universe : TimeQuery() {
        override fun toString(): String = "U"
    }

    public data class Order(val order: Orders, val timeQuery: TimeQuery) : TimeQuery() {
        override fun toString(): String {
            val orderString = when (order) {
                Orders.First -> "first"
                Orders.Last -> "last"
            }
            return "$orderString($timeQuery)"
        }

        public enum class Orders {
            First, Last
        }
    }

    public data class Cause(val cause: Causes, val timeQuery: TimeQuery) : TimeQuery() {
        override fun toString(): String {
            val causeString = when (cause) {
                Causes.Before -> "before"
                Causes.After -> "after"
            }
            return "$causeString($timeQuery)"
        }

        public enum class Causes {
            Before, After
        }
    }

    public data class From(val filter: Filter) : TimeQuery() {
        override fun toString(): String = "from($filter)"
    }

    public sealed class Expression(public val property: TimeProperties) : TimeQuery() {
        public operator fun component1(): TimeProperties = property

        public enum class TimeProperties(public val type: Types) {
            Timestamp(Types.Instant), Error(Types.String), Direction(Types.Boolean)
        }

        public class Equals(property: TimeProperties, public val value: Any) : Expression(property) {
            public operator fun component2(): Any = value

            override fun equals(other: Any?): Boolean =
                other === this || other is Equals && property == other.property && value == other.value

            override fun hashCode(): Int =
                1 + property.hashCode() + value.hashCode()

            override fun toString(): String = "$property eq $value"
        }

        public class NotEquals(property: TimeProperties, public val value: Any) : Expression(property) {
            public operator fun component2(): Any = value

            override fun equals(other: Any?): Boolean =
                other === this || other is NotEquals && property == other.property && value == other.value

            override fun hashCode(): Int =
                2 + property.hashCode() + value.hashCode()

            override fun toString(): String = "$property neq $value"
        }

        public class Lesser(property: TimeProperties, public val value: Any) : Expression(property) {
            public operator fun component2(): Any = value

            override fun equals(other: Any?): Boolean =
                other === this || other is Lesser && property == other.property && value == other.value

            override fun hashCode(): Int =
                3 + property.hashCode() + value.hashCode()

            override fun toString(): String = "$property le $value"
        }

        public class Bigger(property: TimeProperties, public val value: Any) : Expression(property) {
            public operator fun component2(): Any = value

            override fun equals(other: Any?): Boolean =
                other === this || other is Bigger && property == other.property && value == other.value

            override fun hashCode(): Int =
                4 + property.hashCode() + value.hashCode()

            override fun toString(): String = "$property bg $value"
        }

        public class LikeError(public val wildcard: String) : Expression(TimeProperties.Error) {
            public operator fun component2(): Any = wildcard

            override fun equals(other: Any?): Boolean =
                other === this || other is LikeError && property == other.property && wildcard == other.wildcard

            override fun hashCode(): Int =
                5 + property.hashCode() + wildcard.hashCode()

            override fun toString(): String = "$property like '$wildcard'"
        }

        public class MatchError(public val regex: String) : Expression(TimeProperties.Error) {
            public operator fun component2(): Any = regex

            override fun equals(other: Any?): Boolean =
                other === this || other is MatchError && property == other.property && regex == other.regex

            override fun hashCode(): Int =
                6 + property.hashCode() + regex.hashCode()

            override fun toString(): String = "$property like '$regex'"
        }

        public class IsNull(property: TimeProperties) : Expression(property) {
            override fun equals(other: Any?): Boolean =
                other === this || other is IsNull && property == other.property

            override fun hashCode(): Int = 7 + property.hashCode()

            override fun toString(): String = "$property is null"
        }

        public class IsNotNull(property: TimeProperties) : Expression(property) {
            override fun equals(other: Any?): Boolean =
                other === this || other is IsNotNull && property == other.property

            override fun hashCode(): Int = 8 + property.hashCode()

            override fun toString(): String = "$property is not null"
        }
    }

    public sealed class Unary(public val timeQuery: TimeQuery) : TimeQuery() {
        public class Negation(timeQuery: TimeQuery) : Unary(timeQuery) {
            override fun toString(): String = "not($timeQuery)"
        }

        public operator fun component1(): TimeQuery = timeQuery

        override fun hashCode(): Int = timeQuery.hashCode().rotateLeft(16)

        override fun equals(other: Any?): Boolean =
            other !== null && (other === this || other::class == this::class && (other as Unary).timeQuery == timeQuery)
    }

    public sealed class Binary(public val a: TimeQuery, public val b: TimeQuery) : TimeQuery() {
        public class Intersection(a: TimeQuery, b: TimeQuery) : Binary(a, b) {
            override fun toString(): String = "($a) and ($b)"
        }

        public class Union(a: TimeQuery, b: TimeQuery) : Binary(a, b) {
            override fun toString(): String = "($a) or ($b)"
        }

        public class Exception(a: TimeQuery, b: TimeQuery) : Binary(a, b) {
            override fun toString(): String = "($a) sub ($b)"
        }

        public operator fun component1(): TimeQuery = a
        public operator fun component2(): TimeQuery = b

        override fun hashCode(): Int = a.hashCode() xor b.hashCode()

        override fun equals(other: Any?): Boolean =
            other !== null && (
                other === this ||
                    other::class == this::class && (other as Binary).a == a && other.b == b
                )
    }
}

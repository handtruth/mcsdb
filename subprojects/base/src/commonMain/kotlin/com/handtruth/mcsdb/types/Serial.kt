package com.handtruth.mcsdb.types

public class MalformedSerialException(message: String) : IllegalArgumentException(message)

private fun char2int(char: Char, i: Int): Int {
    return char.digitToIntOrNull(16) ?: throw MalformedSerialException("illegal symbol \"$char\" at position $i")
}

public class Serial(internal val bytes: ByteArray) : Number(), Comparable<Serial> {
    public constructor(value: Byte) : this(
        when (value) {
            in Byte.MIN_VALUE..-1 -> forbidNegative()
            0.toByte() -> nil.bytes
            else -> byteArrayOf(value)
        }
    )

    public constructor(value: Short) : this(
        when (value) {
            in Short.MIN_VALUE..-1 -> forbidNegative()
            0.toShort() -> nil.bytes
            else -> {
                val first = value.toByte()
                val second = (value.toInt() ushr 8).toByte()
                if (second == 0.toByte()) {
                    byteArrayOf(first)
                } else {
                    byteArrayOf(first, second)
                }
            }
        }
    )

    public constructor(value: Int) : this(
        if (value < 0) {
            forbidNegative()
        } else {
            int2bytes(value.toLong())
        }
    )

    public constructor(value: Long) : this(
        if (value < 0L) {
            forbidNegative()
        } else {
            int2bytes(value)
        }
    )

    public constructor() : this(nil.bytes)

    public val size: Int get() = bytes.size

    override fun equals(other: Any?): Boolean =
        this === other || other is Serial && bytes.contentEquals(other.bytes)

    override fun hashCode(): Int = bytes.contentHashCode()

    private fun codeInt(i: Int) = bytes[i].toInt() and 0xFF

    override fun toByte(): Byte = if (bytes.isNotEmpty()) bytes[0] else 0

    override fun toShort(): Short {
        val integer = if (bytes.size >= 2) {
            (codeInt(1) shl 8) or codeInt(0)
        } else {
            codeInt(0)
        }
        return integer.toShort()
    }

    override fun toInt(): Int = when (bytes.size) {
        0 -> 0
        1 -> codeInt(0)
        2 -> (codeInt(1) shl 8) or codeInt(0)
        3 -> (codeInt(2) shl 16) or (codeInt(1) shl 8) or codeInt(0)
        else -> (codeInt(3) shl 24) or (codeInt(2) shl 16) or (codeInt(1) shl 8) or codeInt(0)
    }

    private fun codeLong(i: Int): Long = bytes[i].toLong() and 0xFF

    private fun long4(): Long = toInt().toLong() and 0xFFFF_FFFF
    private fun long5(): Long = (codeLong(4) shl 32) or long4()
    private fun long6(): Long = (codeLong(5) shl 40) or long5()
    private fun long7(): Long = (codeLong(6) shl 48) or long6()
    private fun long8(): Long = (codeLong(7) shl 56) or long7()

    override fun toLong(): Long = when (bytes.size) {
        in 0..4 -> long4()
        5 -> long5()
        6 -> long6()
        7 -> long7()
        else -> long8()
    }

    override fun toChar(): Char = toShort().toChar()

    override fun toDouble(): Double = toLong().toDouble()

    override fun toFloat(): Float = toLong().toFloat()

    override fun toString(): String {
        val chars = CharArray(bytes.size * 2)
        for (i in bytes.indices) {
            val byte = bytes[i].toInt()
            val j = i shl 1
            chars[j] = (byte and 0xF).digitToChar(16)
            chars[j + 1] = ((byte ushr 4) and 0xF).digitToChar(16)
        }
        chars.reverse()
        return chars.concatToString()
    }

    public companion object {
        public val nil: Serial = Serial(ByteArray(0))

        public fun parse(string: CharSequence): Serial {
            val length = string.length
            if (length < 0 || (length and 1) != 0) {
                throw MalformedSerialException("illegal serial length: $length")
            }
            if (length == 0) {
                return nil
            }
            if (string[0] == '0' && string[1] == '0') {
                var offset = 2
                while (length - offset > 0 && string[offset] == '0' && string[offset + 1] == '0') {
                    offset += 2
                }
                parse(string.subSequence(offset, length))
            }
            val size = length ushr 1
            val bytes = ByteArray(size)
            for (i in 0 until size) {
                val j = length - (i shl 1) - 1
                bytes[i] = ((char2int(string[j - 1], j - 1) shl 4) or char2int(string[j], j)).toByte()
            }
            return Serial(bytes)
        }

        private fun forbidNegative(): Nothing {
            throw IllegalArgumentException("negative values are forbidden")
        }

        private fun int2bytes(value: Long): ByteArray {
            var skip = 0
            while (value ushr (56 - (skip shl 3)) == 0L) {
                ++skip
                if (skip == 8) {
                    return nil.bytes
                }
            }
            val bytes = ByteArray(8 - skip)
            var integer = value
            for (i in skip..7) {
                bytes[i - skip] = integer.toByte()
                integer = integer ushr 8
            }
            return bytes
        }
    }

    override fun compareTo(other: Serial): Int {
        val r = size.compareTo(other.size)
        if (r != 0) {
            return r
        } else {
            for (i in bytes.indices) {
                val c = codeInt(i).compareTo(other.codeInt(i))
                if (c != 0) {
                    return c
                }
            }
            return 0
        }
    }
}

package com.handtruth.mcsdb.schema

public enum class CausalRelationships {
    Model, Reactive, Transitive
}

package com.handtruth.mcsdb.space

import com.handtruth.mc.graph.Graph
import com.handtruth.mc.graph.MutableGraph
import com.handtruth.mcsdb.schema.CausalRelationships

public typealias Matter = Graph<Particle, CausalRelationships>
public typealias MutableMatter = MutableGraph<Particle, CausalRelationships>

package com.handtruth.mcsdb.time

import com.handtruth.mcsdb.MCSDBException

public class TimeException(public val code: Codes, public val explanation: String) : MCSDBException() {
    public enum class Codes(public val message: String) {
        CyclicReference("cyclic reference discovered"),
        IncompletePrototype("event prototype does not contain necessary fields"),
        WrongEventKind("event of this kind may not go throw reaction process"),
        WrongFieldType("wrong data type assigned to value in event prototype field"),
        InterfaceNotExists("specified interface of this type does not exists"),
        ParticleExists("attempt to create an existing particle"),
        ParticleNotExists("attempt to remove a not existing particle"),
        ExplicitForeignKey("event interface was explicitly declared as foreign key"),
        FantomUndefined("reaction initiator does not inherit fantom interface"),
        Concurrency("parallel transaction made reaction impossible"),
        DependantsExists("event dependants still there, maybe concurrent reaction happened?"),
        ;
    }

    override val message: String get() = "${code.message}: $explanation"
}

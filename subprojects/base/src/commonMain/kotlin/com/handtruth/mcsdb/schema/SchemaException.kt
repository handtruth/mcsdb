package com.handtruth.mcsdb.schema

import com.handtruth.mcsdb.InternalMcsdbApi
import com.handtruth.mcsdb.MCSDBException
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

public class SchemaException(public val code: Codes, public val explanation: String) : MCSDBException() {
    public enum class Codes(public val message: String) {
        InvalidSchema("incorrect schema"),
        DescriptionIsPresent("a different description with such name already exists")
    }

    override val message: String get() = "${code.message}: $explanation"
}

@InternalMcsdbApi
public fun checkSchema(value: Boolean, message: () -> Any?) {
    contract {
        callsInPlace(message, InvocationKind.AT_MOST_ONCE)
        returns() implies value
    }
    if (!value) {
        throw SchemaException(SchemaException.Codes.InvalidSchema, message().toString())
    }
}

@InternalMcsdbApi
public inline fun <T> checkSchemaNotNull(value: T?, message: () -> Any?): T {
    contract {
        callsInPlace(message, InvocationKind.AT_MOST_ONCE)
        returns() implies (value !== null)
    }
    return value ?: throw SchemaException(SchemaException.Codes.InvalidSchema, message().toString())
}

@InternalMcsdbApi
public fun invalidSchema(message: String): Nothing =
    throw SchemaException(SchemaException.Codes.InvalidSchema, message)

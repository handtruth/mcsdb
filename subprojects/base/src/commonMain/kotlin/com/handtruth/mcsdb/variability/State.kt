package com.handtruth.mcsdb.variability

public interface State {
    public val id: Long
}

public data class NamedState(
    override val id: Long,
    val name: String
) : State

package com.handtruth.mcsdb.variability

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.time.TimeQuery

public sealed class VariabilityQuery {
    public object Empty : VariabilityQuery() {
        override fun toString(): String = "∅"
    }

    public object Universe : VariabilityQuery() {
        override fun toString(): String = "U"
    }

    public object Default : VariabilityQuery() {
        override fun toString(): String = "default"
    }

    public data class Reactive(val reactive: Boolean) : VariabilityQuery() {
        override fun toString(): String = "reactive($reactive)"
    }

    public data class Contains(val filter: Filter) : VariabilityQuery() {
        override fun toString(): String = "contains($filter)"
    }

    public data class Happened(val timeQuery: TimeQuery) : VariabilityQuery() {
        override fun toString(): String = "happened($timeQuery)"
    }

    public sealed class Named(public val name: String) : VariabilityQuery() {
        public class Equals(name: String) : Named(name)

        public class Like(name: String) : Named(name)

        public class Match(name: String) : Named(name)

        override fun hashCode(): Int = name.hashCode().rotateLeft(16)

        override fun equals(other: Any?): Boolean =
            other !== null && (
                other === this ||
                    other::class == this::class && (other as Named).name == name
                )
    }

    public sealed class Unary(public val variabilityQuery: VariabilityQuery) : VariabilityQuery() {
        public class Negation(variabilityQuery: VariabilityQuery) : Unary(variabilityQuery) {
            override fun toString(): String = "not($variabilityQuery)"
        }

        public operator fun component1(): VariabilityQuery = variabilityQuery

        override fun hashCode(): Int = variabilityQuery.hashCode().rotateLeft(16)

        override fun equals(other: Any?): Boolean =
            other !== null && (
                other === this ||
                    other::class == this::class && (other as Unary).variabilityQuery == variabilityQuery
                )
    }

    public sealed class Binary(public val a: VariabilityQuery, public val b: VariabilityQuery) : VariabilityQuery() {
        public class Intersection(a: VariabilityQuery, b: VariabilityQuery) : Binary(a, b) {
            override fun toString(): String = "($a) and ($b)"
        }

        public class Union(a: VariabilityQuery, b: VariabilityQuery) : Binary(a, b) {
            override fun toString(): String = "($a) or ($b)"
        }

        public class Exception(a: VariabilityQuery, b: VariabilityQuery) : Binary(a, b) {
            override fun toString(): String = "($a) sub ($b)"
        }

        public operator fun component1(): VariabilityQuery = a
        public operator fun component2(): VariabilityQuery = b

        override fun hashCode(): Int = a.hashCode() xor b.hashCode()

        override fun equals(other: Any?): Boolean =
            other !== null && (
                other === this ||
                    other::class == this::class && (other as Binary).a == a && other.b == b
                )
    }
}

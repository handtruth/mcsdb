package com.handtruth.mcsdb.space

import com.handtruth.mc.types.Dynamic
import com.handtruth.mc.types.contentDeepEquals
import com.handtruth.mc.types.contentDeepHashCode
import com.handtruth.mc.types.contentToString
import com.handtruth.mcsdb.schema.EventDescription

public class Particle(
    public val description: EventDescription,
    public val data: Dynamic
) {
    override fun toString(): String = description.name + data.contentToString()

    override fun equals(other: Any?): Boolean =
        other is Particle && description == other.description &&
            data.contentDeepEquals(other.data)

    override fun hashCode(): Int = description.name.hashCode() + data.contentDeepHashCode()
}

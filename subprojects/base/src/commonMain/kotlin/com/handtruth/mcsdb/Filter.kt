package com.handtruth.mcsdb

import com.handtruth.mc.nbt.TagsModule
import com.handtruth.mc.types.contentDeepEquals
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.Member
import com.handtruth.mcsdb.schema.Roles
import com.handtruth.mcsdb.schema.Types
import com.handtruth.mcsdb.types.Serial
import kotlinx.datetime.Instant

public sealed class Filter {
    public object Empty : Filter() {
        override fun toString(): String = "∅"
    }

    public object Universe : Filter() {
        override fun toString(): String = "U"
    }

    public data class Role(val description: EventDescription, val role: Roles?) : Filter() {
        override fun toString(): String {
            val roleName = when (role) {
                null -> "type"
                Roles.Source -> "source"
                Roles.Fantom -> "fantom"
                Roles.Target -> "target"
            }
            return "$roleName(${description.name})"
        }
    }

    public data class Attached(val description: EventDescription) : Filter() {
        override fun toString(): String = "attached(${description.name})"
    }

    public sealed class Unary(public val filter: Filter) : Filter() {
        public class Negation(filter: Filter) : Unary(filter) {
            override fun toString(): String = "not ($filter)"
        }

        public operator fun component1(): Filter = filter

        override fun hashCode(): Int = filter.hashCode().rotateLeft(16)

        override fun equals(other: Any?): Boolean =
            other !== null && (other === this || other::class == this::class && (other as Unary).filter == filter)
    }

    public sealed class Binary(public val a: Filter, public val b: Filter) : Filter() {
        public class Intersection(a: Filter, b: Filter) : Binary(a, b) {
            override fun toString(): String = "($a) and ($b)"
        }

        public class Union(a: Filter, b: Filter) : Binary(a, b) {
            override fun toString(): String = "($a) or ($b)"
        }

        public class Exception(a: Filter, b: Filter) : Binary(a, b) {
            override fun toString(): String = "($a) sub ($b)"
        }

        public operator fun component1(): Filter = a
        public operator fun component2(): Filter = b

        override fun hashCode(): Int = a.hashCode() xor b.hashCode()

        override fun equals(other: Any?): Boolean =
            other !== null && (
                other === this ||
                    other::class == this::class && (other as Binary).a == a && other.b == b
                )
    }

    public sealed class Expression(public val member: Member) : Filter() {
        public operator fun component1(): Member = member

        protected fun checkComparableType(value: Any) {
            when (value) {
                is Serial, is Byte, is Short, is Int, is Long,
                is UByte, is UShort, is UInt, is ULong, is Char,
                is Float, is Double, is String, is Instant, is ByteArray -> {
                }
                else -> throw IllegalArgumentException("$value is not an MCSDB comparable type")
            }
        }

        protected fun checkType(member: Member, value: Any) {
            val type = member.field.type
            val kotlinClass = type.kotlinClass
            val valueClass = value::class
            val result = if (kotlinClass == null) {
                valueClass in structureTypes
            } else {
                valueClass == kotlinClass
            }
            require(result) { "$valueClass not a $type type" }
        }

        protected fun checkStringMember(member: Member) {
            val type = member.field.type
            require(type == Types.String) { "string member type required" }
        }

        protected fun areEquals(type: Types, a: Any, b: Any): Boolean {
            return when (type) {
                Types.Structure -> a contentDeepEquals b
                Types.Arbitrary -> (a as ByteArray) contentEquals (b as ByteArray)
                else -> a == b
            }
        }

        private companion object {
            val structureTypes = TagsModule.MCSDB.tags.map { it.type }
        }

        public class Equal(member: Member, public val value: Any) : Expression(member) {
            init {
                checkType(member, value)
            }

            override fun hashCode(): Int = member.hashCode() xor value.hashCode()

            override fun equals(other: Any?): Boolean =
                other === this || other is Equal && other.member == member &&
                    areEquals(member.field.type, other.value, value)

            override fun toString(): String = "$member eq $value"
        }

        public class NotEqual(member: Member, public val value: Any) : Expression(member) {
            init {
                checkType(member, value)
            }

            override fun hashCode(): Int = member.hashCode() xor value.hashCode()

            override fun equals(other: Any?): Boolean =
                other === this || other is NotEqual && other.member == member &&
                    areEquals(member.field.type, other.value, value)

            override fun toString(): String = "$member neq $value"
        }

        public class Lesser(member: Member, public val value: Any) : Expression(member) {
            init {
                checkType(member, value)
            }

            public operator fun component2(): Any = value

            override fun hashCode(): Int = member.hashCode() xor value.hashCode()

            override fun equals(other: Any?): Boolean =
                other === this || other is Lesser && member == other.member &&
                    areEquals(member.field.type, other.value, value)

            override fun toString(): String = "$member le $value"
        }

        public class Bigger(member: Member, public val value: Any) : Expression(member) {
            init {
                checkType(member, value)
            }

            public operator fun component2(): Any = value

            override fun equals(other: Any?): Boolean =
                other === this || other is Bigger && member == other.member &&
                    areEquals(member.field.type, other.value, value)

            override fun hashCode(): Int =
                member.hashCode() + value.hashCode()

            override fun toString(): String = "$member bg $value"
        }

        public class Like(member: Member, public val wildcard: String) : Expression(member) {
            init {
                checkStringMember(member)
            }

            public operator fun component2(): Any = wildcard

            override fun equals(other: Any?): Boolean =
                other === this || other is Like && member == other.member && wildcard == other.wildcard

            override fun hashCode(): Int =
                member.hashCode() + wildcard.hashCode()

            override fun toString(): String = "$member like '$wildcard'"
        }

        public class Match(member: Member, public val regex: String) : Expression(member) {
            init {
                checkStringMember(member)
            }

            public operator fun component2(): Any = regex

            override fun equals(other: Any?): Boolean =
                other === this || other is Match && member == other.member && regex == other.regex

            override fun hashCode(): Int =
                member.hashCode() + regex.hashCode()

            override fun toString(): String = "$member like '$regex'"
        }

        public class IsNull(member: Member) : Expression(member) {
            override fun hashCode(): Int = member.hashCode()

            override fun equals(other: Any?): Boolean =
                other === this || other is IsNull && other.member == member

            override fun toString(): String = "isNull($member)"
        }

        public class IsNotNull(member: Member) : Expression(member) {
            override fun hashCode(): Int = member.hashCode()

            override fun equals(other: Any?): Boolean =
                other === this || other is IsNull && other.member == member

            override fun toString(): String = "isNotNull($member)"
        }
    }

    public data class Via(val description: EventDescription, val filter: Filter) : Filter() {
        override fun toString(): String = "via(${description.name}) {$filter}"
    }
}

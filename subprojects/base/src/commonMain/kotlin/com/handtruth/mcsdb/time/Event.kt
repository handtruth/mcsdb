package com.handtruth.mcsdb.time

import com.handtruth.mc.types.*
import com.handtruth.mcsdb.InternalMcsdbApi
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.Kind
import com.handtruth.mcsdb.schema.emptyEventInfo
import com.handtruth.mcsdb.space.Particle
import kotlinx.datetime.Instant

public interface EventInfo {
    public val timestamp: Instant?
    public val throwable: Throwable?
    public val error: String?
}

public val EventInfo.succeeded: Boolean get() = error === null
public val EventInfo.isPrototype: Boolean get() = timestamp === null

internal data class DynamicEventInfo(
    override val timestamp: Instant?,
    override val throwable: Throwable?,
    override val error: String?
) : EventInfo

public interface Event {

    @InternalMcsdbApi
    @Suppress("unused", "FunctionName")
    public fun `$description$`(): EventDescription

    @InternalMcsdbApi
    @Suppress("unused", "FunctionName")
    public fun `$data$`(): Dynamic

    @InternalMcsdbApi
    @Suppress("unused", "FunctionName")
    public fun `$info$`(): EventInfo

    @InternalMcsdbApi
    @Suppress("unused", "FunctionName")
    public fun `$reverse$`(): Event?
}

public fun descriptionOf(event: Event): EventDescription = event.`$description$`()
public fun dataOf(event: Event): Dynamic = event.`$data$`()
public fun infoOf(event: Event): EventInfo = event.`$info$`()
public fun cancel(event: Event): Event? = event.`$reverse$`()

public fun Event.toParticle(): Particle {
    val description = descriptionOf(this)
    val data = dataOf(this)
    val particleData = buildDynamic {
        description.allRealFields.forEach { field ->
            field.name assign data.getOrNull(field.name)
        }
    }
    return Particle(description, particleData)
}

public class AnyEvent internal constructor(
    private val description: EventDescription,
    private val data: Dynamic,
    private val info: EventInfo
) : Event {
    override fun `$description$`(): EventDescription = description

    override fun `$data$`(): Dynamic = data

    override fun `$info$`(): EventInfo = info

    override fun `$reverse$`(): AnyEvent? {
        return if (description.kind == Kind.Direct) {
            val newData = data.toMutableDynamic()
            newData["direction"] = !(newData["direction"] as Boolean)
            AnyEvent(description, newData, emptyEventInfo)
        } else {
            null
        }
    }

    override fun equals(other: Any?): Boolean =
        other === this || other is AnyEvent && description == other.description &&
            data.contentDeepEquals(other.data)

    override fun hashCode(): Int = description.hashCode() + data.hashCode()

    override fun toString(): String = buildString {
        append(description.name)
        append(data.toString())
        val timestamp = info.timestamp
        if (timestamp !== null) {
            append('[')
            val error = info.error
            if (error !== null) {
                append("error = \"")
                val i = error.indexOf('\n')
                val trimmedError = if (i == -1) {
                    error
                } else {
                    error.substring(0, i).removeSuffix("\r") + "..."
                }.trim().replace("\"", "\\\"")
                append(trimmedError)
                append("\", ")
            }
            append("timestamp = ")
            append(timestamp.toString())
            append(']')
        }
    }
}

package com.handtruth.mcsdb.schema

import com.handtruth.mcsdb.space.Particle
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.TimeException
import com.handtruth.mcsdb.variability.State
import kotlin.Boolean
import kotlin.ByteArray
import kotlin.String
import kotlin.reflect.KClass

/**
 * Kind of [Event].
 */
public enum class Kind {
    /**
     * Not an actual event. Interfaces are used to describe relations between final
     * events in space and time.
     *
     * Interface description is an [EventDescription] with Interface kind.
     * Interface in MCSDB is not a type but an object that exists in space only when
     * a particle with [Roles.Source] role is connected to it.
     * You should think of interface like you do in circuit design and not like
     * Kotlin interface type. There are 2 roles for particles on each interface in space.
     * Each particle can be connected to interface with ether [Roles.Source] or [Roles.Target]
     * role. Role [Roles.Fantom] do not participate in space.
     *
     * @see Roles
     */
    Interface,

    /**
     * Irreversible event.
     *
     * Events that should be used to unite other events in reaction
     * process or to involve application parts and actions in MCSDB event processing.
     * Facts are regular events that you should be familiar with, if you worked with
     * event driven architecture.
     *
     * All interface role connections in facts are considered to be [Roles.Fantom]
     * role. If some interfaces do not exist in space then event *will not happen*
     * (see [TimeException.Codes.InterfaceNotExists]).
     */
    Fact,

    /**
     * Reversible events.
     *
     * This events forms space and particle relations. One should use
     * them in purpose to describe reversible action. Reactor defined for reversible event
     * is helpful for synchronisation with outside storage systems.
     *
     * Every successful direct event creates or destroys a [Particle] in space.
     * Direct events has additional field named `direction` of type [Boolean] that only
     * appears in time. If `direction = true` then a new particle will be created in space,
     * otherwise the same particle will be destroyed.
     *
     * The same particle can not exist in space of the same [State] twice nor be deleted
     * from there without be here (see [TimeException.Codes.ParticleExists] and
     * [TimeException.Codes.ParticleNotExists]). If one try to raise an event that would
     * violate this condition then event *will not happen*.
     */
    Direct,
}

/**
 * [Event] role on connected interface. Can be applied with [Modifier.Role].
 */
public enum class Roles {
    Source,
    Fantom,
    Target,
}

public sealed class Modifier {
    public interface BoundType

    public interface EventBound : BoundType
    public interface InheritanceBound : BoundType
    public interface FieldBound : BoundType

    public data class Role(val role: Roles) : Modifier(), InheritanceBound, EventBound

    public data class Unique(val unique: Boolean) : Modifier(), EventBound

    public object AutoIncrement : Modifier(), FieldBound {
        override fun toString(): String = "AutoIncrement"
    }

    public object Nullable : Modifier(), FieldBound {
        override fun toString(): String = "Nullable"
    }

    public object Indexed : Modifier(), FieldBound {
        override fun toString(): String = "Indexed"
    }

    public companion object {
        public val Source: Role = Role(Roles.Source)
        public val Fantom: Role = Role(Roles.Fantom)
        public val Target: Role = Role(Roles.Target)
        public val Unique: Unique = Unique(true)
        public val Shared: Unique = Unique(false)
    }
}

public enum class Types(public val kotlinClass: KClass<*>?) {
    Serial(com.handtruth.mcsdb.types.Serial::class),
    Boolean(kotlin.Boolean::class),
    Byte(kotlin.Byte::class),
    Short(kotlin.Short::class),
    Int(kotlin.Int::class),
    Long(kotlin.Long::class),
    UByte(kotlin.UByte::class),
    UShort(kotlin.UShort::class),
    UInt(kotlin.UInt::class),
    ULong(kotlin.ULong::class),
    Float(kotlin.Float::class),
    Double(kotlin.Double::class),
    Char(kotlin.Char::class),
    String(kotlin.String::class),
    UUID(com.handtruth.mc.types.UUID::class),
    Instant(kotlinx.datetime.Instant::class),
    Structure(null),
    Arbitrary(ByteArray::class),
    ;
}

public data class Member(val description: EventDescription, val field: FieldDescription) {
    override fun toString(): String = "${description.name}::${field.name}"
}

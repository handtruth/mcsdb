package com.handtruth.mcsdb.time

import com.handtruth.mc.graph.Graph
import com.handtruth.mc.graph.MutableGraph
import com.handtruth.mcsdb.schema.CausalRelationships

public typealias History = Graph<Event, CausalRelationships>
public typealias MutableHistory = MutableGraph<Event, CausalRelationships>

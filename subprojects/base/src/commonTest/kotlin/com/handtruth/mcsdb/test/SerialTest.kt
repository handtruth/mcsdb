package com.handtruth.mcsdb.test

import com.handtruth.mcsdb.types.Serial
import kotlin.test.Test
import kotlin.test.assertEquals

class SerialTest {

    @Test
    fun serialTest() {
        val serial = Serial(0x0f4351)
        assertEquals(3, serial.size)
        assertEquals("0F4351", serial.toString())
        val other = Serial.parse("0f4351")
        assertEquals(serial, other)
    }
}

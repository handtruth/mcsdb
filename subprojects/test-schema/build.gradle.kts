plugins {
    kotlin("plugin.serialization")
}

dependencies {
    val commonMainApi by configurations.getting

    commonMainApi(project(":mcsdb-static-api"))
}

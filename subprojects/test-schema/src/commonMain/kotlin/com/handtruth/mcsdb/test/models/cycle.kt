package com.handtruth.mcsdb.test.models

import com.handtruth.mcsdb.schema.Source
import com.handtruth.mcsdb.time.DirectEvent
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.EventCompanion

interface IfaceEvent : Event {
    companion object : EventCompanion()
}

interface OtherEvent : Event {
    companion object : EventCompanion()
}

data class OneEvent(
    override val direction: Boolean
) : @Source IfaceEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class TwoEvent(
    override val direction: Boolean
) : IfaceEvent, @Source OtherEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class ThreeEvent(
    override val direction: Boolean
) : OtherEvent, @Source IfaceEvent, DirectEvent() {
    companion object : EventCompanion()
}

package com.handtruth.mcsdb.test

import com.handtruth.mcsdb.schema.*
import com.handtruth.mcsdb.time.DirectEvent
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.EventCompanion
import com.handtruth.mcsdb.time.FactEvent

@Unique
@Fantom
interface ActorEvent : Event {
    @AutoIncrement
    val actor: Int

    companion object : EventCompanion()
}

@WithName("SystemActor")
data class SystemActorEvent(
    override val actor: Int = 0,
    override val direction: Boolean
) : @Source ActorEvent, DirectEvent() {
    companion object : EventCompanion()
}

@Unique
interface UserEvent : Event {
    @Indexed
    val user: String

    companion object : EventCompanion()
}

data class UserContextEvent(
    override val user: String,
    override val actor: Int,
    override val direction: Boolean
) : @Source UserEvent, ActorEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class UserActorEvent(
    override val actor: Int = 0,
    override val user: String,
    override val direction: Boolean
) : @Source ActorEvent, UserEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class UserCreationEvent(
    @WithName("user")
    val name: String,
    override val actor: Int
) : ActorEvent, FactEvent() {
    companion object : EventCompanion()
}

@Unique
interface GroupEvent : Event {
    val group: String

    companion object : EventCompanion()
}

data class GroupContextEvent(
    override val group: String,
    override val actor: Int,
    override val direction: Boolean
) : @Source GroupEvent, ActorEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class GroupDescriptionEvent(
    val description: String,
    override val group: String,
    override val direction: Boolean
) : GroupEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class GroupActorEvent(
    override val actor: Int = 0,
    override val group: String,
    override val direction: Boolean
) : @Source ActorEvent, GroupEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class GroupCreationEvent(
    @WithName("group")
    val name: String,
    override val actor: Int,
    val description: String,
) : ActorEvent, FactEvent() {
    companion object : EventCompanion()
}

data class GroupMemberEvent(
    override val group: String,
    override val user: String,
    override val actor: Int,
    override val direction: Boolean
) : GroupEvent, UserEvent, ActorEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class MessageEvent(
    val message: String,
    override val user: String,
    override val actor: Int
) : UserEvent, ActorEvent, FactEvent() {
    companion object : EventCompanion()
}

val packageName = "com.handtruth.mcsdb.test"

val actorEvent = interfaceEvent("$packageName.Actor") {
    +Modifier.Unique
    +Modifier.Fantom
    field<Int>("actor") {
        +Modifier.AutoIncrement
    }
}

val systemActorEvent = directEvent("SystemActor") {
    source(actorEvent)
}

val userEvent = interfaceEvent("$packageName.User") {
    +Modifier.Unique
    field("user", Types.String) {
        +Modifier.Indexed
    }
}

val userContextEvent = directEvent("$packageName.UserContext") {
    source(userEvent)
    inherit(actorEvent)
}

val userActorEvent = directEvent("$packageName.UserActor") {
    source(actorEvent)
    inherit(userEvent)
}

val userCreationEvent = factEvent("$packageName.UserCreation") {
    inherit(actorEvent)
    field("user", Types.String)
}

val groupEvent = interfaceEvent("$packageName.Group") {
    +Modifier.Unique
    field("group", Types.String)
}

val groupContextEvent = directEvent("$packageName.GroupContext") {
    source(groupEvent)
    inherit(actorEvent)
}

val groupDescriptionEvent = directEvent("$packageName.GroupDescription") {
    inherit(groupEvent)
    field<String>("description")
}

val groupActorEvent = directEvent("$packageName.GroupActor") {
    source(actorEvent)
    inherit(groupEvent)
}

val groupCreationEvent = factEvent("$packageName.GroupCreation") {
    inherit(actorEvent)
    field<String>("description")
    field("group", Types.String)
}

val groupMemberEvent = directEvent("$packageName.GroupMember") {
    inherit(groupEvent)
    inherit(userEvent)
    inherit(actorEvent)
}

val messageEvent = factEvent("$packageName.Message") {
    inherit(userEvent)
    inherit(actorEvent)

    field<String>("message")
}

val dynamicMCSManSchema = listOf(
    actorEvent, systemActorEvent, userEvent, userContextEvent, userActorEvent, userCreationEvent,
    groupEvent, groupContextEvent, groupDescriptionEvent, groupActorEvent, groupCreationEvent,
    groupMemberEvent, messageEvent
)

package com.handtruth.mcsdb.test.models

import com.handtruth.mc.types.UUID
import com.handtruth.mcsdb.schema.Arbitrary
import com.handtruth.mcsdb.schema.AutoIncrement
import com.handtruth.mcsdb.schema.Source
import com.handtruth.mcsdb.schema.Target
import com.handtruth.mcsdb.time.DirectEvent
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.EventCompanion
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

interface ImageEvent : Event {
    @AutoIncrement
    val image: Long

    companion object : EventCompanion()
}

@Serializable
data class ImageData(
    val name: String,
    val bitmap: ByteArray,
    val width: Int,
    val height: Int,
    val attributes: Map<String, Coordinates>
) {
    @Serializable
    data class Coordinates(val x: Float, val y: Float, val z: Float)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as ImageData

        if (name != other.name) return false
        if (!bitmap.contentEquals(other.bitmap)) return false
        if (width != other.width) return false
        if (height != other.height) return false
        if (attributes != other.attributes) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + bitmap.contentHashCode()
        result = 31 * result + width
        result = 31 * result + height
        result = 31 * result + attributes.hashCode()
        return result
    }
}

data class ImageContextEvent(
    val data: ImageData,
    override val image: Long,
    override val direction: Boolean
) : @Source ImageEvent, DirectEvent() {
    companion object : EventCompanion()
}

@Serializable
data class Stamp(val stamp: String)

data class TotallyOrdinaryDependantEvent(
    override val image: Long,
    val boolean: Boolean?,
    val byte: Byte,
    val short: Short?,
    val int: Int,
    val long: Long?,
    val float: Float,
    val double: Double?,
    @AutoIncrement
    val char: Char,
    // heavy
    val uuid: UUID,
    val instant: Instant,
    @Arbitrary
    val stamp: Stamp,
    override val direction: Boolean
) : @Target ImageEvent, DirectEvent() {
    companion object : EventCompanion()
}

package com.handtruth.mcsdb.test.models

import com.handtruth.mcsdb.time.DirectEvent
import com.handtruth.mcsdb.time.EventCompanion

data class ChainEvent(override val direction: Boolean) : DirectEvent() {
    companion object : EventCompanion()
}

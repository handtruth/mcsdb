package com.handtruth.mcsdb.test.models

import com.handtruth.mcsdb.schema.Source
import com.handtruth.mcsdb.schema.directEvent
import com.handtruth.mcsdb.schema.field
import com.handtruth.mcsdb.schema.interfaceEvent
import com.handtruth.mcsdb.time.DirectEvent
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.EventCompanion
import com.handtruth.mcsdb.types.Serial

private const val packageName = "com.handtruth.mcsdb.test.books"

val bookshelfEvent = interfaceEvent("$packageName.Bookshelf") {
    field<Serial>("bookshelf")
}

val bookshelfContextEvent = directEvent("$packageName.BookshelfContext") {
    source(bookshelfEvent)
    field<String>("location")
}

val bookEvent = directEvent("$packageName.Book") {
    inherit(bookshelfEvent)
    field<String>("name")
}

interface BookshelfEvent : Event {
    val bookshelf: Serial
    companion object : EventCompanion()
}

data class BookshelfContextEvent(
    val location: String,
    override val bookshelf: Serial = Serial.nil,
    override val direction: Boolean
) : @Source BookshelfEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class BookEvent(
    val name: String,
    override val bookshelf: Serial,
    override val direction: Boolean
) : BookshelfEvent, DirectEvent() {
    companion object : EventCompanion()
}

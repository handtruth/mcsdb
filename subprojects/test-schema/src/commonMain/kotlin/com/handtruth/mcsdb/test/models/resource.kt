package com.handtruth.mcsdb.test.models

import com.handtruth.mcsdb.schema.Source
import com.handtruth.mcsdb.schema.Types
import com.handtruth.mcsdb.schema.directEvent
import com.handtruth.mcsdb.schema.interfaceEvent
import com.handtruth.mcsdb.time.DirectEvent
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.EventCompanion
import com.handtruth.mcsdb.types.Serial

interface SessionEvent : Event {
    val session: Serial
    companion object : EventCompanion()
}

data class SessionContextEvent(
    override val session: Serial,
    override val direction: Boolean
) : SessionEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class ConnectionEvent(
    val connection: Serial = Serial.nil,
    override val session: Serial = Serial.nil,
    override val direction: Boolean
) : @Source SessionEvent, DirectEvent() {
    companion object : EventCompanion()
}

private const val thisPackage = "com.handtruth.mcsdb.test.books"

val sessionEvent = interfaceEvent("$thisPackage.Session") {
    field("session", Types.Serial)
}

val sessionContextEvent = directEvent("$thisPackage.SessionContext") {
    inherit(sessionEvent)
}

val connectionEvent = directEvent("$thisPackage.Connection") {
    source(sessionEvent)
    field("connection", Types.Serial)
}

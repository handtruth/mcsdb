import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    application
}

application {
    mainClass.set("com.handtruth.mcsdb.viewer.MCSDBViewerApp")
}

kotlin.sourceSets.all {
    with(languageSettings) {
        useExperimentalAnnotation("kotlin.RequiresOptIn")
    }
}

dependencies {
    val platformVersion: String by project
    implementation(platform("com.handtruth.internal:platform:$platformVersion"))

    implementation("com.handtruth.kommon:kommon-log")

    implementation("no.tornado:tornadofx:1.7.20")
    runtimeOnly("org.xerial:sqlite-jdbc")
    implementation("com.github.brunomnsilva:JavaFXSmartGraph:0.9.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx")
    implementation(project(":mcsdb-encoding"))
    implementation(project(":mcsdb-api"))
    implementation(project(":mcsdb-storage"))
    runtimeOnly("ch.qos.logback:logback-classic")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

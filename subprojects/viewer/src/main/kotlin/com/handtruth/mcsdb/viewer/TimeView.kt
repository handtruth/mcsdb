package com.handtruth.mcsdb.viewer

import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel
import com.handtruth.mc.types.get
import com.handtruth.mcsdb.schema.Kind
import com.handtruth.mcsdb.schema.Types
import com.handtruth.mcsdb.time.dataOf
import com.handtruth.mcsdb.time.descriptionOf
import com.handtruth.mcsdb.time.infoOf
import javafx.beans.property.SimpleListProperty
import javafx.geometry.Orientation
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import tornadofx.*

class TimeView : View() {
    private val controller: AppController by inject()

    private val selectedEvent get() = controller.selectedEventProperty

    override val root = splitpane(Orientation.HORIZONTAL) {
        form {
            removeWhen(selectedEvent.isNull)
            fieldset("Head") {
                field("Internal ID") {
                    val id = selectedEvent.stringBinding { e -> e?.let { dataOf(it)[".event"].toString() } }
                    textfield(id)
                }
                field("Class") {
                    val className = selectedEvent.stringBinding { e -> e?.let { descriptionOf(it).name } }
                    button(className) {
                        action {
                            val name = className.value
                            if (name != null) {
                                controller.selectedClass = controller.schema[name]
                                controller.selectedSide = Sides.Schema
                            }
                        }
                    }
                }
                val extras = selectedEvent.objectBinding { it?.run { infoOf(it) } }
                field("Timestamp") {
                    val timestamp = extras.stringBinding {
                        it?.timestamp?.toLocalDateTime(TimeZone.currentSystemDefault())?.toString()
                    }
                    textfield(timestamp)
                }
                field("Error") {
                    val error = extras.stringBinding { it?.error }
                    disableWhen(error.isNull)
                    textfield(error)
                }
            }
            fieldset("Data") {
                val data = SimpleListProperty(observableListOf<FieldProperty>())

                selectedEvent.onChange { event ->
                    if (event == null) {
                        data.value = observableListOf()
                    } else {
                        val datum = dataOf(event)
                        val newData = mutableListOf<FieldProperty>()
                        descriptionOf(event).allFields.mapTo(newData) {
                            FieldProperty(it.name, it.type, datum.getOrNull(it.name)?.toString())
                        }
                        if (descriptionOf(event).kind == Kind.Direct) {
                            newData += FieldProperty("direction", Types.Boolean, datum["direction"].toString())
                        }
                        data.value = newData.asObservable()
                    }
                }

                tableview(data) {
                    readonlyColumn("Name", FieldProperty::name)
                    readonlyColumn("Type", FieldProperty::type)
                    readonlyColumn("Value", FieldProperty::value)

                    columnResizePolicy = SmartResize.POLICY
                }
            }

            selectedEvent.addListener { _, oldValue, newValue ->
                if (oldValue != null) {
                    removeClass(Styles.ofKind(descriptionOf(oldValue).kind))
                }
                if (newValue != null) {
                    addClass(Styles.ofKind(descriptionOf(newValue).kind))
                }
            }
        }

        var graph: SmartGraphPanel<Long, Int>

        fun buildGraph() = graphpanel(controller.getEventGraph(), controller.graphAutoLayout) {
            setVertexDoubleClickAction { vertex ->
                selectedEvent.value = controller.getEvent(vertex.underlyingVertex.element())
            }
        }

        graph = buildGraph()

        controller.stateProperty.onChange {
            getChildList()!!.remove(graph)
            graph = buildGraph()
        }
    }
}

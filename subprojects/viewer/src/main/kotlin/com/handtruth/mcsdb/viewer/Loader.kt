package com.handtruth.mcsdb.viewer

import com.brunomnsilva.smartgraph.graph.Digraph
import com.brunomnsilva.smartgraph.graph.DigraphEdgeList
import com.brunomnsilva.smartgraph.graph.InvalidVertexException
import com.handtruth.kommon.Log
import com.handtruth.kommon.default
import com.handtruth.mc.types.MutableDynamic
import com.handtruth.mc.types.buildMutableDynamic
import com.handtruth.mc.types.get
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.EventDescriptionMapper
import com.handtruth.mcsdb.schema.Kind
import com.handtruth.mcsdb.schema.readEventDescription
import com.handtruth.mcsdb.space.Particle
import com.handtruth.mcsdb.storage.JVMPreparedQuery
import com.handtruth.mcsdb.storage.Transaction
import com.handtruth.mcsdb.storage.buildGetDatumById
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.variability.IntermediateState
import com.handtruth.mcsdb.variability.NamedState
import com.handtruth.mcsdb.variability.State
import io.ktor.utils.io.core.*
import kotlinx.datetime.Instant
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement
import kotlin.io.use

private const val path = "C:\\Users\\ktlo\\Source\\local\\mc\\mcsdb\\example\\storage.db"

data class EventClass(val id: Long, val description: EventDescription) {
    val name get() = description.name
}

class ClassContainer(val all: Collection<EventClass>) {
    private val byId = mutableMapOf<Long, EventClass>()
    private val byName = mutableMapOf<String, EventClass>()

    init {
        all.associateByTo(byId) { it.id }
        all.associateByTo(byName) { it.name }
    }

    operator fun get(id: Long) = byId[id] ?: error("no such class")
    operator fun get(name: String) = byName[name] ?: error("no such class")
}

@OptIn(Transaction::class)
class Loader {
    private val log = Log.default("loader")

    private val connection = DriverManager.getConnection("jdbc:sqlite:$path").apply {
        autoCommit = false
        transactionIsolation = Connection.TRANSACTION_SERIALIZABLE
    }

    private val getSchema = connection.prepareStatement(
        "select id, name, description from class"
    )

    private fun loadSchema(): ClassContainer {
        val loaded = mutableMapOf<String, EventClass>()
        val getDescription = EventDescriptionMapper { loaded[it]!!.description }
        getSchema.executeQuery().use { result ->
            while (result.next()) {
                val id = result.getLong(1)
                val name = result.getString(2)
                val descriptionBlob = result.getBytes(3)
                val description = ByteReadPacket(descriptionBlob).use { input ->
                    input.readEventDescription(getDescription)
                }
                loaded[name] = EventClass(id, description)
            }
        }
        return ClassContainer(loaded.values)
    }

    val schema by lazy { loadSchema() }

    private fun prepareSchemaGraph(): Digraph<Long, Int> {
        val graph = DigraphEdgeList<Long, Int>()
        for (entry in schema.all) {
            graph.insertVertex(entry.id)
        }
        var edge = 0
        for (entry in schema.all) {
            for (eventInterface in entry.description.inherits) {
                val other = schema[eventInterface.description.name]
                graph.insertEdge(entry.id, other.id, ++edge)
            }
        }
        return graph
    }

    val schemaGraph by lazy { prepareSchemaGraph() }

    private val getDefaultState = connection.prepareStatement(
        """
        select id, name, reactive from state
        where name = (select value from mcsdb_properties where name = 'default_state');
        """
    )

    fun getDefaultState(): NamedState {
        getDefaultState.executeQuery().use { result ->
            check(result.next())
            val id = result.getLong(1)
            val name = result.getString(2)!!
            val reactive = result.getBoolean(3)
            return NamedState(id, name, reactive)
        }
    }

    private val collectParticles = connection.prepareStatement(
        """
        select particle from state_X_particle
        where state = ?;
        """
    )

    private val collectParticleEdges = connection.prepareStatement(
        """
        with state_interfaces (interface, particle, role) as (
            select interface, particle, role from state_X_particle as sXp
            inner join interface_X_datum as iXd on sXp.particle = iXd.datum
            where state = ?
        ) select source.particle, target.particle from state_interfaces as source
        inner join state_interfaces as target on source.interface = target.interface
        where source.role = 1 and target.role = 3;
        """
    )

    private fun collectVertices(graph: Digraph<Long, Int>, statement: PreparedStatement, state: State) {
        statement.setLong(1, state.id)
        statement.executeQuery().use { result ->
            while (result.next()) {
                graph.insertVertex(result.getLong(1))
            }
        }
    }

    private fun collectEdges(graph: Digraph<Long, Int>, statement: PreparedStatement, state: State) {
        statement.setLong(1, state.id)
        statement.executeQuery().use { result ->
            var cnt = 0
            while (result.next()) {
                try {
                    graph.insertEdge(result.getLong(1), result.getLong(2), ++cnt)
                } catch (e: InvalidVertexException) {
                    log.error(e)
                }
            }
        }
    }

    private fun loadGraph(state: State, vertices: PreparedStatement, edges: PreparedStatement): Digraph<Long, Int> {
        val graph = DigraphEdgeList<Long, Int>()
        collectVertices(graph, vertices, state)
        collectEdges(graph, edges, state)
        return graph
    }

    fun loadParticlesGraph(state: State): Digraph<Long, Int> {
        return loadGraph(state, collectParticles, collectParticleEdges)
    }

    private val collectEvents = connection.prepareStatement(
        """
        select event.id from event
        inner join state_X_event as sXe on sXe.event = event.id
        where state = ?;
        """
    )

    private val collectEventEdges = connection.prepareStatement(
        """
        select source, target from event_X_event as eXe
        inner join state_X_event as sXe on event = target
        where state = ?;
        """
    )

    fun loadEventGraph(state: State): Digraph<Long, Int> {
        return loadGraph(state, collectEvents, collectEventEdges)
    }

    private fun loadDatum(id: Long, eventClass: EventClass): MutableDynamic {
        @Suppress("SqlResolve")
        val query = buildString { buildGetDatumById(eventClass.description) }
        return JVMPreparedQuery(connection.prepareStatement(query)).use { statement ->
            statement.executeQuery {
                statement.setLong(id)
            }.use { result ->
                check(result.next())
                check(id == result.getLong())
                buildMutableDynamic {
                    for (field in eventClass.description.allFields) {
                        field.name assign result[field.type]
                    }
                    ".datum" assign id
                }
            }
        }
    }

    private val retrieveParticleClass = connection.prepareStatement(
        "select class from datum where id = ?;"
    )

    fun loadParticle(id: Long): Particle {
        retrieveParticleClass.setLong(1, id)
        val eventClass: EventClass
        retrieveParticleClass.executeQuery().use { result ->
            check(result.next())
            eventClass = schema[result.getLong(1)]
        }
        val data = loadDatum(id, eventClass)
        data[".particle"] = id
        return Particle(eventClass.description, data)
    }

    private val retrieveEventClass = connection.prepareStatement(
        """
        select class, datum, timestamp, error from datum
        inner join event on event.datum = datum.id
        where event.id = ?;
        """
    )

    private val getEventDirection = connection.prepareStatement(
        "select direction from direct_event where id = ?;"
    )

    fun loadEvent(id: Long): Event {
        retrieveEventClass.setLong(1, id)
        val eventClass: EventClass
        val datum: Long
        val timestamp: Instant
        val error: String?
        retrieveEventClass.executeQuery().use { result ->
            check(result.next()) { "event: $id" }
            eventClass = schema[result.getLong(1)]
            datum = result.getLong(2)
            timestamp = Instant.parse(result.getString(3))
            error = result.getString(4)
        }
        val data = loadDatum(datum, eventClass)
        data[".event"] = id
        if (eventClass.description.kind == Kind.Direct) {
            getEventDirection.setLong(1, id)
            getEventDirection.executeQuery().use { result ->
                check(result.next())
                data["direction"] = result.getBoolean(1)
            }
        }
        return eventClass.description.event(timestamp, error, data)
    }

    private val collectStates = connection.prepareStatement("select id from state;")

    private val collectStateEdges = connection.prepareStatement("select parent, child from state_X_state;")

    private fun loadStateGraph(): Digraph<Long, Int> {
        val graph = DigraphEdgeList<Long, Int>()
        collectStates.executeQuery().use { result ->
            while (result.next()) {
                graph.insertVertex(result.getLong(1))
            }
        }
        collectStateEdges.executeQuery().use { result ->
            var cnt = 0
            while (result.next()) {
                graph.insertEdge(result.getLong(1), result.getLong(2), ++cnt)
            }
        }
        return graph
    }

    val stateGraph by lazy { loadStateGraph() }

    private val retrieveState = connection.prepareStatement(
        "select name, reactive from state where id = ?;"
    )

    fun loadState(id: Long): State {
        retrieveState.setLong(1, id)
        retrieveState.executeQuery().use { result ->
            check(result.next())
            val name = result.getString(1)
            val reactive = result.getBoolean(2)
            return if (name == null) {
                IntermediateState(id)
            } else {
                NamedState(id, name, reactive)
            }
        }
    }

    private val getLatestEventByDatum = connection.prepareStatement(
        """
        with recursive subgraph ( event, level ) as (
            select max(event.id) as event, 1 as level from event
            inner join state_X_event on event.id = state_X_event.event
            where state = ? and datum = ?
        union
            select event.id, level+1 from event
            inner join state_X_event on event.id = state_X_event.event
            inner join event_X_event on event.id = event_X_event.target
            inner join subgraph on event_X_event.source = subgraph.event
            where event.error is null and state = ?
        ) select event from subgraph
        inner join event on event = id
        where datum = ?
        order by level limit 1;
        """
    )

    fun findEventOfParticle(particle: Particle, state: State): Event? {
        val datum = particle.data[".datum"] as Long
        getLatestEventByDatum.setLong(1, state.id)
        getLatestEventByDatum.setLong(2, datum)
        getLatestEventByDatum.setLong(3, state.id)
        getLatestEventByDatum.setLong(4, datum)
        val event = getLatestEventByDatum.executeQuery().use { result ->
            if (!result.next()) {
                return null
            }
            result.getLong(1)
        }
        return loadEvent(event)
    }
}

package com.handtruth.mcsdb.viewer

import com.handtruth.mcsdb.schema.FieldDescription
import javafx.beans.property.SimpleListProperty
import javafx.geometry.Orientation
import tornadofx.*

class SchemaView : View() {
    private val controller: AppController by inject()

    private val selectedClass get() = controller.selectedClassProperty

    override val root = splitpane(Orientation.HORIZONTAL) {
        form {
            removeWhen(selectedClass.isNull)
            fieldset("Head") {
                field("Name") {
                    val name = selectedClass.stringBinding { it?.description?.name }
                    textfield(name)
                }
                field("ID") {
                    val name = selectedClass.stringBinding { it?.id?.toString() }
                    textfield(name)
                }
                field("Kind") {
                    val kind = selectedClass.stringBinding { it?.description?.kind?.name }
                    textfield(kind)
                }
                field("Modifiers") {
                    val modifiers = selectedClass.stringBinding { d ->
                        d?.description?.modifiers?.joinToString(separator = " ") { "@$it" }
                    }
                    textfield(modifiers)
                }
            }

            fieldset("Fields") {
                val fields = SimpleListProperty(observableListOf<FieldDescription>())

                selectedClass.onChange {
                    if (it == null) {
                        fields.value = observableListOf()
                    } else {
                        fields.value = it.description.fields.asObservable()
                    }
                }

                tableview(fields) {
                    //removeWhen(fields.emptyProperty())
                    readonlyColumn("Name", FieldDescription::name)
                    readonlyColumn("Type", FieldDescription::type)
                    readonlyColumn("Modifiers", FieldDescription::modifiers)

                    columnResizePolicy = SmartResize.POLICY
                }
            }

            fieldset("Interfaces") {
                val inherits = SimpleListProperty(observableListOf<String>())

                selectedClass.onChange {
                    if (it == null) {
                        inherits.value = observableListOf()
                    } else {
                        inherits.value = it.description.inherits.map { i -> i.description.name }.asObservable()
                    }
                }

                listview(inherits) {
                    onUserSelect {
                        selectedClass.value = controller.schema[it]
                    }
                }
            }

            selectedClass.addListener { _, oldValue, newValue ->
                if (oldValue != null) {
                    removeClass(Styles.ofKind(oldValue.description.kind))
                }
                if (newValue != null) {
                    addClass(Styles.ofKind(newValue.description.kind))
                }
            }
        }
        graphpanel(controller.schemaGraph, controller.graphAutoLayout) {
            for (eventClass in controller.schema.all) {
                getStylableVertex(eventClass.id).setStyle(".schemaVertex")
            }

            setVertexDoubleClickAction { vertex ->
                selectedClass.value?.let { oldClass ->
                    val oldVertex = getStylableVertex(oldClass.id)
                    oldVertex.setStyle(".schemaVertex")
                }
                val newClass = controller.schema[vertex.underlyingVertex.element()]
                vertex.setStyle(".selectedSchemaVertex")
                selectedClass.value = newClass
            }
        }
    }
}

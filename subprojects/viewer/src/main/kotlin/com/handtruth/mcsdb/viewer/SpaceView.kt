package com.handtruth.mcsdb.viewer

import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel
import com.handtruth.mc.types.get
import com.handtruth.mcsdb.space.Particle
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Orientation
import javafx.scene.control.Alert
import tornadofx.*

class SpaceView : View() {
    private val controller: AppController by inject()

    private val selectedParticle = SimpleObjectProperty(null as Particle?)

    override val root = splitpane(Orientation.HORIZONTAL) {
        form {
            removeWhen(selectedParticle.isNull)
            fieldset("Head") {
                field("Internal ID") {
                    val id = selectedParticle.stringBinding { p -> p?.let { it.data[".particle"].toString() } }
                    textfield(id)
                }
                field("Class") {
                    val className = selectedParticle.stringBinding { it?.description?.name }
                    button(className) {
                        action {
                            val name = className.value
                            if (name != null) {
                                controller.selectedClass = controller.schema[name]
                                controller.selectedSide = Sides.Schema
                            }
                        }
                    }
                }
            }

            fieldset("Data") {
                val data = SimpleListProperty(observableListOf<FieldProperty>())

                selectedParticle.onChange { particle ->
                    if (particle == null) {
                        data.value = observableListOf()
                    } else {
                        data.value = particle.description.allRealFields.map {
                            FieldProperty(it.name, it.type, particle.data.getOrNull(it.name)?.toString())
                        }.asObservable()
                    }
                }

                tableview(data) {
                    readonlyColumn("Name", FieldProperty::name)
                    readonlyColumn("Type", FieldProperty::type)
                    readonlyColumn("Value", FieldProperty::value)

                    columnResizePolicy = SmartResize.POLICY
                }
            }

            buttonbar {
                button("Goto Event") {
                    action {
                        selectedParticle.value?.let { particle ->
                            val event = controller.findEventOfParticle(particle)
                            if (event == null) {
                                alert(
                                    Alert.AlertType.INFORMATION,
                                    "Attention",
                                    "No event found that created this particle"
                                )
                            } else {
                                controller.selectedEvent = event
                                controller.selectedSide = Sides.Time
                            }
                        }
                    }
                }
            }

            addClass(Styles.particle)
        }

        var graph: SmartGraphPanel<Long, Int>

        fun buildGraph() = graphpanel(controller.getParticleGraph(), controller.graphAutoLayout) {
            setVertexDoubleClickAction { vertex ->
                selectedParticle.value = controller.getParticle(vertex.underlyingVertex.element())
            }
        }

        graph = buildGraph()

        controller.stateProperty.onChange {
            getChildList()!!.remove(graph)
            graph = buildGraph()
        }
    }
}

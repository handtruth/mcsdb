package com.handtruth.mcsdb.viewer

import com.handtruth.mcsdb.schema.Types

data class FieldProperty(val name: String, val type: Types, val value: String?)

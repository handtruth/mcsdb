package com.handtruth.mcsdb.viewer

enum class Sides {
    Schema, Time, Space, Variability
}

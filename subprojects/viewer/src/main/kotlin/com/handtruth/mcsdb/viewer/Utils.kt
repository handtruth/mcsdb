package com.handtruth.mcsdb.viewer

import com.brunomnsilva.smartgraph.graph.Graph
import com.brunomnsilva.smartgraph.graph.GraphEdgeList
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel
import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import javafx.scene.control.CheckBox
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tornadofx.*
import kotlin.error

fun <V : Any, E : Any> EventTarget.graphpanel(
    graph: Graph<V, E> = GraphEdgeList(), autoLayout: ObservableValue<Boolean>,
    op: SmartGraphPanel<V, E>.() -> Unit = {}
) = opcr(this, SmartGraphPanel(graph).apply {
    automaticLayoutProperty().bind(autoLayout)
    GlobalScope.launch(Dispatchers.Main) {
        delay(500)
        init()
    }
}, op)

private fun code2char(code: Int): Char = when (code) {
    in 0..9 -> '0' + code
    in 10..15 -> 'a' + (code - 10)
    else -> error("unknown code")
}

fun stringify(byteArray: ByteArray): String {
    val result = CharArray(byteArray.size shl 1)
    for (i in byteArray.indices) {
        val byte = byteArray[i].toInt() and 0xFF
        result[i shl 1] = code2char(byte ushr 4)
        result[i shl 1 or 1] = code2char(byte and 0xF)
    }
    return result.concatToString()
}

class ReadOnlyCheckBox() : CheckBox() {
    override fun arm() {}
}

fun EventTarget.checkbox(
    observable: ObservableValue<Boolean>,
    op: ReadOnlyCheckBox.() -> Unit = {}
) = opcr(this, ReadOnlyCheckBox().apply { selectedProperty().bind(observable) }, op)

fun TabPane.side(side: Sides, op: Tab.() -> Unit = {}): Tab {
    val tab = Tab(side.name)
    tab.tag = side
    tabs.add(tab)
    tab.isClosable = false
    tab.addClass(Styles.ofSide(side))
    return tab.also(op)
}

package com.handtruth.mcsdb.viewer

import tornadofx.App

class MCSDBViewerApp : App(MainView::class, Styles::class)

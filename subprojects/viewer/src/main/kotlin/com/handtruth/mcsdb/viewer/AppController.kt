package com.handtruth.mcsdb.viewer

import com.brunomnsilva.smartgraph.graph.Digraph
import com.handtruth.mcsdb.space.Particle
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.variability.State
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import tornadofx.*

class AppController : Controller() {
    private val loader = Loader()

    val schema: ClassContainer get() = loader.schema
    val schemaGraph: Digraph<Long, Int> get() = loader.schemaGraph

    val stateGraph: Digraph<Long, Int> get() = loader.stateGraph

    fun getEventGraph(): Digraph<Long, Int> = loader.loadEventGraph(state)
    fun getEvent(id: Long): Event = loader.loadEvent(id)
    fun getParticleGraph(): Digraph<Long, Int> = loader.loadParticlesGraph(state)
    fun getParticle(id: Long): Particle = loader.loadParticle(id)
    fun getState(id: Long): State = loader.loadState(id)
    fun findEventOfParticle(particle: Particle): Event? = loader.findEventOfParticle(particle, state)

    val stateProperty = SimpleObjectProperty<State>(loader.getDefaultState())
    var state: State by stateProperty

    val selectedClassProperty = SimpleObjectProperty(null as EventClass?)
    var selectedClass: EventClass? by selectedClassProperty

    val selectedSideProperty = SimpleObjectProperty(Sides.Schema)
    var selectedSide: Sides by selectedSideProperty


    val selectedEventProperty = SimpleObjectProperty(null as Event?)
    var selectedEvent: Event? by selectedEventProperty

    val graphAutoLayout = SimpleBooleanProperty(true)
}

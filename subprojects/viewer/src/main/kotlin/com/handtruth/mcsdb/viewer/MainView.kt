package com.handtruth.mcsdb.viewer

import tornadofx.*

class MainView : View("[MCSDB] Viewer") {
    private val controller: AppController by inject()

    private val schemaView: SchemaView by inject()
    private val timeView: TimeView by inject()
    private val spaceView: SpaceView by inject()
    private val variabilityView: VariabilityView by inject()

    init {
        val title = controller.selectedSideProperty.stringBinding {
            "[MCSDB] Viewer: " + it?.name
        }
        titleProperty.bind(title)
    }

    override val root = borderpane {
        top = menubar {
            menu {
                checkbox("Auto Layout", controller.graphAutoLayout)
            }
        }
        center = tabpane {
            prefWidth = 1200.0
            prefHeight = 600.0

            val schema = side(Sides.Schema) {
                content = schemaView.root
            }
            val time = side(Sides.Time) {
                content = timeView.root
            }
            val space = side(Sides.Space) {
                content = spaceView.root
            }
            val variability = side(Sides.Variability) {
                content = variabilityView.root
            }

            controller.selectedSideProperty.onChange {
                val tab = when (it) {
                    Sides.Schema -> schema
                    Sides.Time -> time
                    Sides.Space -> space
                    Sides.Variability -> variability
                    null -> null
                }
                tab?.select()
            }

            tabs.forEach { tab ->
                tab.selectedProperty().onChange {
                    if (it) controller.selectedSide = tab.tag as Sides
                }
            }
        }
    }
}

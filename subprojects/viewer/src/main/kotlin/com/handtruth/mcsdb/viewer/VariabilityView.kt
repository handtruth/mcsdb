package com.handtruth.mcsdb.viewer

import com.handtruth.mcsdb.variability.IntermediateState
import com.handtruth.mcsdb.variability.NamedState
import com.handtruth.mcsdb.variability.ReactiveState
import javafx.geometry.Orientation
import tornadofx.*

class VariabilityView : View() {
    private val controller: AppController by inject()

    private val selectedState get() = controller.stateProperty

    override val root = splitpane(Orientation.HORIZONTAL) {
        form {
            fieldset("Head") {
                field("ID") {
                    val id = selectedState.stringBinding { it?.id?.toString() }
                    textfield(id)
                }
                field("Kind") {
                    val kind = selectedState.stringBinding {
                        when (it) {
                            is IntermediateState -> "Intermediate"
                            is NamedState -> "Named"
                            else -> "Unknown"
                        }
                    }
                    textfield(kind)
                }
                field("Reactive") {
                    enableWhen(selectedState.booleanBinding { it is ReactiveState })
                    val reactive = selectedState.booleanBinding {
                        it is ReactiveState && it.reactive
                    }
                    checkbox(reactive)
                }
                field("Name") {
                    val name = selectedState.stringBinding { if (it is NamedState) it.name else null }
                    removeWhen(name.isNull)
                    textfield(name)
                }
            }
        }

        graphpanel(controller.stateGraph, controller.graphAutoLayout) {
            setVertexDoubleClickAction { vertex ->
                selectedState.value = controller.getState(vertex.underlyingVertex.element())
            }
        }
    }
}

package com.handtruth.mcsdb.viewer

import com.handtruth.mcsdb.schema.Kind
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.shape.StrokeLineCap
import javafx.scene.shape.StrokeType
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val schema by cssclass()
        val time by cssclass()
        val space by cssclass()
        val variability by cssclass()

        fun ofSide(side: Sides) = when (side) {
            Sides.Schema -> schema
            Sides.Time -> time
            Sides.Space -> space
            Sides.Variability -> variability
        }

        val interfaceKind by cssclass()
        val factKind by cssclass()
        val directKind by cssclass()
        val particle by cssclass()
        val state by cssclass()

        fun ofKind(kind: Kind) = when (kind) {
            Kind.Interface -> interfaceKind
            Kind.Fact -> factKind
            Kind.Direct -> directKind
        }

        val graph by cssclass()
        val vertex by cssclass()
        val schemaVertex by cssclass()
        val selectedSchemaVertex by cssclass()
        val `vertex-label` by cssclass()
        val edge by cssclass()
        val `edge-label` by cssclass()
        val arrow by cssclass()
        val textBaseColor by cssproperty<Paint>("-fx-text-base-color")
    }

    init {
        schema {
            backgroundColor += c("#647687")
            borderColor += box(c("#314354"))
            textBaseColor.value = Color.WHITE
        }

        time {
            backgroundColor += c("#E3C800")
            borderColor += box(c("#B09500"))
            textBaseColor.value = Color.WHITE
        }

        space {
            backgroundColor += c("#76608A")
            borderColor += box(c("#432D57"))
            textBaseColor.value = Color.WHITE
        }

        variability {
            backgroundColor += c("#6D8764")
            borderColor += box(c("#3A5431"))
            textBaseColor.value = Color.WHITE
        }

        interfaceKind {
            backgroundColor += c("#D5E8D4")
            borderColor += box(c("#82B366"))
        }

        factKind {
            backgroundColor += c("#FFE6CC")
            borderColor += box(c("#D79B00"))
        }

        directKind {
            backgroundColor += c("#FFF2CC")
            borderColor += box(c("#D6B656"))
        }

        particle {
            backgroundColor += c("#DAE8FC")
            borderColor += box(c("#6C8EBF"))
        }

        state {
            backgroundColor += c("#F5F5F5")
            borderColor += box(c("#666666"))
        }

        graph {
            backgroundColor += c("#F4FFFB")
        }

        vertex {
            strokeWidth = 3.pt
            stroke = c("#61B5F1")
            strokeType = StrokeType.INSIDE
            fill = c("#F5F5F5")
        }

        schemaVertex {
            //strokeWidth = 3.pt
            stroke = c("#666666")
            //strokeType = StrokeType.INSIDE
            fill = c("#B1DFF7")
        }

        selectedSchemaVertex {
            //strokeWidth = 3.pt
            stroke = c("#314354")
            //strokeType = StrokeType.INSIDE
            fill = c("#647687")
        }

        `vertex-label` {
            font = Font.font("sans-serif", FontWeight.BOLD, 8.0)
        }

        edge {
            strokeWidth = 2.pt
            stroke = Color.BLACK
            //strokeDashArray = listOf(2.pt, 5.pt, 2.pt, 5.pt) /* remove for clean edges */
            fill = Color.TRANSPARENT
            strokeLineCap = StrokeLineCap.ROUND
            opacity = 0.8
        }

        `edge-label` {
            font = Font.font("sans-serif", FontWeight.NORMAL, 5.0)
        }

        arrow {
            strokeWidth = 2.pt
            stroke = c("#FF6D66")
            opacity = 0.8
        }
    }
}

import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode

kotlin {
    explicitApi = ExplicitApiMode.Strict
}

dependencies {
    commonMainApi(project(":mcsdb-api"))
    commonMainImplementation("io.ktor:ktor-io")
    commonMainImplementation("com.handtruth.mc:tools-nbt")
    jvmImplementation(kotlin("reflect"))

    commonTestImplementation(project(":mcsdb-test-schema"))
    commonTestRuntimeOnly(project(":mcsdb-embedded"))
    commonTestImplementation("io.ktor:ktor-test-dispatcher")
    jvmTestImplementation("guru.nidi:graphviz-kotlin:0.18.1")
    jvmTestRuntimeOnly("ch.qos.logback:logback-classic")
}

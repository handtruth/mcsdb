package com.handtruth.mcsdb.test

import com.handtruth.mc.graph.MutableGraph
import com.handtruth.mcsdb.context.*
import com.handtruth.mcsdb.schema.CausalRelationships
import com.handtruth.mcsdb.schema.Schema
import com.handtruth.mcsdb.space.MutableMatter
import com.handtruth.mcsdb.time.*
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.first
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse

class One : FactEvent() {
    companion object : EventCompanion()
}

class Two : FactEvent() {
    companion object : EventCompanion()
}

class Three : FactEvent() {
    companion object : EventCompanion()
}

data class Four(override val direction: Boolean) : DirectEvent() {
    companion object : EventCompanion()
}

data class Five(override val direction: Boolean) : DirectEvent() {
    companion object : EventCompanion()
}

class TwoException : Exception()

class ReactorTest {

    @Test
    fun failFactsTest() = mcsdbTest {
        Schema.register(One, Two)

        Schema.reactAfter<One> { event ->
            Time.raise(Two())
            event
        }

        Schema.reactAfter<Two> { throw TwoException() }

        val e = assertFailsWith<TwoException> {
            Time.raise(One())
        }
        val events = actualHistory().edges
        assertEquals(1, events.size)
        val edge = events.first()
        assertEquals(One(), edge.target.value)
        assertEquals(Two(), edge.source.value)
        assertEquals(CausalRelationships.Reactive, edge.value)
        val two = edge.source.value
        assertEquals(e.stackTraceToString(), infoOf(two).error)
    }

    @Test
    fun failParticleTest() = mcsdbTest {
        Schema.register(Three, Four)

        Schema.reactAfter<Three> { event ->
            Time.raise(Four(direction = true))
            event
        }

        Time.raise(Three())

        val e = assertFailsWith<TimeException> {
            Time.raise(Three())
        }
        assertEquals(TimeException.Codes.ParticleExists, e.code)

        val history = actualHistory()
        assertEquals(1, history.edges.size)
        val successSideEdges = history.getVertices(Three()).first { infoOf(it.value).succeeded }.edges
        assertEquals(1, successSideEdges.size)
        val errorSideEdges = history.getVertices(Three()).first { !infoOf(it.value).succeeded }.edges
        assertEquals(0, errorSideEdges.size)
    }

    @Test
    fun reactiveSequence() = mcsdbTest {
        Schema.register(Three, Four, Five)

        Schema.react<Three> { event ->
            Time.raise(Four(direction = true))
            Time.raise(Five(direction = true))
            event
        }

        Time.raise(Five(direction = true))

        val e = assertFailsWith<TimeException> {
            Time.raise(Three())
        }
        assertEquals(TimeException.Codes.ParticleExists, e.code)

        val actualMatter = actualMatter()
        val expectedMatter: MutableMatter = MutableGraph()
        expectedMatter.vertexValues += Five(true).toParticle()
        assertEquals(expectedMatter, actualMatter)
    }

    @Test
    fun layer2ReactorTest() = modelTest {
        Schema.register(Two, Three, Four, Five)
        Schema.react<Two> { event ->
            Time.raise(Three())
            event
        }
        Schema.react<Three> { event ->
            Time.raise(Four(true))
            Time.raise(Five(true))
            event
        }
        Time.raise(Five(true))
        val e = assertFailsWith<TimeException> {
            Time.raise(Two())
        }
        assertEquals(TimeException.Codes.ParticleExists, e.code)
        matter.vertexValues += Five(true).toParticle()
        val two = history.addVertex(Two())
        val three = history.addVertex(Three())
        val four = history.addVertex(Four(true))
        val notFour = history.addVertex(Four(false))
        history.addVertex(Five(true))
        four.connect(notFour, CausalRelationships.Model)
        notFour.connect(three, CausalRelationships.Reactive)
        four.connect(three, CausalRelationships.Reactive)
        three.connect(two, CausalRelationships.Reactive)
        checkModel()
    }

    @Test
    fun layer2ReactorTest2() = modelTest {
        Schema.register(Two, Three, Four, Five)
        Schema.react<Two> { event ->
            Time.raise(Three())
            event
        }
        Schema.react<Three> { event ->
            Time.raise(Four(true))
            Time.raise(Five(true))
            event
        }
        Schema.react<Five> { error("lol") }
        val e = assertFailsWith<IllegalStateException> {
            Time.raise(Two())
        }
        assertEquals("lol", e.message)
        val actual = actualMatter()
        val expected: MutableMatter = MutableGraph()
        assertEquals(expected, actual)
    }

    @Test
    fun errorEventTest() = modelTest {
        Schema.react<Four> { error("lol") }
        val event = async { Time.listen<Four> { U }.first() }
        val e = assertFailsWith<IllegalStateException> {
            Time.raise(Four(true))
        }
        assertFalse(infoOf(event.await()).succeeded)
        assertEquals("lol", e.message)
        history.addVertex(Four(true))
        checkModel()

        assertFailsWith<IllegalStateException> {
            Time.raise(Four(true))
        }
        history.addVertex(Four(true))
        checkModel()
    }
}

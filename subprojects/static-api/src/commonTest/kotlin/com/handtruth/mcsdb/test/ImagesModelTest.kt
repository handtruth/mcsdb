package com.handtruth.mcsdb.test

import com.handtruth.mc.graph.set
import com.handtruth.mc.types.UUID
import com.handtruth.mcsdb.context.ask
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.m
import com.handtruth.mcsdb.schema.CausalRelationships
import com.handtruth.mcsdb.space.Space
import com.handtruth.mcsdb.test.models.ImageContextEvent
import com.handtruth.mcsdb.test.models.ImageData
import com.handtruth.mcsdb.test.models.Stamp
import com.handtruth.mcsdb.test.models.TotallyOrdinaryDependantEvent
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.time.toParticle
import kotlinx.datetime.Clock
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.milliseconds

class ImagesModelTest {

    @Test
    fun imagesTest() = modelTest {
        val expectedData = ImageData(
            name = "a name",
            bitmap = byteArrayOf(3, -17, 26, 115),
            width = 3,
            height = 2,
            attributes = mapOf(
                "location" to ImageData.Coordinates(42.5f, 37.3f, -16.9f)
            )
        )
        val imageEvent = Time.raise(ImageContextEvent(data = expectedData, image = 0L, direction = true))
        val a = history.addVertex(imageEvent)
        matter.vertexValues += imageEvent.toParticle()
        checkModel()

        val dependantPrototype = TotallyOrdinaryDependantEvent(
            image = imageEvent.image,
            boolean = null,
            byte = 32,
            short = -234,
            int = 42,
            long = 4685244684471,
            float = .125f,
            double = null,
            char = 0.toChar(),
            uuid = UUID.nil,
            instant = Clock.System.now(),
            stamp = Stamp("lol kek"),
            direction = true
        )

        val dependantEvent = Time.raise(dependantPrototype)
        val b = history.addVertex(dependantEvent)
        a.connect(b, CausalRelationships.Model)
        matter[imageEvent.toParticle(), dependantEvent.toParticle()] = CausalRelationships.Model
        checkModel()

        val actualSet = setOf(dependantEvent.toParticle())

        val set1 = Space.ask {
            select { m(TotallyOrdinaryDependantEvent::uuid) eq dependantEvent.uuid }
        }
        assertEquals(actualSet, set1)

        val span = 1.milliseconds
        val set2 = Space.ask {
            select { m(TotallyOrdinaryDependantEvent::instant) le dependantEvent.instant }
        }
        assertTrue { set2.isEmpty() }

        val set3 = Space.ask {
            select { m(TotallyOrdinaryDependantEvent::instant) le (dependantEvent.instant + span) }
        }
        assertEquals(actualSet, set3)

        val set4 = Space.ask {
            select { m(TotallyOrdinaryDependantEvent::instant) bg dependantEvent.instant }
        }
        assertTrue { set4.isEmpty() }

        val set5 = Space.ask {
            select { m(TotallyOrdinaryDependantEvent::instant) bg (dependantEvent.instant - span) }
        }
        assertEquals(actualSet, set5)
    }
}

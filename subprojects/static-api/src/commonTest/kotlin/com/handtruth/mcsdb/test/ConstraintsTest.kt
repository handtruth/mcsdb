package com.handtruth.mcsdb.test

import com.handtruth.mc.types.Dynamic
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.schema.CausalRelationships
import com.handtruth.mcsdb.test.models.OneEvent
import com.handtruth.mcsdb.test.models.ThreeEvent
import com.handtruth.mcsdb.test.models.TwoEvent
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.time.TimeException
import com.handtruth.mcsdb.time.toParticle
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ConstraintsTest {

    @Test
    fun cycleTest() = mcsdbTest {
        Time.raise(OneEvent(direction = true))
        Time.raise(TwoEvent(direction = true))
        val error = assertFailsWith<TimeException> {
            Time.raise(ThreeEvent(direction = true))
        }
        assertEquals(TimeException.Codes.CyclicReference, error.code)
    }

    @Test
    fun explicitForeignKey() = modelTest {
        val actor1 = Time.raise(SystemActorEvent(direction = true))
        val a = history.addVertex(actor1)
        matter.vertexValues += actor1.toParticle()
        checkModel()

        val user = Time.raise(UserContextEvent(user = "xydgiz", actor = actor1.actor, direction = true))
        val b = history.addVertex(user)
        a.connect(b, CausalRelationships.Model)
        matter.vertexValues += user.toParticle()
        checkModel()

        val error = assertFailsWith<TimeException> {
            Time.raise(UserActorEvent(actor = actor1.actor, user = user.user, direction = true))
        }
        assertEquals(TimeException.Codes.ExplicitForeignKey, error.code)
        checkModel()
    }

    @Test
    fun errors() {
        assertFailsWith<NotImplementedError> {
            UserEvent.wrap(Dynamic())
        }
    }
}

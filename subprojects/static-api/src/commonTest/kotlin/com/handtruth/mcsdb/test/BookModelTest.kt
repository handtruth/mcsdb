package com.handtruth.mcsdb.test

import com.handtruth.mc.graph.set
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.schema.CausalRelationships
import com.handtruth.mcsdb.test.models.BookEvent
import com.handtruth.mcsdb.test.models.BookshelfContextEvent
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.time.cancel
import com.handtruth.mcsdb.time.toParticle
import kotlin.test.Test

class BookModelTest {

    @Test
    fun books1Test() = modelTest {
        val bookshelf1Event = Time.raise(BookshelfContextEvent(location = "тут", direction = true))
        val historyA = history.addVertex(bookshelf1Event)
        val bookshelf1 = bookshelf1Event.toParticle()
        matter.addVertex(bookshelf1)
        checkModel()

        val bookshelf2Event = Time.raise(BookshelfContextEvent(location = "там", direction = true))
        val historyB = history.addVertex(bookshelf2Event)
        val bookshelf2 = bookshelf2Event.toParticle()
        matter.addVertex(bookshelf2)
        checkModel()

        val book1Event = Time.raise(
            BookEvent(name = "Соль Земли", bookshelf = bookshelf1Event.bookshelf, direction = true)
        )
        val historyC = history.addVertex(book1Event)
        historyA.connect(historyC, CausalRelationships.Model)
        val book1 = book1Event.toParticle()
        matter.addVertex(book1)
        matter[bookshelf1, book1] = CausalRelationships.Model
        checkModel()

        val book2Event = Time.raise(
            BookEvent(name = "Отцы и дети", bookshelf = bookshelf1Event.bookshelf, direction = true)
        )
        val historyD = history.addVertex(book2Event)
        historyA.connect(historyD, CausalRelationships.Model)
        val book2 = book2Event.toParticle()
        matter.addVertex(book2)
        matter[bookshelf1, book2] = CausalRelationships.Model
        checkModel()

        val cancelBook1Event = Time.raise(cancel(book1Event))
        val historyE = history.addVertex(cancelBook1Event)
        historyC.connect(historyE, CausalRelationships.Model)
        matter.vertexValues -= book1
        checkModel()

        Time.raise(book1Event.copy())
        val historyF = history.addVertex(book1Event)
        historyE.connect(historyF, CausalRelationships.Model)
        matter.addVertex(book1)
        matter[bookshelf1, book1] = CausalRelationships.Model
        checkModel()

        val cancelBookshelf1Event = Time.raise(cancel(bookshelf1Event))
        val historyG = history.addVertex(cancelBook1Event)
        historyF.connect(historyG, CausalRelationships.Model)
        val historyH = history.addVertex(cancel(book2Event))
        historyD.connect(historyH, CausalRelationships.Model)
        val historyI = history.addVertex(cancelBookshelf1Event)
        historyG.connect(historyI, CausalRelationships.Model)
        historyH.connect(historyI, CausalRelationships.Model)
        matter.vertexValues -= setOf(bookshelf1, book1, book2)
        checkModel()

        Time.raise(bookshelf1Event.copy())
        val historyJ = history.addVertex(bookshelf1Event)
        historyI.connect(historyJ, CausalRelationships.Model)
        matter.addVertex(bookshelf1)
        checkModel()

        Time.raise(book1Event.copy())
        val historyK = history.addVertex(book1Event)
        historyJ.connect(historyK, CausalRelationships.Model)
        matter[bookshelf1, book1] = CausalRelationships.Model
        checkModel()

        val bookLolEvent = Time.raise(
            BookEvent(name = "LOL", bookshelf = bookshelf1Event.bookshelf, direction = true)
        )
        val historyL = history.addVertex(bookLolEvent)
        historyJ.connect(historyL, CausalRelationships.Model)
        val bookLol = bookLolEvent.toParticle()
        matter[bookshelf1, bookLol] = CausalRelationships.Model
        checkModel()

        val book3Event = Time.raise(
            BookEvent(name = "MCSDB ВКР", bookshelf = bookshelf2Event.bookshelf, direction = true)
        )
        val historyM = history.addVertex(book3Event)
        historyB.connect(historyM, CausalRelationships.Model)
        val book3 = book3Event.toParticle()
        matter[bookshelf2, book3] = CausalRelationships.Model
        checkModel()

        Time.raise(cancelBookshelf1Event.copy())
        val historyN = history.addVertex(cancelBook1Event)
        historyK.connect(historyN, CausalRelationships.Model)
        val historyO = history.addVertex(cancel(bookLolEvent))
        historyL.connect(historyO, CausalRelationships.Model)
        val historyP = history.addVertex(cancelBookshelf1Event)
        historyN.connect(historyP, CausalRelationships.Model)
        historyO.connect(historyP, CausalRelationships.Model)
        matter.vertexValues -= setOf(bookshelf1, book1, bookLol)
        checkModel()

        val cancelBookshelf2Event = Time.raise(cancel(bookshelf2Event))
        val historyQ = history.addVertex(cancel(book3Event))
        historyM.connect(historyQ, CausalRelationships.Model)
        val historyR = history.addVertex(cancelBookshelf2Event)
        historyQ.connect(historyR, CausalRelationships.Model)
        matter.clear()
        checkModel()
    }
}

package com.handtruth.mcsdb.test

import com.handtruth.mc.graph.Graph
import com.handtruth.mc.graph.MutableGraph
import com.handtruth.mc.graph.toMutableGraph
import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.asStatic
import com.handtruth.mcsdb.context.Context
import com.handtruth.mcsdb.context.subgraph
import com.handtruth.mcsdb.open
import com.handtruth.mcsdb.space.MutableMatter
import com.handtruth.mcsdb.space.Space
import com.handtruth.mcsdb.time.MutableHistory
import com.handtruth.mcsdb.time.Time
import io.ktor.test.dispatcher.* // ktlint-disable no-wildcard-imports
import io.ktor.utils.io.core.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import kotlin.test.assertEquals

fun mcsdbTest(block: suspend CoroutineScope.() -> Unit) = testSuspend {
    val url = "sqlite:file:test?mode=memory"
    // val file = "sqlite:file.db"
    Database.open(url).asStatic().use { database ->
        withContext(Context(database)) {
            block()
        }
    }
}

suspend fun actualHistory() = Time.subgraph { U }

suspend fun actualMatter() = Space.subgraph { U }

fun modelTest(block: suspend ModelTestScope.() -> Unit) = mcsdbTest {
    val modelTestScope = ModelTestScope(this)
    modelTestScope.checkModel()
    try {
        modelTestScope.block()
    } catch (e: Throwable) {
        modelTestScope.history.toPNG("expected_history.png")
        modelTestScope.matter.toPNG("expected_matter.png")
        actualHistory().toPNG("actual_history.png")
        actualMatter().toPNG("actual_matter.png")
        throw e
    }
}

class ModelTestScope(scope: CoroutineScope) : CoroutineScope by scope {
    var history: MutableHistory = MutableGraph()
        private set
    var matter: MutableMatter = MutableGraph()
        private set

    private val historyStack = ArrayList<MutableHistory>()
    private val matterStack = ArrayList<MutableMatter>()

    fun push() {
        historyStack += history
        matterStack += matter
        history = history.toMutableGraph()
        matter = matter.toMutableGraph()
    }

    fun pop() {
        history = historyStack.removeLast()
        matter = matterStack.removeLast()
    }

    suspend fun checkModel() {
        assertEquals(history, actualHistory(), "incorrect history generated")
        assertEquals(matter, actualMatter(), "incorrect matter generated")
    }
}

expect fun <V, E> Graph<V, E>.toPNG(file: String, topologyOnly: Boolean = false)

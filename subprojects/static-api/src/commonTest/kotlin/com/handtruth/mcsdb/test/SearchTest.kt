package com.handtruth.mcsdb.test

import com.handtruth.mcsdb.context.ask
import com.handtruth.mcsdb.context.listen
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.m
import com.handtruth.mcsdb.time.DirectEvent
import com.handtruth.mcsdb.time.EventCompanion
import com.handtruth.mcsdb.time.Time
import kotlinx.coroutines.flow.toSet
import kotlin.test.Test
import kotlin.test.assertEquals

data class StringEvent(val string: String, override val direction: Boolean) : DirectEvent() {
    companion object : EventCompanion()
}

data class ULongEvent(val uLong: ULong, override val direction: Boolean) : DirectEvent() {
    companion object : EventCompanion()
}

data class IntEvent(val int: Int, override val direction: Boolean) : DirectEvent() {
    companion object : EventCompanion()
}

class SearchTest {

    @Test
    fun searchStringTest() = mcsdbTest {
        Time.raise(StringEvent("hello world", true))
        val b = Time.raise(StringEvent("123456789", true))
        val c = Time.raise(StringEvent("one two three", true))
        val d = Time.raise(StringEvent("0 1 tvo [6]", true))
        Time.raise(StringEvent("once upon a time", true))
        Time.raise(StringEvent("welcome to meta space", true))

        val actual1 = Time.listen(actual = true, stream = false) {
            m(StringEvent::string) like "* t?o *"
        }.toSet()
        assertEquals(setOf(c, d), actual1)

        val actual2 = Time.listen(actual = true, stream = false) {
            m(StringEvent::string) match """\d+"""
        }.toSet()
        assertEquals(setOf(b), actual2)
    }

    @Test
    fun searchULongTest() = mcsdbTest {
        val ul5 = Time.raise(ULongEvent(5u, direction = true))
        val ul4 = Time.raise(ULongEvent(4u, direction = true))
        val ulBig = Time.raise(ULongEvent((-23).toULong(), direction = true))

        val actual3 = Time.ask { from { m(ULongEvent::uLong) le 5uL } }
        assertEquals(listOf(ul4), actual3)

        val actual4 = Time.ask { from { m(ULongEvent::uLong) bg 4uL } }.toSet()
        assertEquals(setOf(ul5, ulBig), actual4)
    }

    @Test
    fun searchIntTest() = mcsdbTest {
        val i12 = Time.raise(IntEvent(12, direction = true))
        val im12 = Time.raise(IntEvent(-12, direction = true))

        val actual1 = Time.ask { from { m(IntEvent::int) le 0 } }
        assertEquals(listOf(im12), actual1)

        val actual2 = Time.ask { from { m(IntEvent::int) bg 0 } }
        assertEquals(listOf(i12), actual2)
    }
}

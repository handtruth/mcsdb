package com.handtruth.mcsdb.test

import com.handtruth.mc.graph.get
import com.handtruth.mc.graph.set
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.schema.CausalRelationships
import com.handtruth.mcsdb.space.MutableMatter
import com.handtruth.mcsdb.test.models.ConnectionEvent
import com.handtruth.mcsdb.test.models.SessionContextEvent
import com.handtruth.mcsdb.time.MutableHistory
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.time.cancel
import com.handtruth.mcsdb.time.toParticle
import kotlin.test.Test

class ResourceModelTest {

    /*
        1->c->2
     */
    private inline fun connections(
        crossinline block: suspend ModelTestScope.(MutableHistory, MutableMatter) -> Unit
    ) = modelTest {
        val connection1Event = Time.raise(
            ConnectionEvent(direction = true)
        )
        val a = history.addVertex(connection1Event)
        val connection1 = connection1Event.toParticle()
        matter.vertexValues += connection1
        checkModel()

        val session1Event = Time.raise(
            SessionContextEvent(session = connection1Event.session, direction = true)
        )
        val b = history.addVertex(session1Event)
        a.connect(b, CausalRelationships.Model)
        val session1 = session1Event.toParticle()
        matter[connection1, session1] = CausalRelationships.Model
        checkModel()

        val connection2Event = Time.raise(
            ConnectionEvent(session = session1Event.session, direction = true)
        )
        val c = history.addVertex(connection2Event)
        b.connect(c, CausalRelationships.Model)
        val connection2 = connection2Event.toParticle()
        matter[connection2, session1] = CausalRelationships.Model
        checkModel()

        block(history, matter)
    }

    @Test
    fun connections1Test() = connections { history, matter ->
        val connections = history.vertexValues.asSequence().mapNotNull { it as? ConnectionEvent }
        val connection1Event = connections.minByOrNull { it.connection }!!
        val connection2Event = connections.maxByOrNull { it.connection }!!
        val connection1 = connection1Event.toParticle()
        val session1Event = history.vertexValues.find { it is SessionContextEvent } as SessionContextEvent
        val c = history[connection2Event]!!

        val cancelConnection1Event = Time.raise(cancel(connection1Event))
        val d = history.addVertex(cancelConnection1Event)
        c.connect(d, CausalRelationships.Model)
        matter.vertexValues -= connection1
        checkModel()

        val cancelConnection2Event = Time.raise(cancel(connection2Event))
        val e = history.addVertex(cancel(session1Event))
        d.connect(e, CausalRelationships.Model)
        val f = history.addVertex(cancelConnection2Event)
        e.connect(f, CausalRelationships.Model)
        matter.clear()
        checkModel()
    }

    @Test
    fun connections2Test() = connections { history, matter ->
        val connections = history.vertexValues.asSequence().mapNotNull { it as? ConnectionEvent }
        val connection1Event = connections.minByOrNull { it.connection }!!
        val connection1 = connection1Event.toParticle()
        val connection2Event = connections.maxByOrNull { it.connection }!!
        val session1Event = history.vertexValues.find { it is SessionContextEvent } as SessionContextEvent
        val session1 = session1Event.toParticle()
        val b = history[connection2Event]!!

        val connection3Event = Time.raise(ConnectionEvent(session = session1Event.session, direction = true))
        val c = history[session1Event]!!
        val d = history.addVertex(connection3Event)
        c.connect(d, CausalRelationships.Model)
        val connection3 = connection3Event.toParticle()
        matter[connection3, session1] = CausalRelationships.Model
        checkModel()

        val cancelConnection1Event = Time.raise(cancel(connection1Event))
        val e = history.addVertex(cancelConnection1Event)
        c.connect(e, CausalRelationships.Model)
        matter.vertexValues -= connection1
        checkModel()

        val cancelSession1Event = Time.raise(cancel(session1Event))
        val f = history.addVertex(cancelSession1Event)
        e.connect(f, CausalRelationships.Model)
        b.connect(f, CausalRelationships.Model)
        d.connect(f, CausalRelationships.Model)
        matter.vertexValues -= session1
        checkModel()
    }
}

package com.handtruth.mcsdb.test

import com.handtruth.mc.graph.contains
import com.handtruth.mc.graph.get
import com.handtruth.mc.graph.set
import com.handtruth.mcsdb.*
import com.handtruth.mcsdb.context.*
import com.handtruth.mcsdb.schema.CausalRelationships
import com.handtruth.mcsdb.schema.Schema
import com.handtruth.mcsdb.time.*
import com.handtruth.mcsdb.variability.Variability
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.first
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ActorTest {

    @Test
    fun fantomTest() = modelTest {
        Schema.register(
            SystemActorEvent,
            UserContextEvent,
            GroupContextEvent,
            GroupMemberEvent,
            GroupDescriptionEvent,
            GroupActorEvent,
            UserActorEvent,
            MessageEvent
        )
        Schema.reactAfter<UserCreationEvent> { event ->
            val userContext = UserContextEvent(
                user = event.name,
                actor = event.actor,
                direction = true
            )
            Time.raise(userContext)
            val userActor = UserActorEvent(
                user = event.name,
                direction = true
            )
            Time.raise(userActor)
            event
        }
        Schema.reactAfter<GroupCreationEvent> { event ->
            val groupContext = GroupContextEvent(
                group = event.name,
                actor = event.actor,
                direction = true
            )
            Time.raise(groupContext)
            val groupActor = GroupActorEvent(
                group = event.name,
                direction = true
            )
            Time.raise(groupActor)
            val groupDescription = GroupDescriptionEvent(
                group = event.name,
                description = event.description,
                direction = true
            )
            Time.raise(groupDescription)
            event
        }
        val systemActorEvent = Time.raise(SystemActorEvent(direction = true))
        val systemActorNode = history.addVertex(systemActorEvent)
        matter.addVertex(systemActorEvent.toParticle())
        checkModel()

        val ktloAsync = async {
            val ktloFilter = filter { m(UserEvent::user) eq "ktlo" }
            val ktloUserEvent = async {
                Time.listen<UserContextEvent>(filter = ktloFilter).first()
            }
            val ktloActorEvent = async {
                Time.listen<ActorEvent> { ktloFilter and attached(UserActorEvent) }.first()
            }
            ktloUserEvent.await() to ktloActorEvent.await()
        }
        val ktloCreationEvent = Time.raise(
            UserCreationEvent(
                name = "ktlo",
                actor = systemActorEvent.actor
            )
        )
        val (ktloUserEvent, ktloActorEvent) = ktloAsync.await()
        val ktloUserNode = history.addVertex(ktloUserEvent)
        systemActorNode.connect(ktloUserNode, CausalRelationships.Model)
        val ktloActorNode = history.addVertex(ktloActorEvent)
        ktloUserNode.connect(ktloActorNode, CausalRelationships.Model)
        val ktloCreationNode = history.addVertex(ktloCreationEvent)
        systemActorNode.connect(ktloCreationNode, CausalRelationships.Model)
        ktloUserNode.connect(ktloCreationNode, CausalRelationships.Reactive)
        ktloActorNode.connect(ktloCreationNode, CausalRelationships.Reactive)
        matter[ktloUserEvent.toParticle(), ktloActorEvent.toParticle()] = CausalRelationships.Model
        checkModel()

        val peopleAsync = async {
            val peopleFilter = filter { m(GroupEvent::group) eq "people" }
            val peopleGroupEvent = async {
                Time.listen<GroupContextEvent>(filter = peopleFilter).first()
            }
            val peopleActorEvent = async {
                Time.listen<GroupActorEvent>(filter = peopleFilter).first()
            }
            val peopleDescriptionEvent = async {
                Time.listen<GroupDescriptionEvent>(filter = peopleFilter).first()
            }
            Triple(peopleGroupEvent.await(), peopleActorEvent.await(), peopleDescriptionEvent.await())
        }
        val peopleCreationEvent = Time.raise(
            GroupCreationEvent(
                name = "people",
                description = "Everybody People!",
                actor = ktloActorEvent.actor
            )
        )
        val (peopleGroupEvent, peopleActorEvent, peopleDescriptionEvent) = peopleAsync.await()
        val peopleGroupNode = history.addVertex(peopleGroupEvent)
        ktloActorNode.connect(peopleGroupNode, CausalRelationships.Model)
        val peopleActorNode = history.addVertex(peopleActorEvent)
        peopleGroupNode.connect(peopleActorNode, CausalRelationships.Model)
        val peopleDescriptionNode = history.addVertex(peopleDescriptionEvent)
        peopleGroupNode.connect(peopleDescriptionNode, CausalRelationships.Model)
        val peopleCreationNode = history.addVertex(peopleCreationEvent)
        peopleGroupNode.connect(peopleCreationNode, CausalRelationships.Reactive)
        peopleActorNode.connect(peopleCreationNode, CausalRelationships.Reactive)
        peopleDescriptionNode.connect(peopleCreationNode, CausalRelationships.Reactive)
        ktloActorNode.connect(peopleCreationNode, CausalRelationships.Model)
        matter[peopleGroupEvent.toParticle(), peopleActorEvent.toParticle()] = CausalRelationships.Model
        matter[peopleGroupEvent.toParticle(), peopleDescriptionEvent.toParticle()] = CausalRelationships.Model
        checkModel()

        val tarsimunAsync = async {
            val tarsimunFilter = filter { m(UserEvent::user) eq "tarsimun" }
            val tarsimunUserEvent = async {
                Time.listen<UserContextEvent>(filter = tarsimunFilter).first()
            }
            val tarsimunActorEvent = async {
                Time.listen<UserActorEvent>(filter = tarsimunFilter).first()
            }
            tarsimunUserEvent.await() to tarsimunActorEvent.await()
        }
        val tarsimunCreationEvent = Time.raise(
            UserCreationEvent(
                name = "tarsimun",
                actor = ktloActorEvent.actor
            )
        )
        val (tarsimunUserEvent, tarsimunActorEvent) = tarsimunAsync.await()
        val tarsimunUserNode = history.addVertex(tarsimunUserEvent)
        ktloActorNode.connect(tarsimunUserNode, CausalRelationships.Model)
        val tarsimunActorNode = history.addVertex(tarsimunActorEvent)
        tarsimunUserNode.connect(tarsimunActorNode, CausalRelationships.Model)
        val tarsimunCreationNode = history.addVertex(tarsimunCreationEvent)
        tarsimunUserNode.connect(tarsimunCreationNode, CausalRelationships.Reactive)
        tarsimunActorNode.connect(tarsimunCreationNode, CausalRelationships.Reactive)
        ktloActorNode.connect(tarsimunCreationNode, CausalRelationships.Model)
        matter[tarsimunUserEvent.toParticle(), tarsimunActorEvent.toParticle()] = CausalRelationships.Model
        checkModel()

        val tarsimunInPeopleEvent = Time.raise(
            GroupMemberEvent(
                group = "people",
                user = "tarsimun",
                actor = ktloActorEvent.actor,
                direction = true
            )
        )
        val tarsimunInPeopleNode = history.addVertex(tarsimunInPeopleEvent)
        peopleGroupNode.connect(tarsimunInPeopleNode, CausalRelationships.Model)
        tarsimunUserNode.connect(tarsimunInPeopleNode, CausalRelationships.Model)
        matter[peopleGroupEvent.toParticle(), tarsimunInPeopleEvent.toParticle()] = CausalRelationships.Model
        matter[tarsimunUserEvent.toParticle(), tarsimunInPeopleEvent.toParticle()] = CausalRelationships.Model
        checkModel()

        val messageEvent = Time.raise(
            MessageEvent(
                user = "tarsimun",
                actor = ktloActorEvent.actor,
                message = "Hello there! :)"
            )
        )
        val messageNode = history.addVertex(messageEvent)
        tarsimunUserNode.connect(messageNode, CausalRelationships.Model)
        checkModel()

        push()
        val delTarsState = Variability.fork("delete tarsimun") {
            checkModel()

            val tarsimunInPeopleNode2 = history[tarsimunInPeopleEvent]!!
            val tarsimunActorNode2 = history[tarsimunActorEvent]!!
            val deleteTarsimunAsync = async {
                val tarsimunFilter = filter { m(UserEvent::user) eq "tarsimun" }
                val removeTarsimunFromPeopleEvent = async {
                    Time.listen<GroupMemberEvent> { tarsimunFilter }.first { !it.direction }
                }
                val deleteTarsimunActorEvent = async {
                    Time.listen<UserActorEvent> { tarsimunFilter }.first { !it.direction }
                }
                removeTarsimunFromPeopleEvent.await() to deleteTarsimunActorEvent.await()
            }
            val deleteTarsimunEvent = Time.raise(cancel(tarsimunUserEvent))
            val (removeTarsimunFromPeopleEvent, deleteTarsimunActorEvent) = deleteTarsimunAsync.await()
            val removeTarsimunFromPeopleNode = history.addVertex(removeTarsimunFromPeopleEvent)
            tarsimunInPeopleNode2.connect(removeTarsimunFromPeopleNode, CausalRelationships.Model)
            val deleteTarsimunActorNode = history.addVertex(deleteTarsimunActorEvent)
            tarsimunActorNode2.connect(deleteTarsimunActorNode, CausalRelationships.Model)
            val deleteTarsimunNode = history.addVertex(deleteTarsimunEvent)
            deleteTarsimunActorNode.connect(deleteTarsimunNode, CausalRelationships.Model)
            removeTarsimunFromPeopleNode.connect(deleteTarsimunNode, CausalRelationships.Model)
            matter.vertexValues -= listOf(
                tarsimunInPeopleEvent.toParticle(),
                tarsimunActorEvent.toParticle(),
                tarsimunUserEvent.toParticle()
            )
            checkModel()
            getContext().state
        }
        pop()
        checkModel()

        push()
        val delKtloState = Variability.fork("delete ktlo") {
            checkModel()

            val deleteKtloActorEvent = cancel(
                Time.listen<UserEvent>(actual = true, stream = false) {
                    (m(UserEvent::user) eq "ktlo") and primary(UserActorEvent)
                }.first() as UserActorEvent
            )
            val tarsimunActorNode2 = history[tarsimunActorEvent]!!
            val deleteKtloEvent = Time.raise(ktloUserEvent.copy(actor = tarsimunActorEvent.actor, direction = false))
            assertEquals(cancel(ktloActorEvent), deleteKtloActorEvent)
            val deleteKtloActorNode = history.addVertex(deleteKtloActorEvent)
            history[ktloActorEvent, deleteKtloActorEvent] = CausalRelationships.Model
            val deleteKtloNode = history.addVertex(deleteKtloEvent)
            deleteKtloActorNode.connect(deleteKtloNode, CausalRelationships.Model)
            tarsimunActorNode2.connect(deleteKtloNode, CausalRelationships.Model)
            matter.vertexValues -= listOf(deleteKtloEvent.toParticle(), deleteKtloActorEvent.toParticle())

            val actualHistory = actualHistory()
            assertTrue { deleteKtloEvent in actualHistory }
            assertEquals(history.vertices.size, actualHistory.vertices.size)

            checkModel()

            val events = Time.ask { last(from { m(UserContextEvent::user) eq "tarsimun" }) }
            assertEquals(listOf(tarsimunUserEvent), events)

            getContext().state
        }
        pop()
        checkModel()

        val tars = filter { m(UserContextEvent::user) eq "tarsimun" }
        run {
            val states = Variability.ask {
                not(reactive) and happened {
                    from(tars) and (direction eq false)
                } and not(contains(tars))
            }
            assertEquals(setOf(delTarsState), states)
        }

        run {
            val database = getContext().database

            val events2 = database.time.ask(delTarsState) {
                from { tars }
            }
            assertEquals(listOf(tarsimunUserEvent, cancel(tarsimunUserEvent)), events2)

            val events1query = timeQuery {
                from {
                    fantom(ActorEvent) and via<ActorEvent> {
                        type(UserActorEvent) and via<UserEvent> { tars }
                    }
                }
            }
            assertEquals(emptyList(), database.time.ask(delTarsState, events1query))
            assertEquals(
                listOf(cancel(ktloUserEvent.copy(actor = tarsimunActorEvent.actor))),
                database.time.ask(delKtloState, events1query)
            )

            val states = Variability.ask {
                not(reactive) and happened(events1query)
            }
            assertEquals(setOf(delKtloState), states)
        }
    }

    @Test
    fun actorExchangeTest() = modelTest {
        val systemActorEvent = Time.raise(SystemActorEvent(direction = true))
        val systemActorNode = history.addVertex(systemActorEvent)
        matter.vertexValues += systemActorEvent.toParticle()
        checkModel()

        val alefEvent = Time.raise(UserContextEvent(user = "alef", actor = systemActorEvent.actor, direction = true))
        val alefNode = history.addVertex(alefEvent)
        systemActorNode.connect(alefNode, CausalRelationships.Model)
        matter.vertexValues += alefEvent.toParticle()
        checkModel()

        val alefActorEvent = Time.raise(UserActorEvent(user = alefEvent.user, direction = true))
        val alefActorNode = history.addVertex(alefActorEvent)
        alefNode.connect(alefActorNode, CausalRelationships.Model)
        matter[alefEvent.toParticle(), alefActorEvent.toParticle()] = CausalRelationships.Model
        checkModel()

        val groupContextEvent = Time.raise(
            GroupContextEvent(group = "group", actor = systemActorEvent.actor, direction = true)
        )
        val groupContextNode = history.addVertex(groupContextEvent)
        systemActorNode.connect(groupContextNode, CausalRelationships.Model)
        matter.vertexValues += groupContextEvent.toParticle()
        checkModel()

        val cancelGroupContextEvent = Time.raise(
            groupContextEvent.copy(actor = alefActorEvent.actor, direction = false)
        )
        val cancelGroupContextNode = history.addVertex(cancelGroupContextEvent)
        groupContextNode.connect(cancelGroupContextNode, CausalRelationships.Model)
        alefActorNode.connect(cancelGroupContextNode, CausalRelationships.Model)
        assertTrue {
            matter.vertexValues.remove(cancelGroupContextEvent.toParticle())
        }
        checkModel()
    }
}

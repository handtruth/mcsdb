package com.handtruth.mcsdb.test

import com.handtruth.mc.graph.MutableGraph
import com.handtruth.mc.types.Dynamic
import com.handtruth.mcsdb.attached
import com.handtruth.mcsdb.context.ask
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.context.subgraph
import com.handtruth.mcsdb.primary
import com.handtruth.mcsdb.schema.CausalRelationships
import com.handtruth.mcsdb.space.Particle
import com.handtruth.mcsdb.test.models.ChainEvent
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.time.TimeException
import com.handtruth.mcsdb.time.cancel
import com.handtruth.mcsdb.type
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals
import kotlin.time.measureTime
import kotlin.time.measureTimedValue

class ChainModelTest {

    @Test
    fun chainTest() = modelTest {
        val particle = Particle(ChainEvent.description, Dynamic())
        val chain1 = Time.raise(ChainEvent(direction = true))
        val a = history.addVertex(chain1)
        matter.vertexValues += particle
        checkModel()

        val timeError1 = assertFailsWith<TimeException> {
            Time.raise(chain1.copy())
        }
        assertEquals(TimeException.Codes.ParticleExists, timeError1.code, timeError1.explanation)
        checkModel()

        val chain2 = Time.raise(cancel(chain1))
        matter.clear()
        val b = history.addVertex(chain2)
        a.connect(b, CausalRelationships.Model)
        checkModel()

        val timeError2 = assertFailsWith<TimeException> {
            Time.raise(chain2.copy())
        }
        assertEquals(TimeException.Codes.ParticleNotExists, timeError2.code)
        checkModel()

        assertEquals(listOf(chain2), Time.ask { last(U) })
        assertEquals(listOf(chain1), Time.ask { first(U) })
        assertEquals(
            listOf(chain1, chain2),
            Time.ask {
                from {
                    attached(ChainEvent) and primary(ChainEvent) and type(ChainEvent)
                }
            }
        )
        checkModel()

        val chain3 = Time.raise(cancel(chain2))
        val c = history.addVertex(chain3)
        b.connect(c, CausalRelationships.Model)
        matter.vertexValues += particle
        checkModel()

        run {
            val history = Time.subgraph { first(U) or last(U) }
            val expected = MutableGraph<Event, CausalRelationships>()
            val first = expected.addVertex(chain1)
            val last = expected.addVertex(chain1)
            first.connect(last, CausalRelationships.Transitive)
            assertEquals(expected, history)
            val history2 = Time.subgraph { direction eq true }
            assertEquals(expected, history2)
            val history3 = actualHistory()
            assertNotEquals(expected, history3)
        }

        run {
            val events = Time.ask { after(first(U)) and before(last(U)) }
            assertEquals(listOf(chain2), events)
        }

        Time.raise(cancel(chain3))
        val d = history.addVertex(chain2)
        c.connect(d, CausalRelationships.Model)
        matter.clear()
        checkModel()
    }

    @Test
    fun bigChainTest() = mcsdbTest {
        val count = 100
        val start = ChainEvent(direction = true)
        val reacted = measureTime {
            (0 until count * 2).fold(start) { event, iteration ->
                val (next, duration) = measureTimedValue {
                    cancel(Time.raise(event))
                }
                println("#$iteration: $duration")
                next
            }
        }
        println("start checking $reacted")
        val end = cancel(start)
        val expected = MutableGraph<Event, CausalRelationships>()
        val initial: MutableGraph.MutableVertex<Event, CausalRelationships>
        run {
            val a = expected.addVertex(start)
            initial = expected.addVertex(end)
            a.connect(initial, CausalRelationships.Model)
        }
        (0 until count - 1).fold(initial) { vertex, _ ->
            val a = expected.addVertex(start)
            vertex.connect(a, CausalRelationships.Model)
            val b = expected.addVertex(end)
            a.connect(b, CausalRelationships.Model)
            b
        }
        assertEquals(expected, actualHistory())
    }
}

package com.handtruth.mcsdb.util

import com.handtruth.mc.nbt.* // ktlint-disable no-wildcard-imports
import com.handtruth.mcsdb.InternalMcsdbApi
import com.handtruth.mcsdb.schema.checkSchema
import io.ktor.utils.io.core.* // ktlint-disable no-wildcard-imports
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy
import kotlin.reflect.KClass

private val nbt: NBTBinaryFormat = NBTSerialFormat(tagsModule = TagsModule.MCSDB) + NBTBinaryCodec(binaryConfig = NBTBinaryConfig.KBT)

@InternalMcsdbApi
public fun <T> encodeToStructure(serializer: SerializationStrategy<T>, value: T): Any {
    return nbt.encodeToNBT(serializer, value)
}

@InternalMcsdbApi
public fun <T> decodeFromStructure(deserializer: DeserializationStrategy<T>, value: Any): T {
    return nbt.decodeFromNBT(deserializer, value)
}

@InternalMcsdbApi
public fun <T> encodeToArbitrary(serializer: SerializationStrategy<T>, value: T): ByteArray {
    val root = encodeToStructure(serializer, value)
    val tagsModule = nbt.tagsModule
    val tag = tagsModule.tagOf(root)
    val tagId = tagsModule.tagIdOf(tag)
    return buildPacket {
        writeByte(tagId)
        tag.writeBinary(this, nbt, root)
    }.use {
        it.readBytes()
    }
}

@InternalMcsdbApi
public fun <T> decodeFromArbitrary(deserializer: DeserializationStrategy<T>, value: ByteArray): T {
    return ByteReadPacket(value).use { input ->
        val tagId = input.readByte()
        val tag = nbt.tagsModule.tagById(tagId)
        val root = tag.readBinary(input, nbt)
        decodeFromStructure(deserializer, root)
    }
}

@InternalMcsdbApi
public val KClass<*>.outer: KClass<*>
    get() {
        val javaClass = java
        val name = javaClass.name
        val index = name.lastIndexOf('$')
        checkSchema(index != -1) { "$this has no outer class" }
        return javaClass.classLoader.loadClass(name.substring(0, index)).kotlin
    }

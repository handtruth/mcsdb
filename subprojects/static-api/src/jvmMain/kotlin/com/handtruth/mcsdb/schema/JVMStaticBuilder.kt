package com.handtruth.mcsdb.schema

import com.handtruth.mc.types.UUID
import com.handtruth.mcsdb.InternalMcsdbApi
import com.handtruth.mcsdb.types.Serial
import kotlinx.datetime.Instant
import kotlin.reflect.KType
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.jvm.jvmErasure

@InternalMcsdbApi
public actual fun kTypeToMcsdbType(type: KType): Types = when {
    type.hasAnnotation<Structure>() -> Types.Structure
    type.hasAnnotation<Arbitrary>() -> Types.Arbitrary
    else -> when (type.jvmErasure) {
        Boolean::class -> Types.Boolean
        Byte::class -> Types.Byte
        Short::class -> Types.Short
        Int::class -> Types.Int
        Long::class -> Types.Long
        UByte::class -> Types.UByte
        UShort::class -> Types.UShort
        UInt::class -> Types.UInt
        ULong::class -> Types.ULong
        Float::class -> Types.Float
        Double::class -> Types.Double
        Char::class -> Types.Char
        String::class -> Types.String
        Instant::class -> Types.Instant
        UUID::class -> Types.UUID
        Serial::class -> Types.Serial
        ByteArray::class -> Types.Arbitrary
        else -> Types.Structure
    }
}

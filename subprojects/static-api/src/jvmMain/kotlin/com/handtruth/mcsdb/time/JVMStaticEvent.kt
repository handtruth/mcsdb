package com.handtruth.mcsdb.time

import com.handtruth.kommon.setBean
import com.handtruth.mc.types.Dynamic
import com.handtruth.mc.types.UUID
import com.handtruth.mc.types.buildDynamic
import com.handtruth.mcsdb.schema.* // ktlint-disable no-wildcard-imports
import com.handtruth.mcsdb.schema.Target
import com.handtruth.mcsdb.types.Serial
import com.handtruth.mcsdb.util.* // ktlint-disable no-wildcard-imports
import kotlinx.serialization.KSerializer
import kotlinx.serialization.serializer
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty1
import kotlin.reflect.full.* // ktlint-disable no-wildcard-imports
import kotlin.reflect.jvm.jvmErasure

internal actual fun getEventClassForCompanion(`class`: KClass<out EventCompanion>): KClass<out Event> {
    val eventClass = `class`.outer
    checkSchema(eventClass.isSubclassOf(Event::class)) { "$eventClass is not an event class" }
    @Suppress("UNCHECKED_CAST")
    return eventClass as KClass<out Event>
}

internal actual fun buildDescriptionForCompanion(companion: EventCompanion): EventDescription {
    val eventClass = companion.eventClass
    val result = when {
        eventClass.java.isInterface -> buildInterfaceEvent(eventClass)
        eventClass.isSubclassOf(DirectEvent::class) -> buildDirectEvent(eventClass)
        eventClass.isSubclassOf(FactEvent::class) -> buildFactEvent(eventClass)
        else -> invalidSchema(
            "unknown event kind. You should derive ${DirectEvent::class}" +
                " or ${FactEvent::class} or leave it as interface"
        )
    }
    result.setBean(companion)
    return result
}

private fun buildInterfaceEvent(eventClass: KClass<out Event>): EventDescription {
    checkSchema(eventClass.typeParameters.isEmpty()) { "event interface should not have type parameters" }
    var eventName = eventClass.qualifiedName?.removeSuffix("Event")
    val eventModifiers: List<Modifier.EventBound> = eventClass.annotations.mapNotNull { annotation ->
        when (annotation) {
            is WithName -> {
                eventName = annotation.name
                null
            }
            is Target -> Modifier.Target
            is Fantom -> Modifier.Fantom
            is Unique -> Modifier.Unique
            is Shared -> Modifier.Shared
            else -> null
        }
    }
    val descriptionName = checkSchemaNotNull(eventName) { "event interface should have name" }
    return interfaceEvent(descriptionName) {
        modifiers += eventModifiers

        eventClass.supertypes.forEach { supertype ->
            val companion = supertype.jvmErasure.companionObjectInstance
            if (companion is EventCompanion) {
                val inheritanceModifier: List<Modifier.InheritanceBound> =
                    supertype.annotations.mapNotNull { annotation ->
                        when (annotation) {
                            is Target -> Modifier.Target
                            is Fantom -> Modifier.Fantom
                            is Source -> invalidSchema("interface event is not a matter source by itself")
                            else -> null
                        }
                    }
                inherit(companion.description, inheritanceModifier)
            }
        }

        val properties = mutableMapOf<String, KProperty1<out Event, *>>()
        eventClass.declaredMemberProperties
            .filterNot { it.hasAnnotation<Ignore>() }
            .associateByTo(properties) { it.name }
        collectFields(inherits).forEach { properties.remove(it.name) }
        createFields(properties.values)
    }
}

private fun buildDirectEvent(eventClass: KClass<out Event>): EventDescription {
    val kind = Kind.Direct
    val (descriptionName, eventModifiers) = prepareFinalEvent(kind, eventClass)
    return describeEvent(descriptionName, kind) {
        modifiers += eventModifiers

        eventClass.supertypes.forEach { supertype ->
            val companion = supertype.jvmErasure.companionObjectInstance
            if (companion is EventCompanion) {
                val inheritanceModifier: List<Modifier.InheritanceBound> =
                    supertype.annotations.mapNotNull { annotation ->
                        when (annotation) {
                            is Target -> Modifier.Target
                            is Fantom -> Modifier.Fantom
                            is Source -> Modifier.Source
                            else -> null
                        }
                    }
                inherit(companion.description, inheritanceModifier)
            }
        }

        finalFields(kind, eventClass)
    }
}

private fun buildFactEvent(eventClass: KClass<out Event>): EventDescription {
    val kind = Kind.Fact
    val (descriptionName, eventModifiers) = prepareFinalEvent(kind, eventClass)
    return describeEvent(descriptionName, kind) {
        modifiers += eventModifiers

        eventClass.supertypes.forEach { supertype ->
            val companion = supertype.jvmErasure.companionObjectInstance
            if (companion is EventCompanion) {
                val inheritanceModifier: List<Modifier.InheritanceBound> =
                    supertype.annotations.mapNotNull { annotation ->
                        when (annotation) {
                            is Target, is Fantom, is Source -> invalidSchema("facts do not interact with matter")
                            else -> null
                        }
                    }
                inherit(companion.description, inheritanceModifier)
            }
        }

        finalFields(kind, eventClass)
        fields.forEach { field ->
            checkSchema(Modifier.AutoIncrement !in field.modifiers) {
                "facts has no state and cannot have autoincrement properties"
            }
        }
    }
}

private fun prepareFinalEvent(
    kind: Kind,
    eventClass: KClass<out Event>
): Pair<String, List<Modifier.EventBound>> {
    val kindName = kind.name.toLowerCase()
    checkSchema(eventClass.typeParameters.isEmpty()) { "$kindName event should not have type parameters" }
    var eventName = eventClass.qualifiedName?.removeSuffix("Event")
    val eventModifiers: List<Modifier.EventBound> = eventClass.annotations.mapNotNull { annotation ->
        when (annotation) {
            is WithName -> {
                eventName = annotation.name
                null
            }
            is Target, is Fantom, is Unique, is Shared ->
                invalidSchema("'${annotation::class.simpleName}' modifier cannot be applied to $kindName event")
            else -> null
        }
    }
    val descriptionName = checkSchemaNotNull(eventName) { "$kindName event should have name" }
    return descriptionName to eventModifiers
}

private fun EventDescriptionBuilder.finalFields(
    kind: Kind,
    eventClass: KClass<out Event>
) {
    val constructor = checkSchemaNotNull(eventClass.primaryConstructor) {
        "${kind.name.toLowerCase()} event should have primary constructor"
    }
    val names = mutableSetOf<String>()
    constructor.parameters.mapNotNullTo(names) { parameter ->
        if (parameter.hasAnnotation<Ignore>()) {
            checkSchema(parameter.isOptional) {
                "ignored constructor parameter '${parameter.name}' should be optional"
            }
            null
        } else {
            checkSchemaNotNull(parameter.name) { "constructor parameter should have name" }
        }
    }
    val properties = mutableMapOf<String, KProperty1<out Event, *>>()
    eventClass.declaredMemberProperties
        .filter { property ->
            val name = property.name
            if (name in names) {
                names -= name
                true
            } else {
                false
            }
        }
        .associateByTo(properties) { it.name }
    checkSchema(names.isEmpty()) {
        "some constructor parameters does not have associated properties: $names"
    }
    collectFields(inherits).forEach { properties.remove(it.name) }
    if (kind == Kind.Direct) {
        properties.remove("direction")
    }
    createFields(properties.values, true)
}

private fun isIncremental(type: Types) = when (type) {
    Types.Byte, Types.Short, Types.Int, Types.Long,
    Types.UByte, Types.UShort, Types.UInt, Types.ULong,
    Types.Char -> true
    else -> false
}

private fun EventDescriptionBuilder.createFields(
    properties: Collection<KProperty1<out Event, *>>,
    allowDirection: Boolean = false
) {
    properties.forEach { property ->
        var fieldName = property.name
        checkSchema((allowDirection || fieldName != "direction") && fieldName != "class" && fieldName != "extras") {
            "property name '$fieldName' is reserved in events"
        }
        val returnType = property.returnType
        val type = kTypeToMcsdbType(returnType)
        val fieldModifiers = mutableListOf<Modifier.FieldBound>()
        property.annotations.mapNotNullTo(fieldModifiers) { annotation ->
            val modifier: Modifier.FieldBound? = when (annotation) {
                is WithName -> {
                    fieldName = annotation.name; null
                }
                is AutoIncrement -> {
                    checkSchema(isIncremental(type)) { "'AutoIncrement' modifier cannot be applied to type '$type'" }
                    Modifier.AutoIncrement
                }
                is Indexed -> Modifier.Indexed
                else -> null
            }
            modifier
        }
        if (returnType.isMarkedNullable) {
            fieldModifiers += Modifier.Nullable
        }
        field(fieldName, type, fieldModifiers)
    }
}

private fun collectFields(inherits: Collection<InheritanceDescription>) =
    inherits.flatMap { it.description.allFields }

internal actual fun buildPlatformExtra(companion: EventCompanion): Any {
    val eventClass = companion.eventClass
    val description = companion.description
    val kind = description.kind
    val fields = description.allFields.associateBy { it.name }
    val constructor = eventClass.primaryConstructor!!
    val parameters = constructor.parameters.associateBy { it.name!! }
    val result = mutableListOf<Triple<String, KParameter, FieldDescription?>>()
    eventClass.declaredMemberProperties.forEach { property ->
        val codeName = property.name
        val name = property.findAnnotation<WithName>()?.name ?: codeName
        val field = fields[name]
        if (field != null || (kind == Kind.Direct && name == "direction")) {
            result += Triple(name, parameters[codeName]!!, field)
        }
    }
    return result
}

internal actual fun wrapEventToStatic(companion: EventCompanion, eventData: Dynamic): StaticEvent {
    val eventClass = companion.eventClass
    val description = companion.description
    if (description.kind == Kind.Interface) {
        throw NotImplementedError("projection events are not implemented yet")
    }
    val constructor = eventClass.primaryConstructor!!

    @Suppress("UNCHECKED_CAST")
    val parameters = companion.extra as List<Triple<String, KParameter, FieldDescription?>>
    val arguments = parameters.associate { (name, parameter, field) ->
        val raw = eventData.getOrNull(name)
        val value = if (field == null || raw == null) {
            raw
        } else {
            when (field.type) {
                Types.Structure -> {
                    val annotation = parameter.findAnnotation<Structure>()
                    if (annotation == null || annotation.serializer == KSerializer::class) {
                        val type = parameter.type
                        if (type.jvmErasure != Any::class) {
                            val serializer = serializer(type)
                            decodeFromStructure(serializer, raw)
                        } else {
                            raw
                        }
                    } else {
                        @Suppress("UNCHECKED_CAST")
                        val serializer = annotation.serializer as KSerializer<Any?>
                        decodeFromStructure(serializer, raw)
                    }
                }
                Types.Arbitrary -> {
                    raw as ByteArray
                    val annotation = parameter.findAnnotation<Arbitrary>()
                    if (annotation == null || annotation.serializer == KSerializer::class) {
                        val type = parameter.type
                        if (type.jvmErasure != ByteArray::class) {
                            val serializer = serializer(type)
                            decodeFromArbitrary(serializer, raw)
                        } else {
                            raw
                        }
                    } else {
                        @Suppress("UNCHECKED_CAST")
                        val serializer = annotation.serializer as KSerializer<Any?>
                        decodeFromArbitrary(serializer, raw)
                    }
                }
                else -> raw
            }
        }
        parameter to value
    }
    return constructor.callBy(arguments) as StaticEvent
}

internal actual fun tryGetEventCompanionForEvent(`class`: KClass<out Event>): EventCompanion? {
    val companion = `class`.companionObjectInstance
    return companion as? EventCompanion
}

private fun elseBlock(annotation: Arbitrary, property: KProperty<*>, value: Any?): ByteArray {
    @Suppress("UNCHECKED_CAST")
    val serializer = if (annotation.serializer == KSerializer::class) {
        serializer(property.returnType)
    } else {
        annotation.serializer.objectInstance
            ?: TODO("currently only object instances are supported for serializers")
    } as KSerializer<Any?>
    return encodeToArbitrary(serializer, value)
}

internal actual fun event2tag(event: StaticEvent): Dynamic {
    val fields = descriptionOf(event).allFields.associateBy { it.name }
    return buildDynamic {
        if (event is DirectEvent) {
            "direction" assign event.direction
        }
        for (property in event::class.memberProperties) {
            val name = property.findAnnotation<WithName>()?.name ?: property.name
            val field = fields[name] ?: continue
            @Suppress("UNCHECKED_CAST")
            property as KProperty1<StaticEvent, *>
            val value = property.get(event)
            val newValue = when (field.type) {
                Types.Structure -> {
                    if (value == null) {
                        null
                    } else {
                        val annotation = property.findAnnotation<Structure>()

                        @Suppress("UNCHECKED_CAST")
                        val serializer = if (annotation == null || annotation.serializer == KSerializer::class) {
                            serializer(property.returnType)
                        } else {
                            annotation.serializer.objectInstance
                                ?: TODO("currently only object instances are supported for serializers")
                        } as KSerializer<Any?>
                        encodeToStructure(serializer, value)
                    }
                }
                Types.Arbitrary -> {
                    val annotation = property.findAnnotation<Arbitrary>()
                    if (annotation == null || annotation.serializer == KSerializer::class) {
                        when (value) {
                            null, is ByteArray -> value
                            else -> elseBlock(annotation!!, property, value)
                        }
                    } else {
                        elseBlock(annotation, property, value)
                    }
                }
                Types.Serial -> if (value == Serial.nil) null else value
                Types.UUID -> if (value == UUID.nil) null else value
                else -> {
                    if (Modifier.AutoIncrement in field.modifiers) {
                        when (value) {
                            null -> null
                            is Char -> {
                                if (value == 0.toChar()) null else value
                            }
                            else -> {
                                if ((value as Number).toLong() == 0L) null else value
                            }
                        }
                    } else {
                        value
                    }
                }
            }
            name assign newValue
        }
    }
}

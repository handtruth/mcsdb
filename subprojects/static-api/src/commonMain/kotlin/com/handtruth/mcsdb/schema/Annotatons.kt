package com.handtruth.mcsdb.schema

import kotlinx.serialization.KSerializer
import kotlin.reflect.KClass
import kotlin.annotation.Target as KTarget

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.CLASS, AnnotationTarget.PROPERTY)
public annotation class WithName(val name: String)

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.TYPE, AnnotationTarget.CLASS)
public annotation class Target

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.TYPE, AnnotationTarget.CLASS)
public annotation class Fantom

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.CLASS)
public annotation class Unique

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.CLASS)
public annotation class Shared

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.PROPERTY)
public annotation class AutoIncrement

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.TYPE)
public annotation class Source

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.PROPERTY)
public annotation class Ignore

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.PROPERTY)
public annotation class Structure(val serializer: KClass<out KSerializer<*>> = KSerializer::class)

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.PROPERTY)
public annotation class Arbitrary(val serializer: KClass<out KSerializer<*>> = KSerializer::class)

@Retention(AnnotationRetention.RUNTIME)
@KTarget(AnnotationTarget.PROPERTY)
public annotation class Indexed

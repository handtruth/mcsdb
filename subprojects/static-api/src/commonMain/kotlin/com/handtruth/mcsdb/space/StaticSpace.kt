package com.handtruth.mcsdb.space

import com.handtruth.mcsdb.StaticDatabase

internal class StaticSpace(override val database: StaticDatabase) : Space by database.inner.space

package com.handtruth.mcsdb

import com.handtruth.mcsdb.variability.Variability

internal class StaticVariability(override val database: StaticDatabase) : Variability by database.inner.variability

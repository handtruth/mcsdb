package com.handtruth.mcsdb.schema

import com.handtruth.mcsdb.StaticDatabase
import com.handtruth.mcsdb.time.*

internal class StaticSchema(override val database: StaticDatabase) : Schema by database.inner.schema

public suspend inline fun Schema.register(vararg companions: EventCompanion) {
    register(companions.asSequence().map { it.description }.asIterable())
}

public suspend inline fun <reified E : StaticEvent> Schema.react(
    crossinline block: suspend (E) -> E
): Unit = react(eventCompanion<E>().description, Reactor<E>(block))

public suspend inline fun <reified E : StaticEvent> Schema.reactBefore(
    crossinline block: suspend (E) -> E
): Unit = reactBefore(eventCompanion<E>().description, Reactor<E>(block))

public suspend inline fun <reified E : StaticEvent> Schema.reactAfter(
    crossinline block: suspend (E) -> E
): Unit = reactAfter(eventCompanion<E>().description, Reactor<E>(block))

package com.handtruth.mcsdb

import com.handtruth.mcsdb.schema.StaticSchema
import com.handtruth.mcsdb.space.StaticSpace
import com.handtruth.mcsdb.time.StaticTime

internal class StaticDatabase(internal val inner: Database) : Database by inner {
    override val schema = StaticSchema(this)
    override val space = StaticSpace(this)
    override val time = StaticTime(this)
    override val variability = StaticVariability(this)
}

public fun Database.asStatic(): Database {
    return if (this is StaticDatabase) {
        this
    } else {
        StaticDatabase(this)
    }
}

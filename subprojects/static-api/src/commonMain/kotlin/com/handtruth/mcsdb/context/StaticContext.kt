package com.handtruth.mcsdb.context

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.FilterBuilderContext
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.schema.Reactor
import com.handtruth.mcsdb.schema.Schema
import com.handtruth.mcsdb.time.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.Flow

public suspend inline fun Schema.Companion.register(vararg companions: EventCompanion) {
    register(companions.asSequence().map { it.description }.asIterable())
}

public suspend inline fun <reified E : StaticEvent> Schema.Companion.react(
    crossinline block: suspend (E) -> E
): Unit = react(eventCompanion<E>().description, Reactor<E>(block))

public suspend inline fun <reified E : StaticEvent> Schema.Companion.reactBefore(
    crossinline block: suspend (E) -> E
): Unit = reactBefore(eventCompanion<E>().description, Reactor<E>(block))

public suspend inline fun <reified E : StaticEvent> Schema.Companion.reactAfter(
    crossinline block: suspend (E) -> E
): Unit = reactAfter(eventCompanion<E>().description, Reactor<E>(block))

public suspend inline fun <reified E : Event> Time.Companion.listen(
    actual: Boolean = false,
    stream: Boolean = true,
    filter: Filter
): Flow<E> {
    val context = getContext()
    return context.database.time.listen<E>(context.state, actual, stream, filter)
}

public suspend inline fun <reified E : Event> Time.Companion.listen(
    actual: Boolean = false,
    stream: Boolean = true,
    expression: FilterBuilderContext.() -> Filter
): Flow<E> {
    return listen<E>(actual, stream, filter(expression))
}

public suspend inline fun <reified E : StaticEvent> Time.Companion.raise(prototype: E): E {
    return cast(Time.raise(prototype as Event))
}

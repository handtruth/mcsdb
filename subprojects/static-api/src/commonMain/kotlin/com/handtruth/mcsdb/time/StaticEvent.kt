package com.handtruth.mcsdb.time

import com.handtruth.mc.types.Dynamic
import com.handtruth.mc.types.MutableDynamic
import com.handtruth.mc.types.contentDeepEquals
import com.handtruth.mc.types.get
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.companion
import com.handtruth.mcsdb.schema.invalidSchema
import com.handtruth.mcsdb.util.ThreadLocal
import com.handtruth.mcsdb.util.use
import kotlinx.datetime.Instant
import kotlin.reflect.KClass

public abstract class EventCompanion {
    public open val eventClass: KClass<out Event> get() = getEventClassForCompanion(this::class)
    public open val description: EventDescription by lazy { buildDescriptionForCompanion(this) }
    internal val extra: Any by lazy { buildPlatformExtra(this) }
    public open fun wrap(eventData: Dynamic): StaticEvent = wrapEventToStatic(this, eventData)
}

internal val eventInfoVar = ThreadLocal<EventInfo>()

private data class StaticEventInfo(
    override val timestamp: Instant?,
    override val throwable: Throwable?,
    override val error: String?
) : EventInfo

internal val currentEventInfo get() = eventInfoVar.get() ?: StaticEventInfo(null, null, null)

internal inline fun <R> withEventInfo(eventInfo: EventInfo, block: () -> R): R {
    return eventInfoVar.use(eventInfo, block)
}

public sealed class StaticEvent : Event {
    private val description: EventDescription = getEventCompanionForEvent(this::class).description
    final override fun `$description$`(): EventDescription = description
    private val info = currentEventInfo
    final override fun `$info$`(): EventInfo = info
    override fun `$data$`(): Dynamic = event2tag(this)

    override fun equals(other: Any?): Boolean =
        other === this || other is StaticEvent && this::class == other::class &&
            description == other.description &&
            dataOf(this).contentDeepEquals(dataOf(other))

    override fun hashCode(): Int = description.hashCode() + dataOf(this).hashCode()
}

public abstract class DirectEvent : StaticEvent() {
    public abstract val direction: Boolean

    override fun `$reverse$`(): Event? {
        val data = `$data$`() as MutableDynamic
        data["direction"] = !(data["direction"] as Boolean)
        val companion = `$description$`().companion ?: error("EventCompanion not found")
        return companion.wrap(data)
    }
}

public abstract class FactEvent : StaticEvent() {
    override fun `$reverse$`(): Event? = null
}

internal expect fun getEventClassForCompanion(`class`: KClass<out EventCompanion>): KClass<out Event>

internal expect fun buildDescriptionForCompanion(companion: EventCompanion): EventDescription

internal expect fun buildPlatformExtra(companion: EventCompanion): Any

internal expect fun wrapEventToStatic(companion: EventCompanion, eventData: Dynamic): StaticEvent

internal expect fun tryGetEventCompanionForEvent(`class`: KClass<out Event>): EventCompanion?

internal fun getEventCompanionForEvent(`class`: KClass<out Event>): EventCompanion {
    return tryGetEventCompanionForEvent(`class`) ?: invalidSchema("there are no event companion for $`class`")
}

public fun <E : Event> eventCompanionOrNull(`class`: KClass<E>): EventCompanion? = tryGetEventCompanionForEvent(`class`)

public fun <E : Event> eventCompanion(`class`: KClass<E>): EventCompanion = getEventCompanionForEvent(`class`)

public inline fun <reified E : Event> eventCompanion(): EventCompanion = eventCompanion(E::class)

public inline fun <reified E : Event> eventCompanionOrNull(): EventCompanion? = eventCompanionOrNull(E::class)

internal expect fun event2tag(event: StaticEvent): Dynamic

public fun Event.tryStatic(): Event {
    return toStaticOrNull() ?: this
}

public fun Event.toStatic(): StaticEvent {
    return requireNotNull(toStaticOrNull()) { "event has no static variant" }
}

public fun Event.toStaticOrNull(): StaticEvent? {
    return if (this is StaticEvent) {
        this
    } else {
        val description = descriptionOf(this)
        val companion = description.companion
        if (companion != null) {
            val info = infoOf(this)
            val data = dataOf(this)
            withEventInfo(info) { companion.wrap(data) }
        } else {
            null
        }
    }
}

public fun Event.toDynamic(): AnyEvent {
    return if (this is AnyEvent) {
        this
    } else {
        val description = descriptionOf(this)
        val info = infoOf(this)
        val data = dataOf(this)
        return description.event(info, data)
    }
}

public inline fun <reified E : Event> cast(event: Event): E = event.toStatic() as E

public inline fun <reified E : DirectEvent> cancel(event: E): E = cancel(event as Event) as E

@Suppress("UNUSED_PARAMETER")
@Deprecated("facts are irreversible events")
public inline fun <reified E : FactEvent> cancel(event: E): Nothing? = null

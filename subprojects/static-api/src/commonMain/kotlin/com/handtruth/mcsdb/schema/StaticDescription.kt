package com.handtruth.mcsdb.schema

import com.handtruth.kommon.getBeanOrNull
import com.handtruth.mcsdb.time.EventCompanion

public val EventDescription.companion: EventCompanion? get() = getBeanOrNull()

package com.handtruth.mcsdb.schema

import com.handtruth.mcsdb.InternalMcsdbApi
import kotlin.reflect.KType
import kotlin.reflect.typeOf

@InternalMcsdbApi
public expect fun kTypeToMcsdbType(type: KType): Types

public inline fun EventDescriptionBuilder.field(
    name: String,
    type: KType,
    block: FieldDescriptionBuilder.() -> Unit
): FieldDescription = field(name, kTypeToMcsdbType(type), block)

public inline fun <reified T> EventDescriptionBuilder.field(
    name: String,
    block: FieldDescriptionBuilder.() -> Unit
): FieldDescription = field(name, kTypeToMcsdbType(typeOf<T>()), block)

public fun EventDescriptionBuilder.field(
    name: String,
    type: KType,
    modifiers: Collection<Modifier.FieldBound>
): FieldDescription = field(name, kTypeToMcsdbType(type), modifiers)

public inline fun <reified T> EventDescriptionBuilder.field(name: String): FieldDescription =
    field(name, kTypeToMcsdbType(typeOf<T>()))

package com.handtruth.mcsdb

import com.handtruth.mcsdb.schema.Member
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.EventCompanion
import com.handtruth.mcsdb.time.StaticEvent
import com.handtruth.mcsdb.time.eventCompanion
import kotlin.reflect.KProperty1

public fun FilterBuilderContext.type(companion: EventCompanion): Filter.Role = type(companion.description)
public inline fun <reified E : StaticEvent> FilterBuilderContext.type(): Filter.Role = type(eventCompanion<E>())

public fun FilterBuilderContext.source(companion: EventCompanion): Filter.Role = source(companion.description)
public inline fun <reified E : Event> FilterBuilderContext.source(): Filter.Role = source(eventCompanion<E>())

public fun FilterBuilderContext.fantom(companion: EventCompanion): Filter.Role = fantom(companion.description)
public inline fun <reified E : Event> FilterBuilderContext.fantom(): Filter.Role = fantom(eventCompanion<E>())

public fun FilterBuilderContext.target(companion: EventCompanion): Filter.Role = target(companion.description)
public inline fun <reified E : Event> FilterBuilderContext.target(): Filter.Role = target(eventCompanion<E>())

public fun FilterBuilderContext.attached(companion: EventCompanion): Filter.Attached = attached(companion.description)
public inline fun <reified E : Event> FilterBuilderContext.attached(): Filter.Attached =
    attached(eventCompanion<E>())

public fun FilterBuilderContext.primary(companion: EventCompanion): Filter = primary(companion.description)
public inline fun <reified E : Event> FilterBuilderContext.primary(): Filter = primary(eventCompanion<E>())

public inline fun <reified E : Event, T> m(property: KProperty1<E, T>): Member {
    return eventCompanion<E>().description[property.name]
}

public inline fun FilterBuilderContext.via(
    companion: EventCompanion,
    block: FilterBuilderContext.() -> Filter
): Filter.Via = via(companion.description, block)

public inline fun <reified E : Event> FilterBuilderContext.via(
    block: FilterBuilderContext.() -> Filter
): Filter.Via = via(eventCompanion<E>(), block)

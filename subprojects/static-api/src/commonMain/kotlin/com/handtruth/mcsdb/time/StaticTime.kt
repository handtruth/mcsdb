package com.handtruth.mcsdb.time

import com.handtruth.mc.graph.MutableGraph
import com.handtruth.mc.graph.copy
import com.handtruth.mcsdb.*
import com.handtruth.mcsdb.schema.CausalRelationships
import com.handtruth.mcsdb.variability.State
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull

internal class StaticTime(override val database: StaticDatabase) : Time by database.inner.time {
    private val inner get() = database.inner.time

    override fun listen(state: State, actual: Boolean, stream: Boolean, filter: Filter): Flow<Event> {
        return inner.listen(state, actual, stream, filter).map {
            it.tryStatic()
        }
    }

    override suspend fun raise(state: State, prototype: Event): Event {
        return inner.raise(state, prototype.toDynamic()).tryStatic()
    }

    override suspend fun ask(state: State, timeQuery: TimeQuery): List<Event> {
        return inner.ask(state, timeQuery).map { it.tryStatic() }
    }

    override suspend fun subgraph(state: State, timeQuery: TimeQuery): History {
        val history = inner.subgraph(state, timeQuery)
        val newHistory = if (history is MutableGraph<Event, CausalRelationships>) {
            history
        } else {
            history.copy()
        }
        for (vertex in newHistory.vertices) {
            vertex.value = vertex.value.tryStatic()
        }
        return newHistory
    }
}

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
public inline fun <reified E : Event> Time.listen(
    state: State,
    actual: Boolean = false,
    stream: Boolean = true,
    filter: Filter
): Flow<E> {
    val companion = eventCompanionOrNull<E>()
    return if (companion == null) {
        listen(state, actual, stream, filter).mapNotNull {
            it.tryStatic() as? E
        }
    } else {
        listen(state, actual, stream, filter { filter and attached(companion) }).map {
            cast<E>(it)
        }
    }
}

public inline fun <reified E : Event> Time.listen(
    state: State,
    actual: Boolean = false,
    stream: Boolean = true,
    expression: FilterBuilderContext.() -> Filter
): Flow<E> {
    return listen<E>(state, actual, stream, filter(expression))
}

package com.handtruth.mcsdb.schema

import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.StaticEvent
import com.handtruth.mcsdb.time.cast

public inline fun <reified E : StaticEvent> Reactor(crossinline reactor: suspend (event: E) -> E): Reactor =
    object : Reactor {
        override suspend fun react(event: Event) = reactor(cast(event))
        override fun toString() = "reactor"
    }

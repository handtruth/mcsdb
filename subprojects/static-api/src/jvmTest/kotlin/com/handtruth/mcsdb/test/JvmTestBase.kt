package com.handtruth.mcsdb.test

import com.handtruth.mc.graph.Graph
import guru.nidi.graphviz.attribute.*
import guru.nidi.graphviz.engine.Format
import guru.nidi.graphviz.graph
import guru.nidi.graphviz.toGraphviz
import java.io.File

private fun <E> colorOf(edge: E): Int {
    return edge.hashCode().inv() and 0xFFFFFF00.toInt()
}

actual fun <V, E> Graph<V, E>.toPNG(file: String, topologyOnly: Boolean) {
    val dot = graph(directed = true) {
        node[Shape.RECTANGLE]
        var i = 0
        val nodes = vertices.associateWith { v ->
            -("${++i}" + if (!topologyOnly) ": ${v.value}" else "")
        }
        edges.forEach { edge ->
            (nodes[edge.source]!! - nodes[edge.target]!!)[Color.rgba(colorOf(edge.value))]
        }
    }
    dot.toGraphviz().render(Format.PNG).toFile(File(file))
}

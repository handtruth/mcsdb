package com.handtruth.mcsdb

import com.handtruth.kommon.BeanJar
import com.handtruth.mcsdb.schema.SQLiteSchema
import com.handtruth.mcsdb.space.SQLiteSpace
import com.handtruth.mcsdb.storage.Storage
import com.handtruth.mcsdb.time.SQLiteTime
import com.handtruth.mcsdb.variability.SQLiteVariability

class SQLiteDatabase(internal val storage: Storage) : Database {
    override val beanJar: BeanJar = BeanJar.Sync()

    override val schema = SQLiteSchema(this)
    override val time = SQLiteTime(this)
    override val space = SQLiteSpace(this)
    override val variability = SQLiteVariability(this)

    override fun close() {
        storage.close()
    }
}

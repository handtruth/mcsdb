package com.handtruth.mcsdb.time

import kotlinx.atomicfu.locks.SynchronizedObject
import kotlinx.atomicfu.locks.synchronized
import kotlin.coroutines.CoroutineContext

internal data class IdentifiedEvent(val event: Event, val id: Long, val state: Long)

internal class ReactionContext(val transactionKey: TransactionKey) : CoroutineContext.Element {
    data class TransactionKey(val connection: Any, val state: Long)

    private val _events = arrayListOf<IdentifiedEvent>()

    private val lock = SynchronizedObject()

    fun add(event: IdentifiedEvent) {
        synchronized(lock) {
            _events += event
        }
    }

    val events: List<IdentifiedEvent> get() = _events
    val ids: List<Long> get() = events.map { it.id }

    override val key get() = Key

    companion object Key : CoroutineContext.Key<ReactionContext>
}

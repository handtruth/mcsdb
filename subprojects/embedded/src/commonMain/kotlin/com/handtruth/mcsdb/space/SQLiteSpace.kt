package com.handtruth.mcsdb.space

import com.handtruth.mcsdb.SQLiteDatabase
import com.handtruth.mcsdb.variability.State

class SQLiteSpace(override val database: SQLiteDatabase) : Space {
    override suspend fun ask(state: State, spaceQuery: SpaceQuery): Set<Particle> {
        database.schema.registerAll(spaceQuery)
        return database.storage.querySpace((spaceQuery as SpaceQuery.Select).filter, state.id)
    }

    override suspend fun subgraph(state: State, spaceQuery: SpaceQuery): Matter {
        database.schema.registerAll(spaceQuery)
        return database.storage.extractMatter((spaceQuery as SpaceQuery.Select).filter, state.id)
    }
}

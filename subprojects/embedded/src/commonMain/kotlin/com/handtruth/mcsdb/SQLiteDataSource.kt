package com.handtruth.mcsdb

import com.handtruth.mcsdb.storage.sqliteStorage

object SQLiteDataSource : DataSource {
    override val protocol get() = "sqlite"

    override suspend fun connect(location: String): Database {
        val storage = sqliteStorage(location.removePrefix("sqlite:"))
        return SQLiteDatabase(storage)
    }
}

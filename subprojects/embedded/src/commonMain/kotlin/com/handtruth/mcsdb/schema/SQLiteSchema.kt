package com.handtruth.mcsdb.schema

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.SQLiteDatabase
import com.handtruth.mcsdb.space.SpaceQuery
import com.handtruth.mcsdb.time.TimeQuery
import com.handtruth.mcsdb.variability.VariabilityQuery
import kotlinx.atomicfu.locks.SynchronizedObject

class SQLiteSchema(override val database: SQLiteDatabase) : Schema {
    private val reactors = hashMapOf<Long, Reactor>()

    private val lock = SynchronizedObject()

    private operator fun MutableMap<String, EventDescription>.plusAssign(description: EventDescription) {
        put(description.name, description)
    }

    private fun collectDescriptions(descriptions: MutableMap<String, EventDescription>, filter: Filter) {
        when (filter) {
            is Filter.Role -> descriptions += filter.description
            is Filter.Attached -> descriptions += filter.description
            is Filter.Expression -> descriptions += filter.member.description
            is Filter.Via -> {
                descriptions += filter.description
                collectDescriptions(descriptions, filter.filter)
            }
            is Filter.Unary.Negation -> {
                collectDescriptions(descriptions, filter.filter)
            }
            is Filter.Binary -> {
                collectDescriptions(descriptions, filter.a)
                collectDescriptions(descriptions, filter.b)
            }
            else -> {
                /* do nothing */
            }
        }
    }

    internal suspend fun registerAll(filter: Filter) {
        val descriptions = hashMapOf<String, EventDescription>()
        collectDescriptions(descriptions, filter)
        register(descriptions.values)
    }

    private fun collectDescriptions(descriptions: MutableMap<String, EventDescription>, timeQuery: TimeQuery) {
        when (timeQuery) {
            is TimeQuery.Order -> collectDescriptions(descriptions, timeQuery.timeQuery)
            is TimeQuery.Cause -> collectDescriptions(descriptions, timeQuery.timeQuery)
            is TimeQuery.From -> collectDescriptions(descriptions, timeQuery.filter)
            is TimeQuery.Unary.Negation -> collectDescriptions(descriptions, timeQuery.timeQuery)
            is TimeQuery.Binary -> {
                collectDescriptions(descriptions, timeQuery.a)
                collectDescriptions(descriptions, timeQuery.b)
            }
            else -> {
                /* do nothing */
            }
        }
    }

    internal suspend fun registerAll(timeQuery: TimeQuery) {
        val descriptions = hashMapOf<String, EventDescription>()
        collectDescriptions(descriptions, timeQuery)
        register(descriptions.values)
    }

    private fun collectDescriptions(descriptions: MutableMap<String, EventDescription>, spaceQuery: SpaceQuery) {
        when (spaceQuery) {
            is SpaceQuery.Select -> collectDescriptions(descriptions, spaceQuery.filter)
        }
    }

    internal suspend fun registerAll(spaceQuery: SpaceQuery) {
        val descriptions = hashMapOf<String, EventDescription>()
        collectDescriptions(descriptions, spaceQuery)
        register(descriptions.values)
    }

    private fun collectDescriptions(
        descriptions: MutableMap<String, EventDescription>,
        variabilityQuery: VariabilityQuery
    ) {
        when (variabilityQuery) {
            is VariabilityQuery.Contains -> collectDescriptions(descriptions, variabilityQuery.filter)
            is VariabilityQuery.Happened -> collectDescriptions(descriptions, variabilityQuery.timeQuery)
            is VariabilityQuery.Unary.Negation -> collectDescriptions(descriptions, variabilityQuery.variabilityQuery)
            is VariabilityQuery.Binary -> {
                collectDescriptions(descriptions, variabilityQuery.a)
                collectDescriptions(descriptions, variabilityQuery.b)
            }
            else -> {
                /* do nothing */
            }
        }
    }

    internal suspend fun registerAll(variabilityQuery: VariabilityQuery) {
        val descriptions = hashMapOf<String, EventDescription>()
        collectDescriptions(descriptions, variabilityQuery)
        register(descriptions.values)
    }

    override suspend fun register(descriptions: Iterable<EventDescription>) {
        descriptions.forEach { database.storage.registerEventDescription(it) }
    }

    override suspend fun react(description: EventDescription, reactor: Reactor?) {
        require(description.kind != Kind.Interface) { "interface cannot have reactors" }
        val eventClass = database.storage.registerEventDescription(description)
        kotlinx.atomicfu.locks.synchronized(lock) {
            if (reactor === null) {
                reactors.remove(eventClass.id)
            } else {
                reactors[eventClass.id] = reactor
            }
        }
    }

    override fun reactorOf(description: EventDescription): Reactor? {
        val eventClass = database.storage.classRepository.classByName[description.name] ?: return null
        return kotlinx.atomicfu.locks.synchronized(lock) { reactors[eventClass.id] }
    }
}

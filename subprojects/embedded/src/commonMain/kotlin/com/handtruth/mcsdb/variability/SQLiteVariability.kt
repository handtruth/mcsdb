package com.handtruth.mcsdb.variability

import com.handtruth.mcsdb.SQLiteDatabase

class SQLiteVariability(override val database: SQLiteDatabase) : Variability {
    override suspend fun ask(variabilityQuery: VariabilityQuery): Set<State> {
        database.schema.registerAll(variabilityQuery)
        return database.storage.queryVariability(variabilityQuery)
    }

    override suspend fun fork(name: String, state: State): State {
        val newStateId = database.storage.fork(state.id, name)
        return NamedState(newStateId, name)
    }

    override suspend fun create(name: String): State {
        val newStateId = database.storage.createState(name)
        return NamedState(newStateId, name)
    }

    override suspend fun forget(state: State) {
        database.storage.deleteState(state.id)
    }

    override suspend fun setReactive(state: State, reactive: Boolean) {
        database.storage.setReactive(state.id, reactive)
    }
}

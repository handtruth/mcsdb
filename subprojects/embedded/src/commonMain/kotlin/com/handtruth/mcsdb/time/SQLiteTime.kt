package com.handtruth.mcsdb.time

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.SQLiteDatabase
import com.handtruth.mcsdb.context.Context
import com.handtruth.mcsdb.variability.State
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock

class SQLiteTime(override val database: SQLiteDatabase) : Time {
    private val connectionKey = Any()

    override suspend fun ask(state: State, timeQuery: TimeQuery): List<Event> {
        database.schema.registerAll(timeQuery)
        return database.storage.queryTime(timeQuery, state.id)
    }

    private val eventBus = MutableSharedFlow<IdentifiedEvent>()

    private suspend fun fire(
        state: State,
        error: Throwable?,
        prototype: Event,
        causal: Collection<Long>
    ): IdentifiedEvent {
        val description = descriptionOf(prototype)
        val event = description.event(Clock.System.now(), error, dataOf(prototype))
        val id = database.storage.saveEvent(event, state.id, causal)
        val result = IdentifiedEvent(event, id, state.id)
        currentReactionContext(state)?.add(result)
        eventBus.emit(result)
        return result
    }

    private suspend fun cancelFrame(
        state: State,
        frame: ReactionContext,
        throwable: Throwable
    ): Collection<Long> {
        val reactionContext = ReactionContext(frame.transactionKey)
        withContext(reactionContext) {
            for ((event) in frame.events) {
                val info = infoOf(event)
                if (!info.succeeded) {
                    continue
                }
                val reversedPrototype = cancel(event) ?: continue
                try {
                    reactionFrame(reversedPrototype, state)
                } catch (thr: Throwable) {
                    // error inside error: PANIC!!!
                    throwable.addSuppressed(thr)
                    fire(state, thr, reversedPrototype, emptyList())
                    return@withContext
                }
            }
        }
        return reactionContext.ids
    }

    // это суть того, что мне так не хватало в MCSMan, реактор, "написанный" MCSDB автоматически
    private suspend fun modelledReactor(prototypes: List<Event>, state: State) {
        for (nextPrototype in prototypes) {
            reactionFrame(nextPrototype, state)
        }
    }

    private suspend fun reactionFrame(prototype: Event, state: State): IdentifiedEvent {
        val transactionKey = ReactionContext.TransactionKey(connectionKey, state.id)
        val newReactionContext = ReactionContext(transactionKey)
        val description = descriptionOf(prototype)
        database.storage.registerEventDescription(description)
        val (modelledPrototype, nextPrototypes) = database.storage.resolveNextPrototypes(prototype, state.id)
        try {
            val actualPrototype = withContext(newReactionContext) {
                val providedRedactor = database.schema.reactorOf(description)
                modelledReactor(nextPrototypes, state)
                if (providedRedactor == null) {
                    modelledPrototype
                } else {
                    withContext(Context(database, state)) {
                        providedRedactor.react(modelledPrototype)
                    }
                }
            }
            return fire(state, null, actualPrototype, newReactionContext.ids)
        } catch (thr: Throwable) {
            val dependencies = cancelFrame(state, newReactionContext, thr)
            fire(state, thr, modelledPrototype, newReactionContext.ids + dependencies)
            throw thr
        }
    }

    private fun eventStream(state: State, filter: Filter): Flow<Event> = eventBus.mapNotNull {
        val (event, id, actualState) = it
        // emit new events only if they related to filter
        if (actualState == state.id && database.storage.filterEvent(id, filter, actualState)) {
            event
        } else {
            null
        }
    }

    override fun listen(state: State, actual: Boolean, stream: Boolean, filter: Filter): Flow<Event> {
        return if (actual) {
            if (stream) {
                // actual + stream
                flow {
                    database.schema.registerAll(filter)
                    // query all related particles
                    val sequence = database.storage.queryActualEvents(filter, state.id)
                    for (event in sequence) {
                        emit(event)
                    }
                    emitAll(eventStream(state, filter))
                }.buffer()
            } else {
                // actual only
                flow {
                    database.schema.registerAll(filter)
                    val sequence = database.storage.queryActualEvents(filter, state.id)
                    for (event in sequence) {
                        emit(event)
                    }
                }
            }
        } else {
            if (stream) {
                // stream only
                flow {
                    database.schema.registerAll(filter)
                    emitAll(eventStream(state, filter))
                }
            } else {
                // why you even bother? no stream, no particles?!
                emptyFlow()
            }
        }
    }

    private suspend fun currentReactionContext(state: State): ReactionContext? {
        val reactionContext = currentCoroutineContext()[ReactionContext]
        if (reactionContext != null) {
            val transactionKey = ReactionContext.TransactionKey(connectionKey, state.id)
            if (transactionKey == reactionContext.transactionKey) {
                return reactionContext
            }
        }
        return null
    }

    override suspend fun raise(state: State, prototype: Event): Event {
        val identifiedEvent = reactionFrame(prototype, state)
        return identifiedEvent.event
    }

    private suspend fun easyRewind(state: State, sequence: List<Event>) {
        for (prototype in sequence) {
            raise(state, prototype)
        }
    }

    private suspend fun hardRewind(state: State, sequence: List<Event>) {
        val reactionContext = ReactionContext(ReactionContext.TransactionKey(connectionKey, state.id))
        try {
            withContext(reactionContext) {
                for (event in sequence) {
                    easyRewind(state, sequence)
                }
            }
        } catch (thr: Throwable) {
            cancelFrame(state, reactionContext, thr)
            throw thr
        }
    }

    override suspend fun rewind(state: State, timeQuery: TimeQuery) {
        val sequence = database.storage.symmetry(timeQuery, state.id)
        val reactionContext = currentReactionContext(state)
        if (reactionContext == null) {
            hardRewind(state, sequence)
        } else {
            easyRewind(state, sequence)
        }
    }

    override suspend fun forget(state: State, timeQuery: TimeQuery) {
        database.schema.registerAll(timeQuery)
        database.storage.forget(timeQuery, state.id)
    }

    override suspend fun subgraph(state: State, timeQuery: TimeQuery): History {
        database.schema.registerAll(timeQuery)
        return database.storage.extractHistory(timeQuery, state.id)
    }
}

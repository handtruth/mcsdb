package com.handtruth.mcsdb.test

import com.handtruth.mc.types.get
import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.context.* // ktlint-disable no-wildcard-imports
import com.handtruth.mcsdb.open
import com.handtruth.mcsdb.schema.Schema
import com.handtruth.mcsdb.space.Space
import com.handtruth.mcsdb.space.toPrototype
import com.handtruth.mcsdb.test.models.bookEvent
import com.handtruth.mcsdb.test.models.bookshelfContextEvent
import com.handtruth.mcsdb.test.models.bookshelfEvent
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.time.cancel
import com.handtruth.mcsdb.time.dataOf
import io.ktor.test.dispatcher.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.test.Test

class EmbeddedTest {

    private fun database(block: suspend CoroutineScope.() -> Unit) = testSuspend {
        val database = Database.open("sqlite:file:test?mode=memory")
        withContext(Context(database)) {
            Schema.react(userCreationEvent) { event ->
                val data = dataOf(event)
                val userContext = userContextEvent {
                    it["user"] = data["user"]
                    it["actor"] = data["actor"]
                    it["direction"] = true
                }
                Time.raise(userContext)
                val userActor = userActorEvent {
                    it["user"] = data["user"]
                    it["direction"] = true
                }
                Time.raise(userActor)
                event
            }
            Schema.react(groupCreationEvent) { event ->
                val data = dataOf(event)
                val groupContext = groupContextEvent {
                    it["group"] = data["group"]
                    it["actor"] = data["actor"]
                    it["direction"] = true
                }
                Time.raise(groupContext)
                val groupActor = groupActorEvent {
                    it["group"] = data["group"]
                    it["direction"] = true
                }
                Time.raise(groupActor)
                val groupDescription = groupDescriptionEvent {
                    it["group"] = data["group"]
                    it["description"] = data["description"]
                    it["direction"] = true
                }
                Time.raise(groupDescription)
                event
            }
            block()
        }
    }

    @Test
    fun exampleHistory() = database {
        val systemActor = Time.raise(
            systemActorEvent {
                it["direction"] = true
            }
        )
        println(1)
        val systemActorData = dataOf(systemActor)
        val ktloActorAsync = async {
            dataOf(Time.listen { userActorEvent["user"] eq "ktlo" }.first())
        }
        println(2)
        Time.raise(
            userCreationEvent {
                it["user"] = "ktlo"
                it["actor"] = systemActorData["actor"]
            }
        )
        println(3)
        val ktloActor = ktloActorAsync.await()
        Time.raise(
            groupCreationEvent {
                it["group"] = "people"
                it["description"] = "Everybody People!"
                it["actor"] = ktloActor["actor"]
            }
        )
        Time.raise(
            userCreationEvent {
                it["user"] = "xydgiz"
                it["actor"] = ktloActor["actor"]
            }
        )
        Time.raise(
            groupMemberEvent {
                it["group"] = "people"
                it["user"] = "xydgiz"
                it["actor"] = ktloActor["actor"]
                it["direction"] = true
            }
        )
        Time.raise(
            messageEvent {
                it["user"] = "xydgiz"
                it["actor"] = ktloActor["actor"]
                it["message"] = "Hello there! :)"
            }
        )

        val systemActor2 = Space.ask { select { type(systemActorEvent) } }.first()
        println(systemActor2)
        Time.raise(
            userContextEvent {
                it["user"] = "ktlo"
                it["actor"] = systemActor2.data["actor"]
                it["direction"] = false
            }
        )
    }

    @Test
    fun booksGenerate() = database {
        launch {
            Time.listen { target(bookshelfEvent) }.take(2).collect { event ->
                println("created: $event")
            }
        }
        val bookshelf = Time.raise(
            bookshelfContextEvent {
                it["location"] = "Тут"
                it["direction"] = true
            }
        )
        val bookshelfId = dataOf(bookshelf)["bookshelf"]
        val book1 = Time.raise(
            bookEvent {
                it["name"] = "Соль Земли"
                it["bookshelf"] = bookshelfId
                it["direction"] = true
            }
        )
        val book2 = Time.raise(
            bookEvent {
                it["name"] = "Отцы и дети"
                it["bookshelf"] = bookshelfId
                it["direction"] = true
            }
        )
        println("books: [$book1, $book2]")
        val books1 = Space.ask {
            select {
                via(bookshelfEvent) { bookshelfContextEvent["location"] eq "Тут" } and target(bookshelfEvent)
            }
        }.map { it.toPrototype() }
        val antiBooks = books1.map { cancel(it)!! }
        for (event in antiBooks) {
            Time.raise(event)
        }
        for (event in books1) {
            Time.raise(event)
        }
        val books2 = Time.listen(actual = true) {
            via(bookshelfEvent) { bookshelfContextEvent["location"] eq "Тут" } and target(bookshelfEvent)
        }.take(2).toList()
        println(books2)

        val bookshelf2 = Time.listen(actual = true) {
            (bookshelfContextEvent["location"] eq "Тут") and source(bookshelfEvent)
        }.first()
        launch {
            Time.listen { target(bookshelfEvent) }.take(2).collect { event ->
                println(event)
            }
        }
        Time.raise(cancel(bookshelf2)!!)
    }
}

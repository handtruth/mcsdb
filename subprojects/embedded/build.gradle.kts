plugins {
    id("kotlinx-atomicfu")
}

dependencies {
    commonMainApi(project(":mcsdb-api"))
    commonMainImplementation(project(":mcsdb-storage"))
    commonTestImplementation("io.ktor:ktor-test-dispatcher")

    val atomicfuVersion: String by project
    commonMainImplementation("org.jetbrains.kotlinx:atomicfu:$atomicfuVersion")
    commonTestImplementation(project(":mcsdb-test-schema"))
}

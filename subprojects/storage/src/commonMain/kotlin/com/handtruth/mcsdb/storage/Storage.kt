package com.handtruth.mcsdb.storage

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.SchemaException
import com.handtruth.mcsdb.space.Matter
import com.handtruth.mcsdb.space.Particle
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.History
import com.handtruth.mcsdb.time.TimeQuery
import com.handtruth.mcsdb.variability.State
import com.handtruth.mcsdb.variability.VariabilityQuery
import io.ktor.utils.io.core.*

expect suspend fun sqliteStorage(source: String): Storage

abstract class EventClass(
    val id: Long,
    var description: EventDescription,
    val metadata: EventDescriptionMetadata
) : Closeable {
    val name get() = description.name

    val allFields get() = description.allFields
    val allRealFields get() = description.allRealFields
    val freezeFields = description.getFreezeFields()

    override fun close() {}
}

interface ClassRepository<out C : EventClass> : Closeable {
    val classByName: Map<String, C>
    val classById: Map<Long, C>
}

operator fun <C : EventClass> ClassRepository<C>.get(name: String): C {
    return classByName[name] ?: error("no event class for name \"$name\"")
}

operator fun <C : EventClass> ClassRepository<C>.get(id: Long): C {
    return classById[id] ?: error("no event class for id #$id")
}

class ClassRepositoryImplementation<C : EventClass> : ClassRepository<C> {
    private val _descriptionByName = mutableMapOf<String, C>()
    override val classByName: Map<String, C> get() = _descriptionByName

    private val _descriptionById = mutableMapOf<Long, C>()
    override val classById: Map<Long, C> get() = _descriptionById

    fun add(eventClass: C) {
        val other = _descriptionByName[eventClass.name]
        if (other != null && other.description != eventClass.description) {
            throw SchemaException(SchemaException.Codes.DescriptionIsPresent, eventClass.name)
        }
        _descriptionByName[eventClass.name] = eventClass
        _descriptionById[eventClass.id] = eventClass
    }

    override fun close() {
        classById.forEach { it.value.close() }
    }
}

interface Storage : Closeable {
    val classRepository: ClassRepository<EventClass>

    suspend fun getDefaultState(): Long
    suspend fun registerEventDescription(eventDescription: EventDescription): EventClass
    suspend fun resolveNextPrototypes(prototype: Event, state: Long): Pair<Event, List<Event>>
    suspend fun saveEvent(event: Event, state: Long, reactive: Collection<Long>): Long
    suspend fun forget(timeQuery: TimeQuery, state: Long): Int
    suspend fun querySpace(filter: Filter, state: Long): Set<Particle>
    suspend fun queryActualEvents(filter: Filter, state: Long): List<Event>
    suspend fun filterEvent(event: Long, filter: Filter, state: Long): Boolean
    suspend fun queryTime(timeQuery: TimeQuery, state: Long): List<Event>
    suspend fun symmetry(timeQuery: TimeQuery, state: Long): List<Event>
    suspend fun setReactive(state: Long, reactive: Boolean)
    suspend fun fork(state: Long, name: String): Long
    suspend fun createState(name: String): Long
    suspend fun deleteState(state: Long)
    suspend fun queryVariability(variabilityQuery: VariabilityQuery): Set<State>
    suspend fun isReactive(state: Long): Boolean

    suspend fun extractHistory(timeQuery: TimeQuery, state: Long): History
    suspend fun extractMatter(filter: Filter, state: Long): Matter

    suspend fun isEmpty(): Triple<Boolean, Boolean, Boolean>
}

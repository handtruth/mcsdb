package com.handtruth.mcsdb.storage

import com.handtruth.mcsdb.schema.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable

@Serializable
data class EventDescriptionMetadata(val lastFieldValues: MutableMap<String, Long> = mutableMapOf())

internal val eventDescriptionMetadataSerializer: KSerializer<EventDescriptionMetadata>
    get() = EventDescriptionMetadata.serializer()

fun EventDescription.doesInherit(other: EventDescription): Boolean =
    if (name == other.name) {
        true
    } else {
        inherits.any { it.description.doesInherit(other) }
    }

private fun collectFreeze(result: MutableList<Member>, names: Set<String>, description: EventDescription) {
    for (field in description.fields) {
        if (field.name !in names) {
            result += Member(description, field)
        }
    }
    for (eventInterface in description.inherits) {
        collectFreeze(result, names, eventInterface.description)
    }
}

fun EventDescription.getFreezeFields(): Map<EventDescription, List<FieldDescription>> {
    val names = allRealFields.map { it.name }.toSet()
    val result = mutableListOf<Member>()
    collectFreeze(result, names, this)
    return result.groupBy({ it.description }, { it.field })
}

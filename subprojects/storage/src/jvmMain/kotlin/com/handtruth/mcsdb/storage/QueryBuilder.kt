package com.handtruth.mcsdb.storage

import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.FieldDescription
import com.handtruth.mcsdb.schema.Modifier
import com.handtruth.mcsdb.schema.Types
import com.handtruth.mcsdb.time.TimeQuery
import com.handtruth.mcsdb.variability.VariabilityQuery

fun Appendable.tableNameOfInterface(description: EventDescription) {
    append('`')
    append(description.name)
    append("@interface`")
}

fun Appendable.fieldName(name: String) {
    append('`')
    append(name)
    append('`')
}

private inline fun <R> Appendable.field(field: FieldDescription, block: Appendable.() -> R): R {
    fieldName(field.name)
    append(' ')
    val result = block()
    fieldNull(field)
    return result
}

private fun Appendable.fieldNull(field: FieldDescription) {
    if (Modifier.Nullable in field.modifiers) {
        append(" null")
    } else {
        append(" not null")
    }
}

private fun Appendable.field(field: FieldDescription, type: String) {
    field(field) { append(type) }
}

fun Appendable.declareTableOfInterface(description: EventDescription) {
    append("create table ")
    tableNameOfInterface(description)
    append("(`.id` integer primary key not null references interface(id) on delete cascade")
    val fields = description.fields
    for (field in fields) {
        append(", ")
        when (field.type) {
            Types.Serial -> field(field, "integer")
            Types.Boolean, Types.Byte -> field(field, "integer(1)")
            Types.UByte, Types.Short -> field(field, "integer(2)")
            Types.UShort, Types.Char -> field(field, "integer(3)")
            Types.Int -> field(field, "integer(4)")
            Types.UInt -> field(field, "integer(6)")
            Types.Long -> field(field, "integer(8)")
            Types.ULong -> field(field, "blob")
            Types.Float, Types.Double -> field(field, "real")
            Types.String -> field(field, "text")
            Types.UUID -> {
                fieldName(field.name + ".most")
                append(" integer")
                fieldNull(field)
                append(", ")
                fieldName(field.name + ".least")
                append(" integer")
                fieldNull(field)
            }
            Types.Instant -> field(field, "text")
            Types.Structure, Types.Arbitrary -> field(field, "blob")
        }
    }
    if (fields.isNotEmpty()) {
        append(", unique(")
        val iter = fields.iterator()
        val first = iter.next()
        selectField(first)
        for (field in iter) {
            append(", ")
            selectField(field)
        }
        append(')')
    }
    append(");")
}

fun Appendable.uniqueInterfaceTrigger(description: EventDescription) {
    append("create trigger `")
    append(description.name)
    append("@unique` before insert on ")
    tableNameOfInterface(description)
    append(" when exists(select * from ")
    tableNameOfInterface(description)
    append(") begin select raise(abort, 'only one row!'); end;")
}

fun Appendable.buildIndex(description: EventDescription, field: FieldDescription) {
    append("create index `")
    append(description.name)
    append(':')
    append(field.name)
    append("@index` on ")
    tableNameOfInterface(description)
    append('(')
    selectField(field)
    append(");")
}

fun Appendable.selectField(field: FieldDescription) {
    when (field.type) {
        Types.UUID -> {
            fieldName(field.name + ".most")
            append(", ")
            fieldName(field.name + ".least")
        }
        else -> fieldName(field.name)
    }
}

private fun Appendable.fieldAsField(name: String) {
    fieldName(name)
    append(" as ")
    fieldName(name)
}

private fun Appendable.appendInterfaceColumn(description: EventDescription, name: String) {
    tableNameOfInterface(description)
    append('.')
    fieldAsField(name)
}

private fun Appendable.selectInterfaceField(description: EventDescription, field: FieldDescription) {
    when (field.type) {
        Types.UUID -> {
            appendInterfaceColumn(description, field.name + ".most")
            append(", ")
            appendInterfaceColumn(description, field.name + ".least")
        }
        else -> appendInterfaceColumn(description, field.name)
    }
}

private fun Appendable.addInterface(eventClass: EventClass) {
    val description = eventClass.description
    val fields = description.fields
    append("(select interface_X_datum.datum as `.id`")
    for (field in fields) {
        append(", ")
        selectInterfaceField(description, field)
    }
    append(" from ")
    tableNameOfInterface(description)
    append(" inner join interface on interface.id = ")
    tableNameOfInterface(description)
    append(
        ".`.id` " +
            "inner join interface_X_datum on interface_X_datum.interface = interface.id " +
            "where interface.class = "
    )
    append(eventClass.id.toString())
    append(") as ")
    fieldTableOf(eventClass.description)
}

fun Appendable.datumViewOf(description: EventDescription) {
    append('`')
    append(description.name)
    append("@datum`")
}

private fun Appendable.fieldTableOf(description: EventDescription) {
    append('`')
    append(description.name)
    append("@field`")
}

private fun Appendable.selectDatumFieldName(description: EventDescription, name: String) {
    fieldTableOf(description)
    append('.')
    fieldAsField(name)
}

private fun Appendable.selectDatumField(description: EventDescription, field: FieldDescription) {
    when (field.type) {
        Types.UUID -> {
            selectDatumFieldName(description, field.name + ".most")
            append(", ")
            selectDatumFieldName(description, field.name + ".least")
        }
        else -> selectDatumFieldName(description, field.name)
    }
}

fun Appendable.buildDatumView(classRepository: ClassRepository<*>, description: EventDescription) {
    append("create view if not exists ")
    datumViewOf(description)
    append(" as select datum.id as `.id`")
    val interfaces = mutableListOf(description)
    description.interfaces.mapTo(interfaces) { it.second.description }
    for (eventInterface in interfaces) {
        for (field in eventInterface.fields) {
            append(", ")
            selectDatumField(eventInterface, field)
        }
    }
    append(" from datum")
    for (eventInterface in interfaces) {
        append(" inner join ")
        addInterface(classRepository.classByName[eventInterface.name]!!)
        append(" on datum.id = ")
        fieldTableOf(eventInterface)
        append(".`.id`")
    }
    append(';')
}

fun Appendable.buildGetDatumById(eventClass: EventClass, ignoreFreeze: Boolean = false) {
    buildGetDatumById(eventClass.description, ignoreFreeze)
}

fun Appendable.buildGetDatumById(description: EventDescription, ignoreFreeze: Boolean = false) {
    append("select `.id`")
    val fields = if (ignoreFreeze) description.allRealFields else description.allFields
    for (field in fields) {
        append(", ")
        selectField(field)
    }
    append(" from ")
    datumViewOf(description)
    append(" where `.id` = ?;")
}

private fun Appendable.withFieldName(name: String) {
    fieldName(name)
    append(" = ?")
}

private fun Appendable.withField(field: FieldDescription) {
    when (field.type) {
        Types.UUID -> {
            withFieldName(field.name + ".most")
            append(" and ")
            withFieldName(field.name + ".least")
        }
        else -> withFieldName(field.name)
    }
}

private fun Appendable.withFields(fields: List<FieldDescription>) {
    if (fields.isNotEmpty()) {
        append(" where ")
        val iter = fields.iterator()
        val first = iter.next()
        withField(first)
        for (field in iter) {
            append(" and ")
            withField(field)
        }
    }
}

fun Appendable.buildGetDatumId(eventClass: EventClass, ignoreFreeze: Boolean = false) {
    val description = eventClass.description
    append("select `.id` from ")
    datumViewOf(description)
    if (ignoreFreeze) {
        append(" inner join state_X_particle as sXp on sXp.particle = `.id` where state = ?")
        withDatumFields(description, eventClass.allRealFields)
    } else {
        withFields(eventClass.allFields)
    }
    append(';')
}

private fun Appendable.withDatumFieldName(description: EventDescription, name: String) {
    datumViewOf(description)
    append('.')
    withFieldName(name)
}

private fun Appendable.withDatumField(description: EventDescription, field: FieldDescription) {
    when (field.type) {
        Types.UUID -> {
            withDatumFieldName(description, field.name + ".most")
            append(" and ")
            withDatumFieldName(description, field.name + ".least")
        }
        else -> withDatumFieldName(description, field.name)
    }
}

private fun Appendable.withDatumFields(description: EventDescription, fields: List<FieldDescription>) {
    for (field in fields) {
        append(" and ")
        withDatumField(description, field)
    }
}

private const val variability = "variability (state) as (select ?)"

private const val space =
    "space (particle) as (" +
        "select sXp.particle from state_X_particle as sXp " +
        "inner join variability where sXp.state = variability.state" +
        ")"

fun Appendable.buildCheckInterfaceExists(eventClass: EventClass) {
    append("with $variability, $space select exists(select 1 from ")
    val description = eventClass.description
    datumViewOf(description)
    append(
        " inner join space on `.id` = space.particle " +
            "inner join interface_X_datum as iXd on `.id` = iXd.datum " +
            "inner join interface on iXd.interface = interface.id " +
            "where interface.class = "
    )
    append(eventClass.id.toString())
    append(" and iXd.role = 1")
    for (field in eventClass.allFields) {
        append(" and ")
        withDatumField(description, field)
    }
    append(");")
}

fun Appendable.buildGetInterfaceId(eventClass: EventClass) {
    val description = eventClass.description
    append("select `.id` from ")
    tableNameOfInterface(description)
    withFields(description.fields)
    append(';')
}

fun Appendable.buildInsertInterface(eventClass: EventClass) {
    val description = eventClass.description
    val fields = description.fields
    append("insert or ignore into ")
    tableNameOfInterface(description)
    append('(')
    fieldName(".id")
    for (field in fields) {
        append(',')
        selectField(field)
    }
    append(") values (?")
    for (field in fields) {
        append(',')
        when (field.type) {
            Types.UUID -> append("?,?")
            else -> append("?")
        }
    }
    append(");")
}

private fun Appendable.expression2string(
    parameters: MutableList<Pair<Types, Any?>>?,
    expression: Filter.Expression
) {
    when (expression) {
        is Filter.Expression.Equal -> {
            append(" = ?")
            parameters?.add(expression.member.field.type to expression.value)
        }
        is Filter.Expression.IsNotNull -> append(" is not null")
        is Filter.Expression.IsNull -> append(" is null")
        is Filter.Expression.NotEqual -> {
            append(" != ?")
            parameters?.add(expression.member.field.type to expression.value)
        }
        is Filter.Expression.Bigger -> {
            append(" > ?")
            parameters?.add(expression.member.field.type to expression.value)
        }
        is Filter.Expression.Lesser -> {
            append(" < ?")
            parameters?.add(expression.member.field.type to expression.value)
        }
        is Filter.Expression.Like -> {
            append(" glob ?")
            parameters?.add(expression.member.field.type to expression.wildcard)
        }
        is Filter.Expression.Match -> {
            append(" regexp ?")
            parameters?.add(expression.member.field.type to expression.regex)
        }
    }
}

private fun Appendable.expression2string(
    parameters: MutableList<Pair<Types, Any?>>?,
    expression: TimeQuery.Expression
) {
    when (expression) {
        is TimeQuery.Expression.Equals -> {
            append(" = ?")
            parameters?.add(expression.property.type to expression.value)
        }
        is TimeQuery.Expression.IsNotNull -> append(" is not null")
        is TimeQuery.Expression.IsNull -> append(" is null")
        is TimeQuery.Expression.Lesser -> {
            append(" < ?")
            parameters?.add(expression.property.type to expression.value)
        }
        is TimeQuery.Expression.Bigger -> {
            append(" > ?")
            parameters?.add(expression.property.type to expression.value)
        }
        is TimeQuery.Expression.LikeError -> {
            append(" glob ?")
            parameters?.add(expression.property.type to expression.wildcard)
        }
        is TimeQuery.Expression.MatchError -> {
            append(" regexp ?")
            parameters?.add(expression.property.type to expression.regex)
        }
        is TimeQuery.Expression.NotEquals -> {
            append(" != ?")
            parameters?.add(expression.property.type to expression.value)
        }
    }
}

fun Appendable.appendFilter(parameters: MutableList<Pair<Types, Any?>>, filter: Filter) {
    when (filter) {
        is Filter.Binary -> {
            val (a, b) = filter
            appendFilter(parameters, a)
            when (filter) {
                is Filter.Binary.Exception -> append(" except ")
                is Filter.Binary.Intersection -> append(" intersect ")
                is Filter.Binary.Union -> append(" union ")
            }
            appendFilter(parameters, b)
        }
        is Filter.Expression -> {
            val member = filter.member
            append("select `.id` from ")
            datumViewOf(member.description)
            append(" natural join space where ")
            when (member.field.type) {
                Types.UUID -> {
                    fieldName(member.field.name + ".most")
                    expression2string(parameters, filter)
                    append(" and ")
                    fieldName(member.field.name + ".least")
                    expression2string(null, filter)
                }
                else -> {
                    fieldName(member.field.name)
                    expression2string(parameters, filter)
                }
            }
        }
        is Filter.Role -> {
            val (description, role) = filter
            append(
                "select `.id` from interface_X_datum " +
                    "inner join interface on interface_X_datum.interface = interface.id " +
                    "inner join "
            )
            datumViewOf(description)
            append(" on `.id` = interface_X_datum.datum natural join space where interface_X_datum.role = ")
            val roleNumber = if (role == null) 0 else role.ordinal + 1
            append(roleNumber.toString())
        }
        is Filter.Attached -> {
            append("select datum as `.id` from interface_X_datum inner join ")
            tableNameOfInterface(filter.description)
            append(
                " as interface on interface_X_datum.interface = interface.`.id` " +
                    "where datum in space"
            )
        }
        is Filter.Unary.Negation -> {
            append(
                "select datum.id as `.id` from datum " +
                    "inner join space on space.`.id` = datum.id " +
                    "where datum.id not in ("
            )
            appendFilter(parameters, filter.filter)
            append(')')
        }
        is Filter.Via -> {
            val (description, inner) = filter
            append("select iXd1.datum as `.id` from ")
            tableNameOfInterface(description)
            append(" inner join interface_X_datum as iXd1 on iXd1.interface = ")
            tableNameOfInterface(description)
            append(".`.id` inner join interface_X_datum as iXd2 on iXd2.interface = ")
            tableNameOfInterface(description)
            append(".`.id` inner join (")
            appendFilter(parameters, inner)
            append(
                ") as filter on filter.`.id` = iXd2.datum " +
                    "inner join space on space.`.id` = iXd1.datum"
            )
        }
        Filter.Universe -> append("select `.id` from space")
        Filter.Empty -> append("select 0 as `.id` where false")
    }
}

fun Appendable.appendTimeQuery(parameters: MutableList<Pair<Types, Any?>>, timeQuery: TimeQuery) {
    when (timeQuery) {
        is TimeQuery.Binary -> {
            val (a, b) = timeQuery
            appendTimeQuery(parameters, a)
            when (timeQuery) {
                is TimeQuery.Binary.Exception -> append(" except ")
                is TimeQuery.Binary.Intersection -> append(" intersect ")
                is TimeQuery.Binary.Union -> append(" union ")
            }
            appendTimeQuery(parameters, b)
        }
        is TimeQuery.Cause -> {
            val (direction, inner) = timeQuery
            val next: String
            val previous: String
            when (direction) {
                TimeQuery.Cause.Causes.Before -> {
                    next = "source"
                    previous = "target"
                }
                TimeQuery.Cause.Causes.After -> {
                    next = "target"
                    previous = "source"
                }
            }
            append("select id from (with recursive history ( next ) as (select eXe.")
            append(next)
            append(" from event_X_event as eXe inner join (")
            appendTimeQuery(parameters, inner)
            append(") as question on question.id = eXe.")
            append(previous)
            append(" union select eXe.")
            append(next)
            append(" from event_X_event as eXe inner join history on history.next = eXe.")
            append(previous)
            append(") select next as id from history) as cause")
        }
        is TimeQuery.Expression -> {
            if (timeQuery.property == TimeQuery.Expression.TimeProperties.Direction) {
                append("select event.id from event left outer join direct_event on event.id = direct_event.id where ")
            } else {
                append("select id from event where ")
            }
            val propertyName = when (timeQuery.property) {
                TimeQuery.Expression.TimeProperties.Timestamp -> "timestamp"
                TimeQuery.Expression.TimeProperties.Error -> "error"
                TimeQuery.Expression.TimeProperties.Direction -> "direction"
            }
            append(propertyName)
            expression2string(parameters, timeQuery)
        }
        is TimeQuery.From -> {
            append("select event.id from event inner join (")
            appendFilter(parameters, timeQuery.filter)
            append(") as filter where filter.`.id` = event.datum")
        }
        is TimeQuery.Order -> {
            append("select distinct id from (with recursive sample ( id ) as (")
            appendTimeQuery(parameters, timeQuery.timeQuery)
            val next: String
            val previous: String
            when (timeQuery.order) {
                TimeQuery.Order.Orders.First -> {
                    next = "source"
                    previous = "target"
                }
                TimeQuery.Order.Orders.Last -> {
                    next = "target"
                    previous = "source"
                }
            }
            append("), history ( id, start, level ) as (select id, id, 0 from sample union select eXe.")
            append(next)
            append(
                ", history.start, history.level + 1 from event_X_event as eXe " +
                    "inner join history on history.id = eXe."
            )
            append(previous)
            if (timeQuery.order == TimeQuery.Order.Orders.Last) {
                append(" inner join state_X_event as sXe on sXe.event = eXe.target where sXe.state in variability")
            }
            append(
                ") select history.id, history.start, max(level) from history " +
                    "inner join sample on sample.id = history.id " +
                    "group by history.start)"
            )
        }
        is TimeQuery.Unary.Negation -> {
            append("select id from event where id not in (")
            appendTimeQuery(parameters, timeQuery.timeQuery)
            append(')')
        }
        TimeQuery.Empty -> append("select 0 as id where false")
        TimeQuery.Universe -> append("select id from event")
    }
}

fun Appendable.appendVariabilityQuery(parameters: MutableList<Pair<Types, Any?>>, variabilityQuery: VariabilityQuery) {
    when (variabilityQuery) {
        is VariabilityQuery.Reactive -> {
            append("select id from state where reactive = ")
            val value = if (variabilityQuery.reactive) "true" else "false"
            append(value)
        }
        is VariabilityQuery.Binary -> {
            appendVariabilityQuery(parameters, variabilityQuery.a)
            when (variabilityQuery) {
                is VariabilityQuery.Binary.Exception -> append(" except ")
                is VariabilityQuery.Binary.Intersection -> append(" intersect ")
                is VariabilityQuery.Binary.Union -> append(" union ")
            }
            appendVariabilityQuery(parameters, variabilityQuery.b)
        }
        is VariabilityQuery.Contains -> {
            appendLinearize(
                """
                select distinct id from(
                    with space(`.id`) as (
                        select particle from state_X_particle
                    )
                    select state as id from state_X_particle
                    where particle in (
                """
            )
            appendFilter(parameters, variabilityQuery.filter)
            append("))")
        }
        is VariabilityQuery.Happened -> {
            appendLinearize(
                """
                select distinct id from (
                    with variability(state) as (
                        select id from state
                    ), space(`.id`) as (
                        select datum from event
                    )
                    select distinct state as id from state_X_event where event in (
                """
            )
            appendTimeQuery(parameters, variabilityQuery.timeQuery)
            append("))")
        }
        is VariabilityQuery.Named -> {
            append("select id from state where name ")
            parameters += Types.String to variabilityQuery.name
            when (variabilityQuery) {
                is VariabilityQuery.Named.Equals -> append('=')
                is VariabilityQuery.Named.Like -> append("glob")
                is VariabilityQuery.Named.Match -> append("regexp")
            }
            append(" ?")
        }
        is VariabilityQuery.Unary.Negation -> {
            append("select id from state where id not in (")
            appendVariabilityQuery(parameters, variabilityQuery.variabilityQuery)
            append(")")
        }
        VariabilityQuery.Empty -> append("select 0 as id where false")
        VariabilityQuery.Default -> append(
            "select id from state where name = (select value from mcsdb_properties where name = 'default_state')"
        )
        VariabilityQuery.Universe -> append("select id from state")
    }
}

fun Appendable.appendLinearize(string: String) {
    for (line in string.lineSequence()) {
        val trimmed = line.trim()
        if (trimmed.isNotEmpty()) {
            append(trimmed)
            append(' ')
        }
    }
}

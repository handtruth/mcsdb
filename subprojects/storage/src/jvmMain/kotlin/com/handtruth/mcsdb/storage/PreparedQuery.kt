package com.handtruth.mcsdb.storage

import com.handtruth.mc.nbt.NBT
import com.handtruth.mc.nbt.NBTBinaryConfig
import com.handtruth.mc.nbt.NBTStringConfig
import com.handtruth.mc.nbt.TagsModule
import com.handtruth.mc.types.UUID
import com.handtruth.mcsdb.schema.Types
import com.handtruth.mcsdb.types.Serial
import io.ktor.utils.io.core.*
import kotlinx.datetime.Instant

private val dataNBT = NBT(
    tags = TagsModule.MCSDB,
    binary = NBTBinaryConfig.Default.copy(
        endian = NBTBinaryConfig.ByteOrders.Little,
        format = NBTBinaryConfig.Formats.Flat,
        compressBooleans = false
    ),
    string = NBTStringConfig.Handtruth
)

abstract class QueryResult : Closeable {
    protected var index = 1

    protected val nbt get() = dataNBT

    abstract fun getSerial(): Serial?
    abstract fun getBoolean(): Boolean?
    abstract fun getByte(): Byte?
    abstract fun getShort(): Short?
    abstract fun getInt(): Int?
    abstract fun getLong(): Long?
    abstract fun getUByte(): UByte?
    abstract fun getUShort(): UShort?
    abstract fun getUInt(): UInt?
    abstract fun getULong(): ULong?
    abstract fun getFloat(): Float?
    abstract fun getDouble(): Double?
    abstract fun getChar(): Char?
    abstract fun getString(): String?
    abstract fun getUUID(): UUID?
    abstract fun getInstant(): Instant?
    abstract fun getStructure(): Any?
    abstract fun getArbitrary(): ByteArray?
    abstract operator fun get(type: Types): Any?

    open fun next(): Boolean {
        index = 1
        return false
    }
}

abstract class PreparedQuery : Closeable {
    protected var index = 1

    fun reset() {
        index = 1
    }

    protected val nbt get() = dataNBT

    abstract fun setSerial(value: Serial?)
    abstract fun setBoolean(value: Boolean?)
    abstract fun setByte(value: Byte?)
    abstract fun setShort(value: Short?)
    abstract fun setInt(value: Int?)
    abstract fun setLong(value: Long?)
    abstract fun setUByte(value: UByte?)
    abstract fun setUShort(value: UShort?)
    abstract fun setUInt(value: UInt?)
    abstract fun setULong(value: ULong?)
    abstract fun setFloat(value: Float?)
    abstract fun setDouble(value: Double?)
    abstract fun setChar(value: Char?)
    abstract fun setString(value: String?)
    abstract fun setUUID(value: UUID?)
    abstract fun setInstant(value: Instant?)
    abstract fun setStructure(value: Any?)
    abstract fun setArbitrary(value: ByteArray?)

    abstract operator fun set(type: Types, value: Any?)

    @Transaction
    abstract fun executeQuery(): QueryResult

    @Transaction
    inline fun executeQuery(block: PreparedQuery.() -> Unit): QueryResult {
        reset()
        block()
        return executeQuery()
    }

    @Transaction
    abstract fun executeUpdate(): Int

    @Transaction
    inline fun executeUpdate(block: PreparedQuery.() -> Unit): Int {
        reset()
        block()
        return executeUpdate()
    }
}

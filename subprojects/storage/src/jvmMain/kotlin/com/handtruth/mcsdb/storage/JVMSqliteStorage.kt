package com.handtruth.mcsdb.storage

import com.handtruth.kommon.Log
import com.handtruth.kommon.default
import com.handtruth.mc.graph.Graph
import com.handtruth.mc.graph.MutableGraph
import com.handtruth.mc.nbt.*
import com.handtruth.mc.types.*
import com.handtruth.mcsdb.Filter
import com.handtruth.mcsdb.schema.*
import com.handtruth.mcsdb.schema.Types
import com.handtruth.mcsdb.space.Matter
import com.handtruth.mcsdb.space.Particle
import com.handtruth.mcsdb.time.*
import com.handtruth.mcsdb.types.Serial
import com.handtruth.mcsdb.variability.*
import io.ktor.utils.io.core.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import org.intellij.lang.annotations.Language
import org.sqlite.Function
import java.sql.*
import kotlin.io.use
import java.sql.Types as SQLTypes
import java.util.UUID as JavaUUID

private class RegexpFunction : Function() {
    override fun xFunc() {
        val expression = value_text(0)
        val value = value_text(1)
        if (value == null || expression == null) {
            result(0)
        } else {
            val regex = Regex(expression)
            val result = if (regex matches value) 1 else 0
            result(result)
        }
    }
}

actual suspend fun sqliteStorage(source: String): Storage {
    val connection = DriverManager.getConnection("jdbc:sqlite:$source")
    Function.create(connection, "REGEXP", RegexpFunction())
    val script = JVMSqliteStorage.getInitScript()
    withContext(Dispatchers.IO) {
        connection.transactionIsolation = Connection.TRANSACTION_SERIALIZABLE
        for (query in script.splitToSequence("\n\n")) {
            if (query.isBlank()) continue
            connection.createStatement().use {
                it.executeUpdate(query.trim())
            }
        }
        connection.autoCommit = false
    }
    val storage = JVMSqliteStorage(connection)
    storage.prepareRepository()
    return storage
}

class JVMSqliteStorage(private val connection: Connection) : Storage {
    companion object {
        fun getInitScript(): String =
            JVMSqliteStorage::class.java.getResourceAsStream("init_script.sql")!!.reader().readText()

        const val spaceFilter = "with space(`.id`) as (select particle as `.id` from state_X_particle where state = ?)"
    }

    private val log = Log.default("mcsdb/storage")

    private val mutex = Mutex()

    private suspend inline fun <R> transaction(
        @OptIn(Transaction::class) crossinline block: suspend Statement.() -> R
    ): R = mutex.withLock {
        withContext(Dispatchers.IO) {
            val result = runCatching { connection.createStatement().use { it.block() } }
            if (result.isSuccess) {
                connection.commit()
                result.getOrThrow()
            } else {
                connection.rollback()
                throw result.exceptionOrNull()!!
            }
        }
    }

    private val allStatements = mutableListOf<PreparedStatement>()

    private fun prepareWithKeys(@Language("SQL") sql: String): PreparedStatement {
        val statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        allStatements += statement
        return statement
    }

    private fun prepare(@Language("SQL") sql: String): PreparedStatement {
        val statement = connection.prepareStatement(sql)
        allStatements += statement
        return statement
    }

    override val classRepository = ClassRepositoryImplementation<JVMSQLiteEventClass>()

    private data class LoadIntermediate(val id: Long, var description: Any, val metadata: EventDescriptionMetadata) {
        val asInput get() = description as Input
        val asEventDescription get() = description as EventDescription
        val isLoaded get() = description is EventDescription
    }

    suspend fun prepareRepository() {
        val map1 = transaction {
            executeQuery("select id, name, description, metadata from class").use { result ->
                buildMap<String, LoadIntermediate> {
                    while (result.next()) {
                        val id = result.getLong(1)
                        val name = result.getString(2)
                        val description = ByteReadPacket(result.getBytes(3))
                        val metadataBlob = result.getBytes(4)
                        val metadata = ByteReadPacket(metadataBlob).use {
                            nbt.decodeFromInput(eventDescriptionMetadataSerializer, it)
                        }
                        put(name, LoadIntermediate(id, description, metadata))
                    }
                }
            }
        }
        lateinit var mapper: EventDescriptionMapper
        fun getDescription(loaded: LoadIntermediate): EventDescription {
            return if (loaded.isLoaded) {
                loaded.asEventDescription
            } else {
                val description = loaded.asInput.use { input ->
                    input.readEventDescription(mapper)
                }
                loaded.description = description
                description
            }
        }
        mapper = EventDescriptionMapper { name ->
            val loaded = map1[name] ?: error("event description \"$name\" not found")
            getDescription(loaded)
        }
        for ((_, value) in map1.entries) {
            val eventClass = JVMSQLiteEventClass(value.id, getDescription(value), value.metadata)
            classRepository.add(eventClass)
            eventClass.prepareQueries(connection)
        }
    }

    private val addEventDescription = prepareWithKeys(
        "insert into class (name, description, metadata) values (?, ?, ?)"
    )

    private val nbt = NBT(
        tags = TagsModule.MCSDB,
        binary = NBTBinaryConfig.KBT,
        string = NBTStringConfig.Handtruth
    )

    private fun encodeMetadata(metadata: EventDescriptionMetadata): ByteArray {
        return buildByteArray {
            nbt.encodeToOutput(eventDescriptionMetadataSerializer, this, metadata)
        }
    }

    private val getDefaultState = prepare(
        "select id from state where name = (select value from mcsdb_properties where name = 'default_state');"
    )

    override suspend fun getDefaultState(): Long {
        return transaction {
            getDefaultState.executeQuery().use {
                check(it.next())
                it.getLong(1)
            }
        }
    }

    private val registrationMutex = Mutex()

    override suspend fun registerEventDescription(eventDescription: EventDescription): EventClass {
        registrationMutex.withLock {
            return privateRegisterEventDescription(eventDescription)
        }
    }

    private suspend fun privateRegisterEventDescription(eventDescription: EventDescription): JVMSQLiteEventClass {
        run {
            // Check if description name is taken
            val other = classRepository.classByName[eventDescription.name]
            if (other != null) {
                if (other.description == eventDescription) {
                    other.description = eventDescription // TODO: kostil :)
                    return other
                } else {
                    throw SchemaException(SchemaException.Codes.DescriptionIsPresent, eventDescription.name)
                }
            }
        }

        // not registered, ensure, that interfaces are registered
        for ((_, eventInterface) in eventDescription.interfaces) {
            privateRegisterEventDescription(eventInterface.description)
        }

        val lastFieldValues = mutableMapOf<String, Long>()
        for (field in eventDescription.fields) {
            if (Modifier.AutoIncrement in field.modifiers || field.type == Types.Serial) {
                lastFieldValues[field.name] = 1L
            }
        }
        val metadata = EventDescriptionMetadata(lastFieldValues)

        return transaction {
            val descriptionId = with(addEventDescription) {
                setString(1, eventDescription.name)
                val descriptionBlob = buildByteArray {
                    writeEventDescription(eventDescription)
                }
                setBytes(2, descriptionBlob)

                val metadataBlob = encodeMetadata(metadata)
                setBytes(3, metadataBlob)
                val result = executeUpdate()
                check(result == 1) { "new event description insertion failed" }
                generatedKeys.getLong(1)
            }

            execute(buildString { declareTableOfInterface(eventDescription) })
            if (eventDescription.fields.isEmpty()) {
                execute(buildString { uniqueInterfaceTrigger(eventDescription) })
            }
            for (field in eventDescription.fields) {
                if (Modifier.Indexed in field.modifiers) {
                    execute(buildString { buildIndex(eventDescription, field) })
                }
            }

            val eventClass = JVMSQLiteEventClass(descriptionId, eventDescription, metadata)
            classRepository.add(eventClass)
            execute(buildString { buildDatumView(classRepository, eventDescription) })
            eventClass.prepareQueries(connection)
            eventClass
        }
    }

    private fun castLong(value: Long, type: Types): Any = when (type) {
        Types.Serial -> Serial(value)
        Types.Byte -> value.toByte()
        Types.Short -> value.toShort()
        Types.Int -> value.toInt()
        Types.Long -> value
        Types.UByte -> value.toUByte()
        Types.UShort -> value.toUShort()
        Types.UInt -> value.toUInt()
        Types.ULong -> value.toULong()
        Types.Char -> value.toChar()
        else -> error("should not be there")
    }

    private val updateDescriptionMetadata = prepare(
        "update class set metadata = ? where id = ?;"
    )

    @Transaction
    private fun updateClassMetadata(eventClass: EventClass) {
        with(updateDescriptionMetadata) {
            setBytes(1, encodeMetadata(eventClass.metadata))
            setLong(2, eventClass.id)
            executeUpdate()
        }
    }

    @Transaction
    private fun prepareNext(source: Dynamic, target: MutableDynamic, eventClass: EventClass) {
        var metadataUpdated = false
        val metadata = eventClass.metadata
        for (field in eventClass.description.fields) {
            val value = source.getOrNull(field.name)
            val type = field.type
            target[field.name] = if (value == null) {
                val last = metadata.lastFieldValues[field.name]
                if (last == null) {
                    when (type) {
                        Types.UUID -> {
                            val uuid = JavaUUID.randomUUID()
                            UUID(uuid.mostSignificantBits, uuid.leastSignificantBits)
                        }
                        Types.Instant -> Clock.System.now()
                        else -> {
                            if (Modifier.Nullable in field.modifiers) {
                                null
                            } else {
                                throw TimeException(
                                    TimeException.Codes.IncompletePrototype,
                                    "field ${field.name}"
                                )
                            }
                        }
                    }
                } else {
                    val resultValue = castLong(last, type)
                    metadata.lastFieldValues[field.name] = last + 1
                    metadataUpdated = true
                    resultValue
                }
            } else {
                val kotlinClass = type.kotlinClass
                if (kotlinClass != null && kotlinClass != value::class) {
                    throw TimeException(TimeException.Codes.WrongFieldType, "field ${field.name}")
                }
                value
            }
        }
        if (metadataUpdated) {
            updateClassMetadata(eventClass)
        }
        for (interfaceDescription in eventClass.description.inherits) {
            prepareNext(source, target, classRepository.classByName[interfaceDescription.description.name]!!)
        }
    }

    @Transaction
    private fun prepareEvent(event: Event): Event {
        val top = event.`$description$`()
        val data = buildDynamic {
            val base = event.`$data$`()
            prepareNext(base, this, classRepository.classByName[top.name]!!)
            if (top.kind == Kind.Direct) {
                "direction" assign base.getOrNull("direction")
            }
        }
        return top.prototype(data)
    }

    private val getDatumHeader = prepare(
        "select class from datum where id = ? limit 1;"
    )

    @Transaction
    private fun getRealDatumById(id: Long): Particle? {
        getDatumHeader.setLong(1, id)
        val eventClass: JVMSQLiteEventClass
        getDatumHeader.executeQuery().use { result ->
            if (!result.next()) {
                return null
            }
            eventClass = classRepository.classById[result.getLong(1)]!!
        }
        return Particle(eventClass.description, eventClass.queries.getRealDatumById(id)!!)
    }

    @Transaction
    internal fun getDatumById(id: Long): Particle? {
        getDatumHeader.setLong(1, id)
        val eventClass: JVMSQLiteEventClass
        getDatumHeader.executeQuery().use { result ->
            if (!result.next()) {
                return null
            }
            eventClass = classRepository.classById[result.getLong(1)]!!
        }
        return Particle(eventClass.description, eventClass.queries.getDatumById(id)!!)
    }

    @Transaction
    private fun existsInterface(data: Dynamic, description: EventDescription, state: Long): Boolean {
        val eventClass = classRepository.classByName[description.name]!!
        return eventClass.queries.checkInterfaceExists(data, state)
    }

    @Transaction
    private fun checkInterfaces(
        data: Dynamic,
        description: EventDescription,
        state: Long,
        vararg roles: Roles
    ) {
        for (role in roles) {
            for (eventInterface in description.interfaces[role]) {
                if (!existsInterface(data, eventInterface.description, state)) {
                    throw TimeException(
                        TimeException.Codes.InterfaceNotExists,
                        eventInterface.description.name
                    )
                }
            }
        }
    }

    @Transaction
    fun checkForeignKey(
        data: Dynamic,
        description: EventDescription,
        state: Long
    ) {
        for (eventInterface in description.interfaces[Roles.Source]) {
            val interfaceDescription = eventInterface.description
            if (Modifier.Unique in interfaceDescription.modifiers) {
                if (existsInterface(data, interfaceDescription, state)) {
                    throw TimeException(TimeException.Codes.ExplicitForeignKey, interfaceDescription.name)
                }
            }
        }
    }

    private fun fakeParticle(event: Event): Particle {
        return Particle(
            event.`$description$`(),
            buildDynamic { fields += event.`$data$`().fields; this["direction"] = null }
        )
    }

    private val allSources = prepare(
        """
        with recursive variability (state) as (
            select ?
        ), space (particle) as (
            select particle from state_X_particle
            where state in variability
        ), subgraph (source) as (
            select datum from interface_X_datum
            where datum in space and role = 1 and interface = ?
        union
            select pXp.source from particle_X_particle as pXp
            inner join subgraph on subgraph.source = pXp.target
            where pXp.state in variability
        ) select source from subgraph;
        """
    )

    private val allTargets = prepare(
        """
        with recursive variability (state) as (
            select ?
        ), space (particle) as (
            select particle from state_X_particle
            where state in variability
        ), subgraph (target) as (
            select datum from interface_X_datum
            where datum in space and role = 3 and interface = ?
        union
            select pXp.target from particle_X_particle as pXp
            inner join subgraph on subgraph.target = pXp.source
            where pXp.state in variability
        ) select target from subgraph;
        """
    )

    @Transaction
    private fun getSubgraph(particles: MutableSet<Long>, prepared: PreparedStatement, interfaceId: Long, state: Long) {
        prepared.setLong(1, state)
        prepared.setLong(2, interfaceId)
        prepared.executeQuery().use { result ->
            while (result.next()) {
                particles += result.getLong(1)
            }
        }
    }

    @Transaction
    private fun getSourcesAndTargets(
        sources: MutableSet<Long>,
        targets: MutableSet<Long>,
        data: Dynamic,
        interfaceClass: JVMSQLiteEventClass,
        state: Long
    ) {
        val id = interfaceClass.queries.getInterfaceId(data) ?: return
        getSubgraph(sources, allSources, id, state)
        getSubgraph(targets, allTargets, id, state)
    }

    @Transaction
    private fun checkCycles(data: Dynamic, interfaceClass: JVMSQLiteEventClass, state: Long) {
        val sources = hashSetOf<Long>()
        val targets = hashSetOf<Long>()
        for ((_, eventInterface) in interfaceClass.description.interfaces) {
            getSourcesAndTargets(sources, targets, data, classRepository[eventInterface.description.name], state)
        }
        val intersection = sources intersect targets
        if (intersection.isNotEmpty()) {
            val particles = intersection.map { getRealDatumById(it) }
            throw TimeException(TimeException.Codes.CyclicReference, particles.toString())
        }
    }

    private val hasDependents = prepare(
        """
        with recursive variability(state) as (
            select ?
        ), last_source ( interface ) as (
            select interface from interface_X_datum as iXd
            inner join state_X_particle as sXp on particle = datum
            where role = 1 and state in variability
            group by interface
            having count(datum) = 1
        ), subgraph (datum) as (
            select target from particle_X_particle as pXp
            inner join last_source on last_source.interface = pXp.interface
            where pXp.source = ? and pXp.state in variability
        union
            select target from particle_X_particle as pXp
            inner join subgraph on subgraph.datum = pXp.source
            inner join last_source on last_source.interface = pXp.interface
            where state in variability
        ) select exists(select 1 from subgraph); 
        """
    )

    @Transaction
    private fun hasDependents(id: Long, state: Long): Boolean {
        hasDependents.setLong(1, state)
        hasDependents.setLong(2, id)
        hasDependents.executeQuery().use { result ->
            val next = result.next()
            assert(next)
            return result.getBoolean(1)
        }
    }

    private val getNextDependants = prepare(
        """
        with recursive variability(state) as (
            select ?
        ), last_source ( interface ) as (
            select interface from interface_X_datum as iXd
            inner join state_X_particle as sXp on particle = datum
            where role = 1 and state in variability
            group by interface
            having count(datum) = 1
        )
        select distinct target, class from particle_X_particle as pXp
        inner join last_source on last_source.interface = pXp.interface
        inner join datum on id = target
        where state in variability and pXp.source = ?;
        """
    )

    @Transaction
    private fun getNextDependents(
        id: Long,
        state: Long,
        mainData: Dynamic,
        mainClass: JVMSQLiteEventClass
    ): List<Event> {
        getNextDependants.setLong(1, state)
        getNextDependants.setLong(2, id)
        val list = mutableListOf<Pair<Long, JVMSQLiteEventClass>>()
        getNextDependants.executeQuery().use { result ->
            while (result.next()) {
                val particle = result.getLong(1)
                val classId = result.getLong(2)
                val eventClass = classRepository.classById[classId]!!
                list += (particle to eventClass)
            }
        }
        return list.map { (particle, eventClass) ->
            val data = eventClass.queries.getRealDatumById(particle)
            data as MutableDynamic
            for ((freezeInterface, freezeFields) in eventClass.freezeFields) {
                if (!mainClass.description.doesInherit(freezeInterface)) {
                    throw TimeException(TimeException.Codes.FantomUndefined, freezeInterface.name)
                }
                for (field in freezeFields) {
                    data[field.name] = mainData.getOrNull(field.name)
                }
            }
            data["direction"] = false
            eventClass.description.prototype(data)
        }
    }

    override suspend fun resolveNextPrototypes(prototype: Event, state: Long): Pair<Event, List<Event>> {
        @OptIn(Transaction::class)
        return transaction {
            val prepared = prepareEvent(prototype)
            val description = descriptionOf(prepared)
            val eventClass = classRepository.classByName[description.name]!!
            val data = dataOf(prepared)
            when (description.kind) {
                Kind.Interface ->
                    throw TimeException(TimeException.Codes.WrongEventKind, Kind.Interface.name)
                Kind.Fact -> {
                    checkInterfaces(data, description, state, *Roles.values())
                    prepared to emptyList()
                }
                Kind.Direct -> {
                    if (data["direction"] as Boolean) {
                        val existingId = eventClass.queries.getParticleId(data, state)
                        if (existingId != 0L) {
                            throw TimeException(
                                TimeException.Codes.ParticleExists,
                                fakeParticle(prepared).toString()
                            )
                        }
                        checkInterfaces(data, description, state, Roles.Target, Roles.Fantom)
                        checkCycles(data, eventClass, state)
                        checkForeignKey(data, description, state)
                        prepared to emptyList()
                    } else {
                        val subject = eventClass.queries.getParticleId(data, state)
                        if (subject == 0L) {
                            throw TimeException(
                                TimeException.Codes.ParticleNotExists,
                                fakeParticle(prepared).toString()
                            )
                        }
                        checkInterfaces(data, description, state, Roles.Fantom)
                        prepared to getNextDependents(subject, state, data, eventClass)
                    }
                }
            }
        }
    }

    private val connectEvent = prepare(
        "insert into event_X_event (source, target) values (?, ?);"
    )

    @Transaction
    private fun connectCasualRelationships(event: Long, dependencies: Collection<Long>) {
        for (dependency in dependencies) {
            connectEvent.setLong(1, dependency)
            connectEvent.setLong(2, event)
            val updated = connectEvent.executeUpdate()
            assert(updated == 1)
        }
    }

    private val createInterface = prepareWithKeys(
        "insert into interface ( class ) values (?);"
    )

    @Transaction
    private fun getInterfaceId(eventClass: JVMSQLiteEventClass, data: Dynamic): Long {
        val id = eventClass.queries.getInterfaceId(data)
        return if (id == null) {
            createInterface.setLong(1, eventClass.id)
            val updated = createInterface.executeUpdate()
            assert(updated == 1)
            val newId = createInterface.generatedKeys.getLong(1)
            eventClass.queries.insertInterface(newId, data)
            newId
        } else {
            id
        }
    }

    private val assignInterface = prepare(
        "insert into interface_X_datum (interface, datum, role) VALUES (?, ?, ?);"
    )

    @Transaction
    private fun assignInterface(eventClass: JVMSQLiteEventClass, data: Dynamic, datum: Long, role: Roles?) {
        val roleId = (role?.let { it.ordinal + 1 } ?: 0).toByte()
        val interfaceId = getInterfaceId(eventClass, data)
        assignInterface.setLong(1, interfaceId)
        assignInterface.setLong(2, datum)
        assignInterface.setByte(3, roleId)
        val updated = assignInterface.executeUpdate()
        assert(updated == 1)
    }

    private val createDatum = prepareWithKeys(
        "insert into datum(class) values (?);"
    )

    @Transaction
    private fun getDatumId(eventClass: JVMSQLiteEventClass, data: Dynamic): Long {
        val id = eventClass.queries.getDatumId(data)
        if (id == null) {
            createDatum.setLong(1, eventClass.id)
            val updated = createDatum.executeUpdate()
            assert(updated == 1)
            val newId = createDatum.generatedKeys.getLong(1)
            val isFact = eventClass.description.kind == Kind.Fact
            val rootRole = if (isFact) Roles.Fantom else null
            assignInterface(eventClass, data, newId, rootRole)
            for ((role, eventInterface) in eventClass.description.interfaces) {
                val actualRole = if (isFact) Roles.Fantom else role
                assignInterface(classRepository[eventInterface.description.name], data, newId, actualRole)
            }
            return newId
        } else {
            return id
        }
    }

    private val createEvent = prepareWithKeys(
        "insert into event (datum, timestamp, error) values (?, ?, ?);"
    )

    private val setEventState = prepare(
        "insert into state_X_event ( state, event ) VALUES (?, ?)"
    )

    private val getAncestors = prepare(
        "select event from current_ancestors where state = ? and datum = ?;"
    )

    @Transaction
    private fun createEvent(eventClass: JVMSQLiteEventClass, data: Dynamic, info: EventInfo, state: Long): Long {
        val datum = getDatumId(eventClass, data)
        val isDirect = eventClass.description.kind == Kind.Direct
        if (isDirect && info.succeeded) {
            if (data["direction"] as Boolean) {
                createMatter(datum, state)
            } else {
                val particle = eventClass.queries.getParticleId(data, state)
                collapseMatter(particle, state)
            }
        }
        getAncestors.setLong(1, state)
        getAncestors.setLong(2, datum)
        val ancestors = getAncestors.executeQuery().use { result ->
            buildList {
                while (result.next()) {
                    add(result.getLong(1))
                }
            }
        }
        val timestamp = info.timestamp!!
        createEvent.setLong(1, datum)
        createEvent.setString(2, timestamp.toString())
        val error = info.error
        if (error != null) {
            createEvent.setString(3, error)
        } else {
            createEvent.setNull(3, SQLTypes.VARCHAR)
        }
        val added = createEvent.executeUpdate()
        assert(added == 1)
        val id = createEvent.generatedKeys.getLong(1)
        if (isDirect) {
            createDirectEvent.setLong(1, id)
            createDirectEvent.setBoolean(2, data["direction"] as Boolean)
            val updated = createDirectEvent.executeUpdate()
            assert(updated == 1)
        }
        connectCasualRelationships(id, ancestors)
        setEventState.setLong(1, state)
        setEventState.setLong(2, id)
        val updated = setEventState.executeUpdate()
        assert(updated == 1)
        return id
    }

    private val setParticleState = prepare(
        "insert into state_X_particle ( state, particle ) values (?, ?);"
    )

    @Transaction
    private fun createMatter(particle: Long, state: Long) {
        setParticleState.setLong(1, state)
        setParticleState.setLong(2, particle)
        val updated2 = setParticleState.executeUpdate()
        assert(updated2 == 1)
    }

    private val detachParticleFromState = prepare(
        "delete from state_X_particle where state = ? and particle = ?;"
    )

    @Transaction
    private fun collapseMatter(particle: Long, state: Long) {
        detachParticleFromState.setLong(1, state)
        detachParticleFromState.setLong(2, particle)
        val updated = detachParticleFromState.executeUpdate()
        assert(updated == 1)
    }

    @Transaction
    private fun connectReactiveRelations(id: Long, reactive: Collection<Long>) {
        if (reactive.isNotEmpty()) {
            connection.createStatement().use { statement ->
                statement.executeUpdate(
                    """
                    insert or ignore into event_X_event(source, target, reactive)
                    select src, $id, true from (select null as src
                    union all values ${reactive.joinToString { "($it)" }})
                    where src is not null;
                    """
                )
            }
        }
    }

    private val createDirectEvent = prepare(
        "insert into direct_event (id, direction) values (?, ?);"
    )

    private val checkROState = prepare(
        "select name is null from state where id = ?;"
    )

    override suspend fun saveEvent(event: Event, state: Long, reactive: Collection<Long>): Long {
        val description = event.`$description$`()
        val data = event.`$data$`()
        val extras = event.`$info$`()
        val eventClass = classRepository.classByName[description.name]!!
        // require(!extras.isPrototype) { "a prototype, not event" }
        @OptIn(Transaction::class)
        return transaction {
            checkROState.setLong(1, state)
            checkROState.executeQuery().use {
                val next = it.next()
                assert(next)
                if (it.getBoolean(1)) {
                    throw VariabilityException(VariabilityException.Codes.ReadOnlyState, state.toString())
                }
            }
            when (description.kind) {
                Kind.Interface -> throw AssertionError("interface event can not be saved")
                Kind.Fact -> {
                    checkInterfaces(data, description, state, *Roles.values())
                }
                Kind.Direct -> {
                    if (data["direction"] as Boolean) {
                        if (eventClass.queries.getParticleId(data, state) != 0L) {
                            throw TimeException(
                                TimeException.Codes.Concurrency,
                                "particle already created"
                            )
                        }
                        checkInterfaces(data, description, state, Roles.Target, Roles.Fantom)
                        checkCycles(data, eventClass, state)
                        checkForeignKey(data, description, state)
                    } else {
                        val subject = eventClass.queries.getParticleId(data, state)
                        if (subject == 0L) {
                            throw TimeException(
                                TimeException.Codes.Concurrency,
                                "particle not exists anymore"
                            )
                        }
                        checkInterfaces(data, description, state, Roles.Fantom)
                        if (hasDependents(subject, state)) {
                            val subgraph = getNextDependents(subject, state, data, eventClass)
                            throw TimeException(
                                TimeException.Codes.DependantsExists,
                                "dependants: " + subgraph.joinToString()
                            )
                        }
                    }
                }
            }
            val id: Long = createEvent(eventClass, data, extras, state)
            connectReactiveRelations(id, reactive)
            id
        }
    }

    override suspend fun forget(timeQuery: TimeQuery, state: Long): Int {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val statement = buildString {
            append(spaceFilter)
            append(", history(id) as (")
            appendTimeQuery(parameters, timeQuery)
            append(
                " union select eXe.source from event_X_event as eXe " +
                    "inner join history on history.id = eXe.target) " +
                    "delete from event where id in (" +
                    "select id from history " +
                    "inner join state_X_event as sXe on id = sXe.event " +
                    "where sXe.state = ?);"
            )
        }
        log.verbose { "forget: $statement" }
        @OptIn(Transaction::class)
        return transaction {
            val count = JVMPreparedQuery(connection.prepareStatement(statement)).use { prepared ->
                prepared.executeUpdate {
                    setLong(state)
                    for ((type, value) in parameters) {
                        prepared[type] = value
                    }
                    setLong(state)
                }
            }
            log.verbose { "forgot $count events" }
            count
        }
    }

    private class ParticleHead(val id: Long, val eventClass: JVMSQLiteEventClass)

    override suspend fun querySpace(filter: Filter, state: Long): Set<Particle> {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val query = buildString {
            append(
                "$spaceFilter " +
                    "select `.id`, class from datum " +
                    "inner join ("
            )
            appendFilter(parameters, filter)
            append(") as filter on `.id` = datum.id;")
        }
        log.verbose { "space filter: $query" }
        val particles = mutableSetOf<Particle>()
        @OptIn(Transaction::class)
        transaction {
            val particleHeads = mutableListOf<ParticleHead>()
            JVMPreparedQuery(connection.prepareStatement(query)).use { prepared ->
                prepared.executeQuery {
                    setLong(state)
                    for ((type, value) in parameters) {
                        prepared[type] = value
                    }
                }.use { result ->
                    val resultSet = (result as JVMQueryResult).resultSet
                    while (resultSet.next()) {
                        val id = resultSet.getLong(1)
                        val classId = resultSet.getLong(2)
                        val eventClass = classRepository[classId]
                        particleHeads += ParticleHead(id, eventClass)
                    }
                }
            }
            particleHeads.mapTo(particles) { particle ->
                val eventClass = particle.eventClass
                val data = eventClass.queries.getRealDatumById(particle.id)!!
                Particle(eventClass.description, data)
            }
        }
        return particles
    }

    private class EventHead(
        val timestamp: Instant?,
        val error: String?,
        val direction: Boolean,
        val datum: Long,
        val eventClass: JVMSQLiteEventClass
    )

    private suspend fun executeEventQuery(
        query: String,
        parameters: List<Pair<Types, Any?>>,
        state: Long,
        events: MutableCollection<Event>
    ) {
        @OptIn(Transaction::class)
        transaction {
            val heads = mutableListOf<EventHead>()
            JVMPreparedQuery(connection.prepareStatement(query)).use { prepared ->
                prepared.executeQuery {
                    setLong(state)
                    for ((type, value) in parameters) {
                        prepared[type] = value
                    }
                }.use { result ->
                    val resultSet = (result as JVMQueryResult).resultSet
                    while (resultSet.next()) {
                        val timestamp = Instant.parse(resultSet.getString(1))
                        val error = resultSet.getString(2)
                        val direction = resultSet.getBoolean(3)
                        val datum = resultSet.getLong(4)
                        val classId = resultSet.getLong(5)
                        val eventClass = classRepository[classId]
                        heads += EventHead(timestamp, error, direction, datum, eventClass)
                    }
                }
            }
            heads.mapTo(events) { head ->
                val eventClass = head.eventClass
                val description = eventClass.description
                val data = eventClass.queries.getDatumById(head.datum) as MutableDynamic
                if (description.kind == Kind.Direct) {
                    data["direction"] = head.direction
                }
                val timestamp = head.timestamp
                if (timestamp == null) {
                    description.prototype(data)
                } else {
                    description.event(timestamp, head.error, data)
                }
            }
        }
    }

    override suspend fun queryActualEvents(filter: Filter, state: Long): List<Event> {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val query = buildString {
            append(
                "with variability(state) as (select ?), " +
                    "space(`.id`) as (" +
                    "select particle from state_X_particle " +
                    "where state in variability) " +
                    "select timestamp, error, true, datum, class " +
                    "from datum inner join ("
            )
            appendFilter(parameters, filter)
            append(
                ") as filter on `.id` = datum.id " +
                    "left outer join event on event.datum = `.id` " +
                    "left outer join state_X_event as sXe on sXe.event = event.id " +
                    "where sXe.state in variability " +
                    "order by event.id asc;"
            )
        }
        log.verbose { "actual query: $query" }
        val result = mutableListOf<Event>()
        executeEventQuery(query, parameters, state, result)
        return result
    }

    override suspend fun filterEvent(event: Long, filter: Filter, state: Long): Boolean {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val query = buildString {
            appendLinearize(
                """
                with variability(state) as (
                    select ?
                ), space(`.id`) as (
                    select datum.id from datum
                    inner join event on datum.id = event.datum
                    inner join state_X_event as sXe on sXe.event = event.id
                    where sXe.state in variability
                )
                select exists(
                    select 1
                    from event
                    inner join state_X_event as sXe on sXe.event = event.id
                    inner join (
                """
            )
            appendFilter(parameters, filter)
            appendLinearize(
                """
                    ) as filter on filter.`.id` = event.datum
                    where event.id = ? and sXe.state in variability
                );
                """
            )
        }
        log.verbose { "filter event: $query" }
        @OptIn(Transaction::class)
        return transaction {
            JVMPreparedQuery(connection.prepareStatement(query)).use { prepared ->
                prepared.executeQuery {
                    setLong(state)
                    for ((type, value) in parameters) {
                        prepared[type] = value
                    }
                    setLong(event)
                }.use { result ->
                    val next = result.next()
                    assert(next)
                    result.getBoolean()!!
                }
            }
        }
    }

    override suspend fun queryTime(timeQuery: TimeQuery, state: Long): List<Event> {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val query = buildString {
            appendLinearize(
                """
                with variability(state) as (
                    select ?
                ), time (event) as (
                    select event from state_X_event
                    where state in variability
                ), space(`.id`) as (
                    select datum from event
                    where id in time
                )
                select timestamp, error, direction, datum, class
                from event
                inner join datum on datum.id = event.datum
                left outer join direct_event on direct_event.id = event.id
                inner join (select distinct id from (
                """
            )
            appendTimeQuery(parameters, timeQuery)
            appendLinearize(
                """
                )) as question on question.id = event.id
                where event.id in time order by event.id asc;
                """
            )
        }
        log.verbose { "time query: $query" }
        val events = mutableListOf<Event>()
        executeEventQuery(query, parameters, state, events)
        return events
    }

    private class SymmetryHead(val direction: Boolean, val datum: Long, val eventClass: JVMSQLiteEventClass)

    override suspend fun symmetry(timeQuery: TimeQuery, state: Long): List<Event> {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val query = buildString {
            appendLinearize(
                """
                with variability(state) as (
                    select ?
                ), time (event) as (
                    select event from state_X_event
                    where state in variability
                ), space(`.id`) as (
                    select datum from event
                    where id in time
                )
                select not direction, datum, class from event
                inner join datum on datum.id = event.datum
                inner join direct_event on direct_event.id = event.id
                inner join (
                """
            )
            val newTimeQuery = TimeQuery.Binary.Union(
                TimeQuery.Cause(TimeQuery.Cause.Causes.After, timeQuery),
                timeQuery
            )
            appendTimeQuery(parameters, newTimeQuery)
            appendLinearize(
                """
                ) as question on question.id = event.id
                where event.id in time and event.error is null
                order by event.id desc;
                """
            )
        }
        log.verbose { "symmetry query: $query" }
        val sequence = arrayListOf<Event>()
        @OptIn(Transaction::class)
        transaction {
            val heads = mutableListOf<SymmetryHead>()
            JVMPreparedQuery(connection.prepareStatement(query)).use { prepared ->
                prepared.executeQuery {
                    setLong(state)
                    for ((type, value) in parameters) {
                        prepared[type] = value
                    }
                }.use { result ->
                    val resultSet = (result as JVMQueryResult).resultSet
                    while (resultSet.next()) {
                        val direction = resultSet.getBoolean(1)
                        val datum = resultSet.getLong(2)
                        val classId = resultSet.getLong(3)
                        val eventClass = classRepository[classId]
                        heads += SymmetryHead(direction, datum, eventClass)
                    }
                }
            }
            heads.mapTo(sequence) { head ->
                val eventClass = head.eventClass
                val description = eventClass.description
                val data = eventClass.queries.getDatumById(head.datum) as MutableDynamic
                data["direction"] = head.direction
                description.prototype(data)
            }
        }
        return sequence
    }

    private val setReactiveState = prepare(
        "update state set reactive = ? where reactive != ? and id = ? and name is not null"
    )

    override suspend fun setReactive(state: Long, reactive: Boolean) {
        @OptIn(Transaction::class)
        transaction {
            setReactiveState.setBoolean(1, reactive)
            setReactiveState.setBoolean(2, reactive)
            setReactiveState.setLong(3, state)
            val updated = setReactiveState.executeUpdate()
            assert(updated in 0..1)
            check(updated != 1) {
                "unable to set reactive state to $reactive (it is ether unnamed or already set to $reactive)"
            }
        }
    }

    private val createState = prepareWithKeys(
        "insert into state (name) values (?);"
    )

    @Transaction
    fun createStatePrivate(name: String): Long {
        try {
            createState.setString(1, name)
            val created = createState.executeUpdate()
            assert(created == 1)
            return createState.generatedKeys.use { result ->
                val next = result.next()
                assert(next)
                result.getLong(1)
            }
        } catch (e: SQLDataException) {
            throw VariabilityException(VariabilityException.Codes.ExistingName, name)
        }
    }

    private val forkSpaceBranch = prepare(
        """
            insert into state_X_particle (state, particle)
            select ?, particle from state_X_particle
            where state = ?;
        """
    )

    private val forkTimeBranch = prepare(
        """
            insert into state_X_event (state, event)
            select ?, event from state_X_event
            where state = ?;
        """
    )

    @Transaction
    private fun forkSide(oldState: Long, newState: Long, statement: PreparedStatement) {
        statement.setLong(1, newState)
        statement.setLong(2, oldState)
        statement.executeUpdate()
    }

    @Transaction
    private fun forkPrivate(state: Long, name: String): Long {
        try {
            val newState = createStatePrivate(name)
            forkSide(state, newState, forkSpaceBranch)
            forkSide(state, newState, forkTimeBranch)
            return newState
        } catch (e: SQLDataException) {
            throw VariabilityException(VariabilityException.Codes.ExistingName, name)
        }
    }

    override suspend fun fork(state: Long, name: String): Long {
        @OptIn(Transaction::class)
        return transaction {
            forkPrivate(state, name)
        }
    }

    private val deleteState = prepare("delete from state where id = ?;")

    override suspend fun createState(name: String): Long {
        @OptIn(Transaction::class)
        return transaction {
            createStatePrivate(name)
        }
    }

    override suspend fun deleteState(state: Long) {
        @OptIn(Transaction::class)
        return transaction {
            deleteState.setLong(1, state)
            val deleted = deleteState.executeUpdate()
            check(deleted == 1) { "state #$state does not exists" }
        }
    }

    override suspend fun queryVariability(variabilityQuery: VariabilityQuery): Set<State> {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val query = buildString {
            appendLinearize(
                """
                select state.id, name, reactive from state inner join (
                    select distinct id from (
                """
            )
            appendVariabilityQuery(parameters, variabilityQuery)
            append(")) as query on query.id = state.id;")
        }
        log.verbose { "variability query: $query" }
        @OptIn(Transaction::class)
        return transaction {
            buildSet {
                JVMPreparedQuery(connection.prepareStatement(query)).use { prepared ->
                    prepared.executeQuery {
                        for ((type, value) in parameters) {
                            this[type] = value
                        }
                    }.use { result ->
                        result as JVMQueryResult
                        val resultSet = result.resultSet
                        while (resultSet.next()) {
                            val id = resultSet.getLong(1)
                            val name = resultSet.getString(2)!!
                            resultSet.getBoolean(3)
                            val state = NamedState(id, name)
                            add(state)
                        }
                    }
                }
            }
        }
    }

    private val isReactive = prepare(
        "select reactive from state where id = ?;"
    )

    override suspend fun isReactive(state: Long): Boolean {
        @OptIn(Transaction::class)
        return transaction {
            isReactive.setLong(1, state)
            isReactive.executeQuery().use { result ->
                if (!result.next()) {
                    false
                } else {
                    result.getBoolean(1)
                }
            }
        }
    }

    private class HistoryVertex(
        val id: Long,
        val eventClass: JVMSQLiteEventClass,
        val datum: Long,
        val timestamp: Instant,
        val error: String?,
        val direction: Boolean
    )

    private class RelationEdge(
        val source: Long,
        val target: Long,
        val relationship: CausalRelationships
    )

    private data class GraphNode<N>(val id: Long, val node: N)

    private fun <V> makeGraph(
        vertices: Collection<GraphNode<V>>,
        edges: Collection<RelationEdge>
    ): Graph<V, CausalRelationships> {
        val result = MutableGraph<V, CausalRelationships>()
        val id2vertex = hashMapOf<Long, MutableGraph.MutableVertex<V, CausalRelationships>>()
        for ((id, event) in vertices) {
            val vertex = result.addVertex(event)
            id2vertex[id] = vertex
        }
        for (edge in edges) {
            val source = id2vertex[edge.source]!!
            val target = id2vertex[edge.target]!!
            source.connect(target, edge.relationship)
        }
        return result
    }

    override suspend fun extractHistory(timeQuery: TimeQuery, state: Long): History {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val query = buildString {
            appendLinearize(
                """
                with recursive variability(state) as (
                    select ?
                ), space(`.id`) as (
                    select datum from event
                    inner join state_X_event as sXe on sXe.event = event.id
                    inner join variability on variability.state = sXe.state
                ), events(id) as (
                    select distinct question.id from (
                """
            )
            appendTimeQuery(parameters, timeQuery)
            appendLinearize(
                """
                ) as question
                    inner join state_X_event as sXe on question.id = sXe.event
                    inner join variability on variability.state = sXe.state
                ), search(goal, start, next) as (
                    select events2.id, events1.id, events1.id
                    from events as events1
                    cross join events as events2
                    where events2.id > events1.id
                union all
                    select goal, start, target from search
                    inner join event_X_event as eXe on source = search.next
                    inner join state_X_event as sXe on target = event
                    inner join variability on variability.state = sXe.state
                    where target == goal or (target < goal and target not in events)
                )
                    select events.id as source, class, 0 as link,
                        datum, timestamp, error, direction from events
                    inner join event on event.id = events.id
                    inner join datum on event.datum = datum.id
                    left outer join direct_event on event.id = direct_event.id
                union all
                    select source, target, case when reactive then 2 else 1 end as link,
                        null, null, null, null
                    from event_X_event as eXe
                    where source in events and target in events
                union all
                    select start, goal, 3,
                        null, null, null, null
                    from search
                    where (start, goal) not in (select source, target from event_X_event) and goal = next;
                """
            )
        }
        log.verbose { "history query: $query" }
        val (vertices, edges) = @OptIn(Transaction::class) transaction {
            val vertices = mutableListOf<HistoryVertex>()
            val edges = mutableListOf<RelationEdge>()
            JVMPreparedQuery(connection.prepareStatement(query)).use { prepared ->
                prepared.executeQuery {
                    setLong(state)
                    for (parameter in parameters) {
                        this[parameter.first] = parameter.second
                    }
                }.use { result ->
                    val resultSet = (result as JVMQueryResult).resultSet
                    while (resultSet.next()) {
                        val source = resultSet.getLong(1)
                        val target = resultSet.getLong(2)
                        val link = resultSet.getLong(3)
                        if (link == 0L) {
                            // collect vertex
                            val eventClass = classRepository[target]
                            val datum = resultSet.getLong(4)
                            val timestamp = Instant.parse(resultSet.getString(5))
                            val error = resultSet.getString(6)
                            val direction = resultSet.getBoolean(7)
                            vertices += HistoryVertex(
                                id = source,
                                eventClass = eventClass,
                                datum = datum,
                                timestamp = timestamp,
                                error = error,
                                direction = direction
                            )
                        } else {
                            // collect edge
                            val relationship = CausalRelationships.values()[link.toInt() - 1]
                            edges += RelationEdge(source = source, target = target, relationship = relationship)
                        }
                    }
                }
            }
            val nodes = vertices.map { vertex ->
                val eventClass = vertex.eventClass
                val description = eventClass.description
                val data = eventClass.queries.getDatumById(vertex.datum) as MutableDynamic
                if (description.kind == Kind.Direct) {
                    data["direction"] = vertex.direction
                }
                val event = description.event(vertex.timestamp, vertex.error, data)
                GraphNode(vertex.id, event)
            }
            nodes to edges
        }
        return makeGraph(vertices, edges)
    }

    private class MatterVertex(val id: Long, val eventClass: JVMSQLiteEventClass)

    override suspend fun extractMatter(filter: Filter, state: Long): Matter {
        val parameters = mutableListOf<Pair<Types, Any?>>()
        val query = buildString {
            appendLinearize(
                """
                with recursive variability(state) as (
                    select ?
                ), space(`.id`) as (
                    select particle from state_X_particle
                    where state in variability
                ), particles(id) as (
                """
            )
            appendFilter(parameters, filter)
            appendLinearize(
                """
                ), search(goal, start, next) as (
                    select particles2.id, particles1.id, particles1.id
                    from particles as particles1
                    cross join particles as particles2
                    where particles2.id != particles1.id
                union all
                    select goal, start, target from search
                    inner join particle_X_particle as pXp on source = search.next
                    where target == goal or (target not in particles and state in variability)
                )
                select id as source, class as target, 0 as link from particles
                    natural join datum
                union all
                    select source, target, 1
                    from particle_X_particle as pXp
                    where source in particles and target in particles and state in variability
                union all
                    select start, goal, 3 from search
                    where (start, goal) not in (select source, target from particle_X_particle) and goal = next;
                """
            )
        }
        log.verbose { "matter query: $query" }
        val (vertices, edges) = @OptIn(Transaction::class) transaction {
            val vertices = mutableListOf<MatterVertex>()
            val edges = mutableListOf<RelationEdge>()
            JVMPreparedQuery(connection.prepareStatement(query)).use { prepared ->
                prepared.executeQuery {
                    setLong(state)
                    for (parameter in parameters) {
                        this[parameter.first] = parameter.second
                    }
                }.use { result ->
                    val resultSet = (result as JVMQueryResult).resultSet
                    while (resultSet.next()) {
                        val source = resultSet.getLong(1)
                        val target = resultSet.getLong(2)
                        val link = resultSet.getLong(3)
                        if (link == 0L) {
                            // collect vertex
                            val eventClass = classRepository[target]
                            vertices += MatterVertex(
                                id = source,
                                eventClass = eventClass
                            )
                        } else {
                            // collect edge
                            val relationship = CausalRelationships.values()[link.toInt() - 1]
                            edges += RelationEdge(source = source, target = target, relationship = relationship)
                        }
                    }
                }
            }
            val nodes = vertices.map { vertex ->
                val eventClass = vertex.eventClass
                val description = eventClass.description
                val data = eventClass.queries.getRealDatumById(vertex.id)!!
                val particle = Particle(description, data)
                GraphNode(vertex.id, particle)
            }
            nodes to edges
        }
        return makeGraph(vertices, edges)
    }

    override suspend fun isEmpty(): Triple<Boolean, Boolean, Boolean> {
        @Transaction
        fun Statement.exists(@Language("SQL") query: String): Boolean =
            executeQuery("select exists($query);").use { result ->
                val next = result.next()
                assert(next)
                result.getBoolean(1)
            }
        @OptIn(Transaction::class)
        return transaction {
            connection.createStatement().use { statement ->
                val events = statement.exists("select 1 from event")
                val datums = statement.exists("select 1 from datum")
                val interfaces = statement.exists("select 1 from interface")
                Triple(events, datums, interfaces)
            }
        }
    }

    override fun close() {
        allStatements.forEach { it.close() }
        classRepository.close()
    }
}

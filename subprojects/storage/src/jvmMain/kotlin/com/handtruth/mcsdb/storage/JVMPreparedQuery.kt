package com.handtruth.mcsdb.storage

import com.handtruth.mc.types.UUID
import com.handtruth.mcsdb.schema.Types
import com.handtruth.mcsdb.types.Serial
import io.ktor.utils.io.core.*
import kotlinx.datetime.Instant
import java.sql.PreparedStatement
import java.sql.ResultSet
import kotlin.io.use
import java.sql.Types as SQLTypes

class JVMQueryResult(val resultSet: ResultSet) : QueryResult() {
    private inline fun <reified T> nullable(lambda: ResultSet.() -> T): T? {
        val value = resultSet.lambda()
        return if (resultSet.wasNull()) null else value
    }

    override fun getSerial(): Serial? {
        val value = resultSet.getLong(index++)
        return if (resultSet.wasNull()) null else Serial(value)
    }

    override fun getBoolean() = nullable { getBoolean(index++) }
    override fun getByte() = nullable { getByte(index++) }
    override fun getShort() = nullable { getShort(index++) }
    override fun getInt() = nullable { getInt(index++) }
    override fun getLong() = nullable { getLong(index++) }

    override fun getUByte() = nullable { getShort(index++).toUByte() }
    override fun getUShort() = nullable { getInt(index++).toUShort() }
    override fun getUInt() = nullable { getLong(index++).toUInt() }
    override fun getULong() = nullable {
        val bytes = getBytes(index++)
        assert(bytes.size == 8)
        var result = 0uL
        for (i in bytes.indices) {
            result = (result shl 8) or (bytes[i].toULong() and 0xFFuL)
        }
        result
    }

    override fun getFloat() = nullable { getFloat(index++) }
    override fun getDouble() = nullable { getDouble(index++) }
    override fun getChar() = nullable { getInt(index++).toChar() }
    override fun getString(): String? = resultSet.getString(index++)

    override fun getUUID(): UUID? {
        val most = resultSet.getLong(index++)
        val least = resultSet.getLong(index++)
        return if (resultSet.wasNull()) null else UUID(most, least)
    }

    override fun getInstant(): Instant? {
        val timestamp = resultSet.getString(index++)
        return if (resultSet.wasNull()) null else Instant.parse(timestamp)
    }

    override fun getStructure(): Any? {
        val blob = resultSet.getBytes(index++)
        return blob?.let {
            ByteReadPacket(blob).use { input ->
                val tagId = input.readByte()
                val tag = nbt.tagsModule.tagById(tagId)
                tag.readBinary(input, nbt)
            }
        }
    }

    override fun getArbitrary(): ByteArray? {
        return resultSet.getBytes(index++)
    }

    override fun get(type: Types): Any? = when (type) {
        Types.Serial -> getSerial()
        Types.Boolean -> getBoolean()
        Types.Byte -> getByte()
        Types.Short -> getShort()
        Types.Int -> getInt()
        Types.Long -> getLong()
        Types.UByte -> getUByte()
        Types.UShort -> getUShort()
        Types.UInt -> getUInt()
        Types.ULong -> getULong()
        Types.Float -> getFloat()
        Types.Double -> getDouble()
        Types.Char -> getChar()
        Types.String -> getString()
        Types.UUID -> getUUID()
        Types.Instant -> getInstant()
        Types.Structure -> getStructure()
        Types.Arbitrary -> getArbitrary()
    }

    override fun next(): Boolean {
        super.next()
        return resultSet.next()
    }

    override fun close() {
        resultSet.close()
    }
}

class JVMPreparedQuery(private val preparedStatement: PreparedStatement) : PreparedQuery() {
    private inline fun <reified T> nullable(type: Int, value: T?, block: PreparedStatement.(Int, T) -> Unit) {
        if (value == null) {
            preparedStatement.setNull(index++, type)
        } else {
            preparedStatement.block(index++, value)
        }
    }

    override fun setSerial(value: Serial?) = nullable(SQLTypes.BIGINT, value) { i, v ->
        setLong(i, v.toLong())
    }

    override fun setBoolean(value: Boolean?) = nullable(SQLTypes.BOOLEAN, value, PreparedStatement::setBoolean)
    override fun setByte(value: Byte?) = nullable(SQLTypes.TINYINT, value, PreparedStatement::setByte)
    override fun setShort(value: Short?) = nullable(SQLTypes.SMALLINT, value, PreparedStatement::setShort)
    override fun setInt(value: Int?) = nullable(SQLTypes.INTEGER, value, PreparedStatement::setInt)
    override fun setLong(value: Long?) = nullable(SQLTypes.BIGINT, value, PreparedStatement::setLong)

    override fun setUByte(value: UByte?) = nullable(SQLTypes.TINYINT, value) { i, v ->
        setShort(i, v.toShort())
    }

    override fun setUShort(value: UShort?) = nullable(SQLTypes.SMALLINT, value) { i, v ->
        setInt(i, v.toInt())
    }

    override fun setUInt(value: UInt?) = nullable(SQLTypes.INTEGER, value) { i, v ->
        setLong(i, v.toLong())
    }

    override fun setULong(value: ULong?) = nullable(SQLTypes.BIGINT, value) { i, v ->
        val bytes = ByteArray(8) { (v shr ((7 - it) shl 3) and 0xFFu).toByte() }
        setBytes(i, bytes)
    }

    override fun setFloat(value: Float?) = nullable(SQLTypes.FLOAT, value, PreparedStatement::setFloat)
    override fun setDouble(value: Double?) = nullable(SQLTypes.DOUBLE, value, PreparedStatement::setDouble)

    override fun setChar(value: Char?) = nullable(SQLTypes.SMALLINT, value) { i, v ->
        setInt(i, v.toInt() and 0xFFFF)
    }

    override fun setString(value: String?) = nullable(SQLTypes.VARCHAR, value, PreparedStatement::setString)

    override fun setUUID(value: UUID?) {
        if (value == null) {
            repeat(2) {
                preparedStatement.setNull(index++, SQLTypes.BIGINT)
            }
        } else {
            preparedStatement.setLong(index++, value.most)
            preparedStatement.setLong(index++, value.least)
        }
    }

    override fun setInstant(value: Instant?) {
        if (value == null) {
            preparedStatement.setNull(index++, SQLTypes.VARCHAR)
        } else {
            preparedStatement.setString(index++, value.toString())
        }
    }

    override fun setStructure(value: Any?) = nullable(SQLTypes.BLOB, value) { i, v ->
        val blob = buildByteArray {
            val tag = nbt.tagsModule.tagOf(v)
            val id = nbt.tagsModule.tagIdOf(tag)
            writeByte(id)
            tag.writeBinary(this, nbt, v)
        }
        setBytes(i, blob)
    }

    override fun setArbitrary(value: ByteArray?) = nullable(SQLTypes.BLOB, value) { i, v ->
        setBytes(i, v)
    }

    override fun set(type: Types, value: Any?) = when (type) {
        Types.Serial -> setSerial(value as Serial?)
        Types.Boolean -> setBoolean(value as Boolean?)
        Types.Byte -> setByte(value as Byte?)
        Types.Short -> setShort(value as Short?)
        Types.Int -> setInt(value as Int?)
        Types.Long -> setLong(value as Long?)
        Types.UByte -> setUByte(value as UByte?)
        Types.UShort -> setUShort(value as UShort?)
        Types.UInt -> setUInt(value as UInt?)
        Types.ULong -> setULong(value as ULong?)
        Types.Float -> setFloat(value as Float?)
        Types.Double -> setDouble(value as Double?)
        Types.Char -> setChar(value as Char?)
        Types.String -> setString(value as String?)
        Types.UUID -> setUUID(value as UUID?)
        Types.Instant -> setInstant(value as Instant?)
        Types.Structure -> setStructure(value)
        Types.Arbitrary -> setArbitrary(value as ByteArray?)
    }

    @Transaction
    override fun executeQuery(): JVMQueryResult {
        return JVMQueryResult(preparedStatement.executeQuery())
    }

    @Transaction
    override fun executeUpdate(): Int {
        return preparedStatement.executeUpdate()
    }

    override fun close() {
        preparedStatement.close()
    }
}

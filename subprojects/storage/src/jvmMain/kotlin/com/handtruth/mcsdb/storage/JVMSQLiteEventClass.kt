package com.handtruth.mcsdb.storage

import com.handtruth.mcsdb.schema.EventDescription
import java.sql.Connection

class JVMSQLiteEventClass(
    id: Long,
    description: EventDescription,
    metadata: EventDescriptionMetadata
) : EventClass(id, description, metadata) {

    lateinit var queries: Queries
        private set

    fun prepareQueries(connection: Connection) {
        val eventClass = this
        assert(!eventClass::queries.isInitialized)
        val getDatumById = buildString { buildGetDatumById(eventClass) }
        val getRealDatumById = buildString { buildGetDatumById(eventClass, true) }
        val getDatumId = buildString { buildGetDatumId(eventClass) }
        val getParticleId = buildString { buildGetDatumId(eventClass, true) }
        val getInterfaceId = buildString { buildGetInterfaceId(eventClass) }
        val insertInterface = buildString { buildInsertInterface(eventClass) }
        val checkInterfaceExists = buildString { buildCheckInterfaceExists(eventClass) }
        val queries = Queries(
            eventClass = eventClass,
            getDatumById = JVMPreparedQuery(connection.prepareStatement(getDatumById)),
            getRealDatumById = JVMPreparedQuery(connection.prepareStatement(getRealDatumById)),
            getDatumId = JVMPreparedQuery(connection.prepareStatement(getDatumId)),
            getParticleId = JVMPreparedQuery(connection.prepareStatement(getParticleId)),
            getInterfaceId = JVMPreparedQuery(connection.prepareStatement(getInterfaceId)),
            insertInterface = JVMPreparedQuery(connection.prepareStatement(insertInterface)),
            checkInterfaceExists = JVMPreparedQuery(connection.prepareStatement(checkInterfaceExists))
        )
        eventClass.queries = queries
    }

    override fun close() {
        queries.close()
    }
}

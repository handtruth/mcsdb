package com.handtruth.mcsdb.storage

import io.ktor.utils.io.core.*

inline fun buildByteArray(block: Output.() -> Unit): ByteArray = buildPacket { block() }.readBytes()

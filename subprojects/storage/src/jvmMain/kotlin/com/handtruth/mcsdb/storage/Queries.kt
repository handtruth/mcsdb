package com.handtruth.mcsdb.storage

import com.handtruth.mc.types.Dynamic
import com.handtruth.mc.types.buildDynamic
import com.handtruth.mcsdb.schema.FieldDescription
import io.ktor.utils.io.core.*
import kotlin.io.use

class Queries(
    private val eventClass: EventClass,
    private val getDatumById: PreparedQuery,
    private val getRealDatumById: PreparedQuery,
    private val getDatumId: PreparedQuery,
    private val getParticleId: PreparedQuery,
    private val getInterfaceId: PreparedQuery,
    private val insertInterface: PreparedQuery,
    private val checkInterfaceExists: PreparedQuery,
) : Closeable {

    private fun PreparedQuery.setFields(data: Dynamic, fields: List<FieldDescription>) {
        for (field in fields) {
            this[field.type] = data.getOrNull(field.name)
        }
    }

    @Transaction
    private fun getDatumByIdGeneric(id: Long, query: PreparedQuery, fields: List<FieldDescription>): Dynamic? {
        return query.executeQuery {
            setLong(id)
        }.use {
            if (it.next()) {
                buildDynamic {
                    check(id == it.getLong())
                    for (field in fields) {
                        field.name assign it[field.type]
                    }
                }
            } else {
                null
            }
        }
    }

    @Transaction
    fun getDatumById(id: Long): Dynamic? =
        getDatumByIdGeneric(id, getDatumById, eventClass.description.allFields)

    @Transaction
    fun getRealDatumById(id: Long): Dynamic? =
        getDatumByIdGeneric(id, getRealDatumById, eventClass.description.allRealFields)

    @Transaction
    fun getDatumId(data: Dynamic): Long? {
        return getDatumId.executeQuery {
            setFields(data, eventClass.description.allFields)
        }.use {
            if (it.next()) it.getLong()!! else null
        }
    }

    @Transaction
    fun getParticleId(data: Dynamic, state: Long): Long {
        return getParticleId.executeQuery {
            setLong(state)
            setFields(data, eventClass.description.allRealFields)
        }.use {
            if (it.next()) {
                it.getLong()!!
            } else {
                0L
            }
        }
    }

    @Transaction
    fun getInterfaceId(data: Dynamic): Long? {
        return getInterfaceId.executeQuery {
            setFields(data, eventClass.description.fields)
        }.use {
            if (it.next()) it.getLong()!! else null
        }
    }

    @Transaction
    fun insertInterface(id: Long, data: Dynamic) {
        val insert = insertInterface.executeUpdate {
            setLong(id)
            setFields(data, eventClass.description.fields)
        }
        assert(insert == 1)
    }

    @Transaction
    fun checkInterfaceExists(data: Dynamic, state: Long): Boolean {
        checkInterfaceExists.executeQuery {
            setLong(state)
            setFields(data, eventClass.allFields)
        }.use { result ->
            val next = result.next()
            assert(next)
            return result.getBoolean()!!
        }
    }

    override fun close() {
        getDatumById.close()
        getRealDatumById.close()
        getDatumId.close()
        getParticleId.close()
        getInterfaceId.close()
        insertInterface.close()
        checkInterfaceExists.close()
    }
}

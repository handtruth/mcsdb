package com.handtruth.mcsdb.storage

@Retention(AnnotationRetention.BINARY)
@RequiresOptIn
annotation class Transaction

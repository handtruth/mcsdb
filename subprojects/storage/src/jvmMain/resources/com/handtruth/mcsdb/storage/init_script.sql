PRAGMA application_id = 1339;

PRAGMA foreign_keys = ON;

-- Ассоциативный массив с некоторыми значениями MCSDB
create table if not exists mcsdb_properties (
    name text not null primary key,
    value blob
) without rowid;

-- Задание версии хранилища MCSDB
insert or ignore into mcsdb_properties values ('version', '0.1');

-- Задание имени состояния по умолчанию
insert or ignore into mcsdb_properties values ('default_state', 'main');

-- Таблица со всеми описаниями событий
create table if not exists class (
    id integer primary key autoincrement not null,
    name text not null,
    description blob not null,
    -- Этот атрибут хранит изменяемые параметры описания,
    -- например следующее значение числа при использовании
    -- модификатора auto_increment
    metadata blob not null,
    unique ( name )
);

create unique index if not exists class_name_idx on class ( name );

-- Таблица, которая связывает описание события
-- с набором значений полей, которые непосредственно
-- объявлены в описании события.
create table if not exists interface (
    id integer primary key autoincrement not null,
    class integer not null references class
);

-- Таблица, связывающая датум и описание события
create table if not exists datum (
    id integer primary key autoincrement not null,
    class integer not null references class
);

-- Таблица со всеми событиями во всех состояниях
create table if not exists event (
    id integer primary key autoincrement not null,
    datum integer not null references datum,
    timestamp text not null,
    error text null
);

create index if not exists event_timestamp_idx on event ( timestamp );

-- Таблица связывающая направленное событие с его направлением
create table if not exists direct_event (
    id integer primary key not null references event on delete cascade,
    direction boolean not null
) without rowid;

-- Таблица со всеми состояниями
create table if not exists state (
    id integer primary key autoincrement not null,
    name text not null unique,
    reactive boolean not null default false
);

-- В таблице состояний должно присутствовать главное состояние
insert or ignore into state ( name, reactive )
    select value, true from mcsdb_properties where name = 'default_state';

-- Таблица, которая описывает содержание каждого датума
create table if not exists interface_X_datum (
    interface integer not null references interface,
    datum integer not null references datum on delete cascade,
    role boolean not null,
    primary key ( interface, datum )
) without rowid;

-- Датум становится частицей, только когда прикреплён к
-- какому-нибудь состоянию (с некоторыми fantom оговорками)
create table if not exists state_X_particle (
    state integer not null references state on delete cascade,
    particle integer not null references datum,
    primary key ( state, particle )
) without rowid;

-- Таблица, связывающая состояние и события
create table if not exists state_X_event (
    state integer not null references state on delete cascade,
    event integer not null references event on delete cascade,
    primary key ( state, event )
) without rowid;

-- Таблица причинно-следственных связей
create table if not exists event_X_event (
    source integer not null references event on delete cascade,
    target integer not null references event on delete cascade,
    reactive boolean not null default false,
    primary key ( source, target ),
    check ( source != target )
) without rowid;

-- Историческая зависимость состояний
create view if not exists state_X_state as
    with causal as (
        select distinct sXeS.state as a, sXeT.state as b from event_X_event as eXe
        inner join state_X_event as sXeS on sXeS.event = eXe.source
        inner join state_X_event as sXeT on sXeT.event = eXe.target
        where sXeS.state != sXeT.state
    ), subset as (
        select sXe1.state as source, sXe2.state as target from state_X_event as sXe1
        inner join state_X_event as sXe2 on sXe1.event = sXe2.event
        where sXe1.state != sXe2.state
        group by sXe1.state, sXe2.state
        having count(*) = (select count(*) from state_X_event as sXe3 where sXe3.state = sXe1.state)
    )
    select source, target from subset
    inner join causal on source = a and target = b;

-- Вычисление связей между частицами через интерфейсы
create view if not exists particle_X_particle as
    with state_interfaces (interface, particle, role, state) as (
        select interface, particle, role, state from state_X_particle as sXp
        inner join interface_X_datum as iXd on sXp.particle = iXd.datum
    ) select source.interface as interface, source.particle as source, target.particle as target, source.state as state
    from state_interfaces as source
    inner join state_interfaces as target on source.interface = target.interface and source.state = target.state
    where source.role = 1 and target.role = 3;

-- Друзья датумов
create view if not exists entanglement(a, b) as
with candidates(a, b) as (
    select iXd1.datum, iXd2.datum
    from interface_X_datum as iXd1
    inner join interface_X_datum as iXd2 on iXd2.interface = iXd1.interface
    where iXd1.role = 0 and iXd2.role = 0
), interfaces(interface, datum) as (
    select interface, datum from interface_X_datum
    where role != 2
)
select a, b from candidates
inner join interfaces as ia on a = ia.datum
inner join interfaces as ib on b = ib.datum
group by a, b
having group_concat(distinct ia.interface) = group_concat(distinct ib.interface);

-- Вычисление модельных причинно-следственных связей
-- для указанного состояния
create view if not exists current_ancestors(state, datum, event) as
    with interface_X_event(event, datum, role, interface) as (
        select max(event.id), event.datum, role, interface from event
        inner join interface_X_datum as iXd on iXd.datum = event.datum
        group by event.datum, role, interface
    ), sources(state, interface, datum, event) as (
        select sXp.state, interface, event.datum, max(event.id)
        from interface_X_datum as iXd
        inner join state_X_particle as sXp on particle = iXd.datum
        inner join event on iXd.datum = event.datum
        inner join state_X_event as sXe on sXe.event = event.id and sXp.state = sXe.state
        where role = 1
        group by sXp.state, interface, event.datum
    ), links(state, datum, event) as (
        -- root interface
        select state, a, max(sXe.event)
        from interface_X_event as iXe
        inner join entanglement on entanglement.b = iXe.datum
        inner join state_X_event as sXe on iXe.event = sXe.event
        where role = 0
        group by state, a
    union all
        -- source/target swap
        select sXe.state, iXd_this.datum, iXe_other.event
        from interface_X_datum as iXd_this
        inner join interface_X_event as iXe_other on iXe_other.interface = iXd_this.interface
        inner join state_X_event as sXe on iXe_other.event = sXe.event
        where (iXd_this.role = 1 and iXe_other.role = 3) -- source/target
           or (iXd_this.role = 3 and iXe_other.role = 1) -- target/source
    union all
        -- fantom interface
        select sources.state, iXd_this.datum, sources.event
        from interface_X_datum as iXd_this
        inner join sources on iXd_this.interface = sources.interface
        where iXd_this.role = 2
    union all
        -- source links
        select sources.state, iXd_this.datum, sources.event from interface_X_datum as iXd_this
        inner join interface_X_datum as iXd_targets on iXd_this.interface = iXd_targets.interface
        inner join state_X_particle as sXp on iXd_targets.datum = sXp.particle
        inner join sources on iXd_this.interface = sources.interface and sXp.state = sources.state
        where iXd_this.role = 1 and iXd_targets.role = 3
        group by sources.state, iXd_this.datum
        having count(iXd_targets.datum) >= 1 and count(sources.datum) = 1
    ), successfull_links(state, datum, event) as (
        select state, links.datum, event from links
        inner join event on event.id = event
        where error is null
    ), min_link(state, datum, min_event) as (
        select state, datum, min(event) from successfull_links
        group by state, datum
    ), walking(state, datum, start, next, lvl) as (
        -- транзитивное сокращение
        select state, datum, event, event, 0 from successfull_links
    union all
        select sXe.state, walking.datum, start, source, lvl + 1 from walking
        inner join event_X_event as eXe on target = next
        inner join state_X_event as sXe on source = sXe.event
        inner join min_link on sXe.state = min_link.state and walking.datum = min_link.datum
        where source >= min_event
    ) select distinct state, datum, start from (
        select state, datum, start, next, max(lvl) from walking
        group by state, datum, next
    );

-- Несколько триггеров для контроля состояния
create trigger if not exists datum_econ_particle after delete on state_X_particle
when not exists(select * from state_X_particle where particle = old.particle)
 and not exists(select * from event where datum = old.particle)
begin
    delete from datum where id = old.particle;
end;

create trigger if not exists datum_econ_event after delete on event
when not exists(select * from state_X_particle where particle = old.datum)
 and not exists(select * from event where datum = old.datum)
begin
    delete from datum where id = old.datum;
end;

create trigger if not exists interface_econ after delete on interface_X_datum
when not exists(select * from interface_X_datum where interface = old.interface)
begin
    delete from interface where id = old.interface;
end;

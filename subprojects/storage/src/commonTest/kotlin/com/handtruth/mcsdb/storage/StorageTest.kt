package com.handtruth.mcsdb.storage

import com.handtruth.mc.types.Dynamic
import com.handtruth.mc.types.MutableDynamic
import com.handtruth.mc.types.buildDynamic
import com.handtruth.mc.types.get
import com.handtruth.mcsdb.filter
import com.handtruth.mcsdb.schema.EventDescription
import com.handtruth.mcsdb.schema.Kind
import com.handtruth.mcsdb.test.*
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.dataOf
import com.handtruth.mcsdb.time.descriptionOf
import com.handtruth.mcsdb.time.timeQuery
import io.ktor.test.dispatcher.*
import io.ktor.utils.io.core.*
import kotlinx.datetime.Clock
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class StorageTest {

    private suspend fun Storage.positiveEvent(
        description: EventDescription,
        state: Long,
        data: Dynamic = Dynamic(),
        vararg reactive: Long
    ): Event {
        val prototype = description.prototype(buildDynamic { fields += data.fields; "direction" assign true })
        val (mainPrototype, dependents) = resolveNextPrototypes(prototype, state)
        val prototypeList = dependents + mainPrototype
        assertEquals(1, prototypeList.size)
        val fullPrototype = prototypeList.first()
        val fullData = dataOf(fullPrototype)
        assertEquals(description, descriptionOf(fullPrototype))
        if (description.kind == Kind.Direct) {
            assertEquals(true, fullData.getOrNull("direction"))
        }
        val event = description.event(Clock.System.now(), fullData)
        val id = saveEvent(event, state, reactive.asList())
        (dataOf(event) as MutableDynamic)[".event"] = id
        return event
    }

    private suspend inline fun Storage.positiveEvent(
        description: EventDescription,
        state: Long,
        vararg reactive: Event,
        data: MutableDynamic.() -> Unit
    ) = positiveEvent(
        description,
        state,
        buildDynamic(data),
        *LongArray(reactive.size) { dataOf(reactive[it])[".event"] as Long }
    )

    private fun storage(block: suspend (Storage) -> Unit) {
        testSuspend {
            val mem = "file:test?mode=memory"
            // val file = "file.db"
            sqliteStorage(mem).use { block(it) }
        }
    }

    @Test
    fun deleteDataTest() = storage { storage ->
        val state = storage.getDefaultState()
        storage.registerEventDescription(actorEvent)
        storage.registerEventDescription(systemActorEvent)
        storage.registerEventDescription(userEvent)
        storage.registerEventDescription(userContextEvent)
        storage.registerEventDescription(userActorEvent)
        storage.registerEventDescription(groupEvent)
        storage.registerEventDescription(groupContextEvent)
        storage.registerEventDescription(groupActorEvent)
        storage.registerEventDescription(groupDescriptionEvent)
        storage.registerEventDescription(groupMemberEvent)
        storage.registerEventDescription(userCreationEvent)
        storage.registerEventDescription(groupCreationEvent)
        storage.registerEventDescription(messageEvent)

        // put some events
        val systemActor = storage.positiveEvent(systemActorEvent, state)
        val ktlo = storage.positiveEvent(userContextEvent, state) {
            "user" assign "ktlo"
            "actor" assign dataOf(systemActor)["actor"]
        }
        val ktloActor = storage.positiveEvent(userActorEvent, state) {
            "user" assign dataOf(ktlo)["user"]
        }
        storage.positiveEvent(userCreationEvent, state, ktlo, ktloActor) {
            "user" assign dataOf(ktlo)["user"]
            "actor" assign dataOf(systemActor)["actor"]
        }
        val tarsimun = storage.positiveEvent(userContextEvent, state) {
            "user" assign "tarsimun"
            "actor" assign dataOf(ktloActor)["actor"]
        }
        val tarsimunActor = storage.positiveEvent(userActorEvent, state) {
            "user" assign "tarsimun"
        }
        storage.positiveEvent(userCreationEvent, state, tarsimun, tarsimunActor) {
            "user" assign dataOf(tarsimun)["user"]
            "actor" assign dataOf(ktloActor)["actor"]
        }
        val people = storage.positiveEvent(groupContextEvent, state) {
            "group" assign "people"
            "actor" assign dataOf(ktloActor)["actor"]
        }
        val description = "Everybody people!"
        val peopleDescription = storage.positiveEvent(groupDescriptionEvent, state) {
            "group" assign dataOf(people)["group"]
            "description" assign description
        }
        val peopleActor = storage.positiveEvent(groupActorEvent, state) {
            "group" assign dataOf(people)["group"]
        }
        storage.positiveEvent(groupCreationEvent, state, people, peopleDescription, peopleActor) {
            "group" assign dataOf(people)["group"]
            "actor" assign dataOf(ktloActor)["actor"]
            "description" assign description
        }
        storage.positiveEvent(groupMemberEvent, state) {
            "user" assign dataOf(tarsimun)["user"]
            "group" assign dataOf(people)["group"]
            "actor" assign dataOf(ktloActor)["actor"]
        }
        storage.positiveEvent(messageEvent, state) {
            "user" assign dataOf(tarsimun)["user"]
            "actor" assign dataOf(ktloActor)["actor"]
            "message" assign "Hello fellow human!"
        }

        // reverse all the events
        storage.symmetry(timeQuery { U }, state).forEach {
            val event = descriptionOf(it).event(Clock.System.now(), dataOf(it))
            storage.saveEvent(event, state, emptyList())
        }
        val matter = storage.extractMatter(filter { U }, state)
        assertTrue(matter.toString()) { matter.vertices.isEmpty() }
        storage.forget(timeQuery { U }, state)
        val history = storage.extractHistory(timeQuery { U }, state)
        assertTrue { history.vertices.isEmpty() }
        val (events, datums, interfaces) = storage.isEmpty()
        assertFalse(events, "some events still there")
        assertFalse(datums, "some datums still there")
        assertFalse(interfaces, "some interfaces still there")
    }
}

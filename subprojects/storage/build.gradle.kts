plugins {
    kotlin("plugin.serialization")
}

dependencies {
    commonMainImplementation(project(":mcsdb-base"))
    commonMainImplementation(project(":mcsdb-encoding"))
    commonMainImplementation("com.handtruth.kommon:kommon-log")
    commonTestImplementation(project(":mcsdb-test-schema"))
    jvmMainImplementation("org.xerial:sqlite-jdbc")
    commonTestImplementation("io.ktor:ktor-test-dispatcher")
    jvmTestRuntimeOnly("ch.qos.logback:logback-classic")
}

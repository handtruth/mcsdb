dependencies {
    fun kotlinx(name: String) = "org.jetbrains.kotlinx:kotlinx-$name"
    fun handtruth(name: String) = "com.handtruth.$name"
    fun mc(name: String) = handtruth("mc:$name")
    fun kommon(name: String) = handtruth("kommon:kommon-$name")

    commonMainApi(project(":mcsdb-base"))
    commonMainApi(mc("tools-nbt"))
    commonMainApi(mc("tools-paket"))
    commonTestImplementation(project(":mcsdb-test-schema"))
}

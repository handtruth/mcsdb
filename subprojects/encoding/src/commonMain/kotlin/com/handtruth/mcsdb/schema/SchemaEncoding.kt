package com.handtruth.mcsdb.schema

import com.handtruth.mc.paket.codec.*
import io.ktor.utils.io.core.*
import kotlinx.collections.immutable.toImmutableList

val KindCodec = EnumCodec(Kind.values())
val RolesCodec = EnumCodec(Roles.values())
val TypesCodec = EnumCodec(Types.values())

fun interface EventDescriptionMapper {
    operator fun get(name: String): EventDescription
}

class EventDescriptionCodec(private val mapper: EventDescriptionMapper) : Codec<EventDescription> {
    override fun measure(value: EventDescription): Nothing = throw UnsupportedOperationException()

    override fun read(input: Input) = input.readEventDescription(mapper)

    override fun write(output: Output, value: EventDescription) = output.writeEventDescription(value)
}

fun Input.readEventDescription(mapper: EventDescriptionMapper): EventDescription {
    val name = StringCodec.read(this)
    val kind = KindCodec.read(this)
    val modifiers = readModifiersAsEventBound()
    val inheritsSize = UZIntCodec.read(this).toInt()
    val inherits = List(inheritsSize) { readInheritanceDescription(mapper) }
    val fieldsSize = UZIntCodec.read(this).toInt()
    val fields = List(fieldsSize) { readFieldDescription() }
    return EventDescription(name, kind, ModifierSet(modifiers), inherits.toImmutableList(), fields.toImmutableList())
}

fun Output.writeEventDescription(value: EventDescription) {
    StringCodec.write(this, value.name)
    KindCodec.write(this, value.kind)
    writeEventBoundModifiers(value.modifiers)
    UZIntCodec.write(this, value.inherits.size.toUInt())
    for (inherit in value.inherits) {
        writeInheritanceDescription(inherit)
    }
    UZIntCodec.write(this, value.fields.size.toUInt())
    for (field in value.fields) {
        writeFieldDescription(field)
    }
}

fun Input.readFieldDescription(): FieldDescription {
    val name = StringCodec.read(this)
    val type = TypesCodec.read(this)
    val modifiers = readModifiersAsFieldBound()
    return FieldDescription(name, type, ModifierSet(modifiers))
}

fun Input.readInheritanceDescription(map: EventDescriptionMapper): InheritanceDescription {
    val name = StringCodec.read(this)
    val description = map[name]
    val modifiers = readModifiersAsInheritanceBound()
    return InheritanceDescription(description, ModifierSet(modifiers))
}

fun Output.writeFieldDescription(value: FieldDescription) {
    StringCodec.write(this, value.name)
    TypesCodec.write(this, value.type)
    writeFieldBoundModifiers(value.modifiers)
}

fun Output.writeInheritanceDescription(value: InheritanceDescription) {
    StringCodec.write(this, value.description.name)
    writeInheritanceBoundModifiers(value.modifiers)
}

fun Input.readModifiers() = buildSet {
    while (true) {
        val modifier = when (val id = readByte().toInt()) {
            0 -> break
            1 -> Modifier.Role(RolesCodec.read(this@readModifiers))
            2 -> Modifier.Unique(BooleanCodec.read(this@readModifiers))
            3 -> Modifier.AutoIncrement
            4 -> Modifier.Nullable
            5 -> Modifier.Indexed
            else -> error("unknown modifier #$id")
        }
        add(modifier)
    }
}

private inline fun <reified M> Input.readModifiersAs(): Set<M> {
    val modifiers = readModifiers()
    for (modifier in modifiers) {
        modifier as M
    }
    @Suppress("UNCHECKED_CAST")
    return modifiers as Set<M>
}

fun Input.readModifiersAsEventBound(): Set<Modifier.EventBound> = readModifiersAs()
fun Input.readModifiersAsInheritanceBound(): Set<Modifier.InheritanceBound> = readModifiersAs()
fun Input.readModifiersAsFieldBound(): Set<Modifier.FieldBound> = readModifiersAs()

fun Output.writeModifiers(value: Set<Modifier>) {
    for (modifier in value) {
        when (modifier) {
            is Modifier.Role -> {
                writeByte(1)
                RolesCodec.write(this, modifier.role)
            }
            is Modifier.Unique -> {
                writeByte(2)
                BooleanCodec.write(this, modifier.unique)
            }
            Modifier.AutoIncrement -> {
                writeByte(3)
            }
            Modifier.Nullable -> {
                writeByte(4)
            }
            Modifier.Indexed -> {
                writeByte(5)
            }
        }
    }
    writeByte(0)
}

private fun <M> Output.writeCategorizedModifiers(modifiers: Set<M>) {
    for (modifier in modifiers) {
        modifier as Modifier
    }
    @Suppress("UNCHECKED_CAST")
    modifiers as Set<Modifier>
    writeModifiers(modifiers)
}

fun Output.writeEventBoundModifiers(modifiers: Set<Modifier.EventBound>) =
    writeCategorizedModifiers(modifiers)

fun Output.writeInheritanceBoundModifiers(modifiers: Set<Modifier.InheritanceBound>) =
    writeCategorizedModifiers(modifiers)

fun Output.writeFieldBoundModifiers(modifiers: Set<Modifier.FieldBound>) =
    writeCategorizedModifiers(modifiers)

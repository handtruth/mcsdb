package com.handtruth.mcsdb.test

import com.handtruth.mcsdb.schema.EventDescriptionMapper
import com.handtruth.mcsdb.schema.readEventDescription
import com.handtruth.mcsdb.schema.writeEventDescription
import io.ktor.utils.io.core.*
import kotlin.test.Test
import kotlin.test.assertEquals

class EncodingTest {
    @Test
    fun encodeAndDecode() {
        val descriptions = dynamicMCSManSchema.associateBy { it.name }
        val mapper = EventDescriptionMapper { descriptions[it]!! }

        for (expected in dynamicMCSManSchema) {
            val actual = buildPacket {
                writeEventDescription(expected)
            }.use {
                it.readEventDescription(mapper)
            }
            assertEquals(expected, actual)
        }
    }
}

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    application
}

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/27145523/packages/maven")
    maven("https://mvn.handtruth.com")
}

application {
    mainClass.set("io.ktor.server.cio.EngineMain")
}

dependencies {
    val mcsdbVersion: String by project
    val ktorVersion: String by project
    val koinVersion: String by project

    implementation(platform("com.handtruth.mcsdb:mcsdb-bom:$mcsdbVersion"))
    implementation(platform("io.ktor:ktor-bom:$ktorVersion"))

    implementation("com.handtruth.mcsdb:mcsdb-static-api")
    runtimeOnly("com.handtruth.mcsdb:mcsdb-embedded")

    implementation("io.ktor:ktor-server-core")
    implementation("io.ktor:ktor-serialization")
    runtimeOnly("io.ktor:ktor-server-cio")

    implementation("io.insert-koin:koin-ktor:$koinVersion")
    implementation("io.insert-koin:koin-logger-slf4j:$koinVersion")
    implementation("com.handtruth:docker-client:0.1.0")
    implementation("com.handtruth.kommon:kommon-log")
    runtimeOnly("ch.qos.logback:logback-classic:1.2.3")

    testImplementation("io.ktor:ktor-server-tests")
}

package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class StructuredText(
    val text: String,
    val bold: Boolean = false,
    val italic: Boolean = false,
    val underlined: Boolean = false,
    val color: Colors = Colors.White,
) {
    @Serializable
    enum class Colors {
        White, Red, Blue, Green, Black
    }
}

@Serializable
data class User(val id: Long, val name: String)

@Serializable
data class ErrorObject(val message: String)

@Serializable
data class NewUser(val name: String, val password: String)

@Serializable
data class UserMod(val name: String? = null, val password: String? = null)

@Serializable
data class Server(val image: String, val description: List<StructuredText>)

@Serializable
data class ServerMod(val description: List<StructuredText>? = null)

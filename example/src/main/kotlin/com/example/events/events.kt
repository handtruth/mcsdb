package com.example.events

import com.example.model.StructuredText
import com.handtruth.mc.types.UUID
import com.handtruth.mcsdb.schema.Fantom
import com.handtruth.mcsdb.schema.Source
import com.handtruth.mcsdb.schema.Target
import com.handtruth.mcsdb.schema.Unique
import com.handtruth.mcsdb.time.DirectEvent
import com.handtruth.mcsdb.time.Event
import com.handtruth.mcsdb.time.EventCompanion
import com.handtruth.mcsdb.time.FactEvent
import com.handtruth.mcsdb.types.Serial

@Unique
@Fantom
interface ActorEvent : Event {
    val actor: Serial

    companion object : EventCompanion()
}

@Unique
interface SystemActorEvent : Event {
    companion object : EventCompanion()
}

data class SystemActorContextEvent(
    override val actor: Serial = Serial.nil,
    override val direction: Boolean
) : @Source ActorEvent, @Source SystemActorEvent, DirectEvent() {
    companion object : EventCompanion()
}

@Unique
interface UserEvent : Event {
    val user: Serial

    companion object : EventCompanion()
}

data class UserContextEvent(
    override val user: Serial = Serial.nil,
    override val actor: Serial,
    override val direction: Boolean
) : @Source UserEvent, ActorEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class UserActorEvent(
    override val user: Serial,
    override val actor: Serial = Serial.nil,
    override val direction: Boolean
) : UserEvent, @Source ActorEvent, DirectEvent() {
    companion object : EventCompanion()
}

@Unique
interface UniqueUserNameEvent : Event {
    val userName: String

    companion object : EventCompanion()
}

data class UserNameEvent(
    override val userName: String,
    override val user: Serial,
    override val direction: Boolean
) : UserEvent, @Source UniqueUserNameEvent, DirectEvent() {
    companion object : EventCompanion()
}

interface LoginMethodEvent : UserEvent, ActorEvent {
    companion object : EventCompanion()
}

data class PasswordEvent(
    val password: String,
    override val user: Serial,
    override val actor: Serial,
    override val direction: Boolean
) : @Source LoginMethodEvent, DirectEvent() {
    companion object : EventCompanion()
}

interface ServerEvent : Event {
    val server: String

    companion object : EventCompanion()
}

data class ServerContextEvent(
    val image: String,
    override val server: String,
    override val actor: Serial,
    override val direction: Boolean
) : @Source ServerEvent, ActorEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class ServerDescriptionEvent(
    val description: List<StructuredText>,
    override val server: String,
    override val direction: Boolean
) : ServerEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class UserRegistrationEvent(
    val userName: String,
    val password: String,
    override val actor: Serial
) : ActorEvent, FactEvent() {
    companion object : EventCompanion()
}

@Unique
interface SessionEvent : @Target ActorEvent {
    val session: UUID

    companion object : EventCompanion()
}

data class SessionContextEvent(
    override val session: UUID = UUID.nil,
    override val actor: Serial,
    override val direction: Boolean
) : @Source SessionEvent, DirectEvent() {
    companion object : EventCompanion()
}

data class AddUserClientActionEvent(
    val userName: String,
    val password: String,
    override val session: UUID,
    override val actor: Serial
) : SessionEvent, FactEvent() {
    companion object : EventCompanion()
}

data class RemoveUserClientActionEvent(
    val user: Serial,
    override val session: UUID,
    override val actor: Serial
) : SessionEvent, FactEvent() {
    companion object : EventCompanion()
}

data class ChangeUserNameClientActionEvent(
    val newName: String,
    override val user: Serial,
    override val session: UUID,
    override val actor: Serial
) : SessionEvent, UserEvent, FactEvent() {
    companion object : EventCompanion()
}

data class ChangePasswordClientActionEvent(
    val newPassword: String,
    override val user: Serial,
    override val session: UUID,
    override val actor: Serial
) : SessionEvent, UserEvent, FactEvent() {
    companion object : EventCompanion()
}

data class CreateServerClientActionEvent(
    val image: String,
    val server: String,
    val description: List<StructuredText>,
    override val session: UUID,
    override val actor: Serial
) : SessionEvent, FactEvent() {
    companion object : EventCompanion()
}

data class DeleteServerClientActionEvent(
    val server: String,
    override val session: UUID,
    override val actor: Serial
) : SessionEvent, FactEvent() {
    companion object : EventCompanion()
}

data class ChangeServerDescriptionEvent(
    val newDescription: List<StructuredText>,
    override val server: String,
    override val session: UUID,
    override val actor: Serial
) : SessionEvent, ServerEvent, FactEvent() {
    companion object : EventCompanion()
}

interface VolumeEvent : Event {
    val volume: String

    companion object : EventCompanion()
}

@Unique
interface ServerVolumeEvent : ServerEvent, @Source VolumeEvent {
    companion object : EventCompanion()
}

data class VolumeContextEvent(
    override val server: String,
    override val volume: String,
    override val direction: Boolean
) : @Source ServerVolumeEvent, DirectEvent() {
    companion object : EventCompanion()
}

package com.example.server

import com.example.events.ActorEvent
import com.example.model.ErrorObject
import com.example.server.routes.root
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import io.ktor.sessions.*
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.get
import org.koin.logger.SLF4JLogger

fun Application.server(testing: Boolean = false) {
    install(ContentNegotiation) {
        json()
    }
    val mcsdbUrl = environment.config.propertyOrNull("mcsdb.uri")?.getString()
        ?: "sqlite:storage.sqlite"
    val configModule = module {
        single { environment.config }
    }
    install(Koin) {
        SLF4JLogger()
        modules(configModule, mainModule)
    }
    install(MCSDB) {
        uri = mcsdbUrl
        setup {
            initializeMCSDb(get(), get(), get())
        }
    }
    install(StatusPages) {
        exception<Throwable> { e ->
            log.error("server error", e)
            call.respond(HttpStatusCode.InternalServerError, ErrorObject("unknown server error"))
        }
        exception<MissingSessionException> {
            call.respond(HttpStatusCode.BadRequest, ErrorObject("session id not provided in request headers"))
        }
        exception<IllegalArgumentException> { e ->
            call.respond(HttpStatusCode.BadRequest, ErrorObject(e.message ?: "bad request"))
        }
    }
    routing { root() }
}

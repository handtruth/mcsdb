package com.example.server

import com.example.events.SessionContextEvent
import com.example.events.SessionEvent
import com.handtruth.mc.types.MalformedUUIDException
import com.handtruth.mc.types.UUID
import com.handtruth.mcsdb.context.listen
import com.handtruth.mcsdb.m
import com.handtruth.mcsdb.time.Time
import io.ktor.application.*
import kotlinx.coroutines.flow.firstOrNull
import java.security.MessageDigest
import java.security.SecureRandom
import java.util.*

private const val saltSize = 16

private val hashAlgorithm = MessageDigest.getInstance("SHA-256")

fun createHash(password: String, salt: ByteArray, saltStart: Int = 0): ByteArray {
    val passwordBytes = password.toByteArray(Charsets.UTF_8)
    val hashInput = concat(passwordBytes, salt, bStart = saltStart, bSize = saltSize)
    return hashAlgorithm.digest(hashInput)
}

fun concat(
    a: ByteArray,
    b: ByteArray,
    aStart: Int = 0,
    aSize: Int = a.size - aStart,
    bStart: Int = 0,
    bSize: Int = b.size - bStart
): ByteArray {
    val result = ByteArray(aSize + bSize)
    System.arraycopy(a, aStart, result, 0, aSize)
    System.arraycopy(b, bStart, result, aSize, bSize)
    return result
}

private val secureRandom = SecureRandom()

fun generateSalt(): ByteArray {
    val result = ByteArray(saltSize)
    secureRandom.nextBytes(result)
    return result
}

private val base64Encoder = Base64.getEncoder()

fun encodeBase64(value: ByteArray): String = base64Encoder.encodeToString(value)

fun encryptPassword(password: String): String {
    val salt = generateSalt()
    val hashedPassword = createHash(password, salt)
    val bytes = concat(hashedPassword, salt)
    return encodeBase64(bytes)
}

private val base64Decoder = Base64.getDecoder()

fun decodeBase64(value: String): ByteArray = base64Decoder.decode(value)

fun checkPassword(password: String, encryptedPassword: String): Boolean {
    val expectedHashedPassword = decodeBase64(encryptedPassword)
    val hashSize = expectedHashedPassword.size - saltSize
    val actualHashedPassword = createHash(password, expectedHashedPassword, hashSize)
    return Arrays.equals(expectedHashedPassword, 0, hashSize, actualHashedPassword, 0, hashSize)
}

fun ApplicationCall.getSessionUUID(): UUID {
    val header = request.headers["Session"] ?: throw MissingSessionException()
    try {
        return UUID.parse(header)
    } catch (e: MalformedUUIDException) {
        throw IllegalArgumentException(e.message)
    }
}

suspend fun ApplicationCall.getSession(): SessionEvent {
    val sessionId = getSessionUUID()
    return Time.listen<SessionContextEvent>(actual = true, stream = false) {
        m(SessionEvent::session) eq sessionId
    }.firstOrNull() ?: throw IllegalArgumentException("session $sessionId not found")
}

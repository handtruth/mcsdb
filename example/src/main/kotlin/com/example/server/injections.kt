package com.example.server

import com.handtruth.docker.DockerClient
import com.handtruth.kommon.LogFactory
import io.ktor.config.*
import io.ktor.http.*
import org.koin.core.qualifier.named
import org.koin.dsl.module

val mainModule = module(createdAtStart = true) {
    single(named("docker-url")) {
        val config: ApplicationConfig = get()
        config.propertyOrNull("example-server.docker-url")?.getString() ?: "http://localhost:2375"
    }

    single { LogFactory("example-server") }

    single { DockerClient(Url(get<String>(named("docker-url"))), get()) }
}

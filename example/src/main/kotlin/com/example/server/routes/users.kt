package com.example.server.routes

import com.example.events.*
import com.example.model.ErrorObject
import com.example.model.NewUser
import com.example.model.User
import com.example.model.UserMod
import com.example.server.getSession
import com.handtruth.mcsdb.FilterBuilderContext.eq
import com.handtruth.mcsdb.context.getContext
import com.handtruth.mcsdb.context.listen
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.m
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.types.Serial
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.toList

private fun ApplicationCall.getUserId(): Serial {
    val id = parameters["id"]?.toLongOrNull() ?: throw IllegalArgumentException("id were not specified")
    return Serial(id)
}

fun Route.users() {
    get {
        call.getSession()
        val users = Time.listen<UserContextEvent>(actual = true, stream = false) { U }
            .toList()
        println(users)
        call.respond(users.map { it.user.toLong() })
    }

    put {
        val session = call.getSession()
        val newUser = call.receive<NewUser>()
        val context = getContext()
        val userEventAsync = async(context) {
            Time.listen<UserNameEvent> { m(UserNameEvent::userName) eq newUser.name }.first()
        }
        Time.raise(
            AddUserClientActionEvent(
                userName = newUser.name,
                password = newUser.password,
                session = session.session,
                actor = session.actor
            )
        )
        val userEvent = userEventAsync.await()
        call.respond(User(userEvent.user.toLong(), userEvent.userName))
    }

    route("{id}") {
        get {
            call.getSession()
            val id = call.getUserId()
            val userName = Time.listen<UserNameEvent>(actual = true, stream = false) {
                m(UserNameEvent::user) eq id
            }.firstOrNull()
            if (userName == null) {
                call.respond(HttpStatusCode.NotFound, ErrorObject("user #$id not found"))
            } else {
                call.respond(User(userName.user.toLong(), userName.userName))
            }
        }

        post {
            val session = call.getSession()
            val id = call.getUserId()
            val user = call.receive<UserMod>()
            if (user.name != null) {
                Time.raise(
                    ChangeUserNameClientActionEvent(
                        newName = user.name,
                        user = id,
                        session = session.session,
                        actor = session.actor
                    )
                )
            }
            if (user.password != null) {
                Time.raise(
                    ChangePasswordClientActionEvent(
                        newPassword = user.password,
                        user = id,
                        session = session.session,
                        actor = session.actor
                    )
                )
            }
            call.respond(ErrorObject("successfully changed user parameters"))
        }

        delete {
            val session = call.getSession()
            val id = call.getUserId()
            coroutineScope {
                val nameAsync = async {
                    Time.listen<UserNameEvent> { m(UserEvent::user) eq id }.first { !it.direction }
                }
                Time.raise(
                    RemoveUserClientActionEvent(user = id, session = session.session, actor = session.actor)
                )
                call.respond(User(id.toLong(), nameAsync.await().userName))
            }
        }
    }
}

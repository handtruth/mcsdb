package com.example.server.routes

import com.example.events.*
import com.example.model.ErrorObject
import com.example.model.Server
import com.example.model.ServerMod
import com.example.server.getSession
import com.handtruth.mcsdb.context.listen
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.m
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.type
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.toList

fun ApplicationCall.getServerName(): String {
    return parameters["server"] ?: throw IllegalArgumentException("missing server name")
}

fun Route.servers() {
    route("{server}") {
        put {
            val session = call.getSession()
            val name = call.getServerName()
            val server = call.receive<Server>()
            val createdServer = Time.raise(
                CreateServerClientActionEvent(
                    image = server.image,
                    server = name,
                    description = server.description,
                    session = session.session,
                    actor = session.actor
                )
            )
            call.respond(Server(image = createdServer.image, description = createdServer.description))
        }

        get {
            val name = call.getServerName()
            val events = Time.listen<ServerEvent>(actual = true, stream = false) {
                (m(ServerEvent::server) eq name) and (type(ServerContextEvent) or type(ServerDescriptionEvent))
            }.toList()
            if (events.size < 2) {
                call.respond(HttpStatusCode.NotFound, ErrorObject("server $name not found"))
            }
            val serverContext = events.first { it is ServerContextEvent } as ServerContextEvent
            val description = events.first { it is ServerDescriptionEvent } as ServerDescriptionEvent
            call.respond(Server(image = serverContext.image, description = description.description))
        }

        post {
            val session = call.getSession()
            val name = call.getServerName()
            val server = call.receive<ServerMod>()
            if (server.description != null) {
                Time.raise(
                    ChangeServerDescriptionEvent(
                        newDescription = server.description,
                        server = name,
                        session = session.session,
                        actor = session.actor
                    )
                )
            }
            call.respond(ErrorObject("server parameters successfully changed"))
        }

        delete {
            val session = call.getSession()
            val name = call.getServerName()
            Time.raise(DeleteServerClientActionEvent(server = name, session = session.session, actor = session.actor))
            call.respond(ErrorObject("successfully removed"))
        }
    }
}

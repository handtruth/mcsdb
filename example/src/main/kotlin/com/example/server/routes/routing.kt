package com.example.server.routes

import com.example.events.*
import com.example.model.ErrorObject
import com.example.server.checkPassword
import com.example.server.getSession
import com.handtruth.mcsdb.FilterBuilderContext.and
import com.handtruth.mcsdb.FilterBuilderContext.eq
import com.handtruth.mcsdb.FilterBuilderContext.or
import com.handtruth.mcsdb.context.listen
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.m
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.type
import com.handtruth.mcsdb.via
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.flow.toList

fun Routing.root() {
    route("/user") {
        users()
    }

    route("/server") {
        servers()
    }

    post("login") {
        val username = call.parameters["username"]
        val password = call.parameters["password"]
        if (username == null || password == null) {
            call.respond(
                HttpStatusCode.BadRequest,
                ErrorObject("username or password were not specified")
            )
        } else {
            val events = Time.listen(actual = true, stream = false) {
                (type(PasswordEvent) or type(UserActorEvent)) and via(UserEvent) {
                    m(UserNameEvent::userName) eq username
                }
            }.toList()
            if (events.size < 2) {
                call.respond(HttpStatusCode.BadRequest, ErrorObject("invalid username or password"))
            } else {
                val actor = events.first { it is UserActorEvent } as UserActorEvent
                val passwordEvent = events.first { it is PasswordEvent } as PasswordEvent
                if (checkPassword(password, passwordEvent.password)) {
                    val session = Time.raise(
                        SessionContextEvent(actor = actor.actor, direction = true)
                    )
                    call.response.header("Session", session.session.toString())
                    call.respond(ErrorObject("successfully logged in"))
                } else {
                    call.respond(HttpStatusCode.BadRequest, ErrorObject("invalid username or password"))
                }
            }
        }
    }

    post("quit") {
        val session = call.getSession()
        Time.raise(SessionContextEvent(session = session.session, actor = session.actor, direction = false))
    }
}

package com.example.server

import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.asStatic
import com.handtruth.mcsdb.context.Context
import com.handtruth.mcsdb.open
import com.handtruth.mcsdb.variability.variabilityQuery
import io.ktor.application.*
import io.ktor.routing.*
import io.ktor.util.*
import io.ktor.util.pipeline.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicBoolean

class MCSDB(val context: Context) {
    private var applied = AtomicBoolean(false)

    class Configuration {
        var uri = "sqlite:storage.sqlite"
        var state = variabilityQuery { default }
        var setupRoutine: suspend CoroutineScope.() -> Unit = { }

        fun setup(block: suspend CoroutineScope.() -> Unit) {
            setupRoutine = block
        }
    }

    // Implements ApplicationFeature as a companion object.
    companion object Feature : ApplicationFeature<ApplicationCallPipeline, Configuration, MCSDB> {
        // Creates a unique key for the feature.
        override val key = AttributeKey<MCSDB>("mcsdb")

        // Code to execute when installing the plugin.
        override fun install(pipeline: ApplicationCallPipeline, configure: Configuration.() -> Unit): MCSDB {

            val configuration = Configuration().apply(configure)

            val context = runBlocking {
                val database = Database.open(configuration.uri).asStatic()
                val state = database.variability.ask(configuration.state).first()
                val context = Context(database, state)
                withContext(context, configuration.setupRoutine)
                context
            }

            val feature = MCSDB(context)

            pipeline.intercept(ApplicationCallPipeline.Call) {
                if (!feature.applied.getAndSet(true)) {
                    application.routing {
                        for (child in children) {
                            walk(child, context)
                        }
                    }
                }
            }

            return feature
        }

        private val field = run {
            val it = Route::class.java.getDeclaredField("handlers")
            check(it.trySetAccessible())
            it
        }

        private fun walk(route: Route, context: Context) {
            @Suppress("UNCHECKED_CAST")
            val handlers = field.get(route) as MutableList<PipelineInterceptor<Unit, ApplicationCall>>
            for (i in handlers.indices) {
                val previous = handlers[i]
                handlers[i] = {
                    withContext(context) {
                        previous(it)
                    }
                }
            }
            for (child in route.children) {
                walk(child, context)
            }
        }
    }
}

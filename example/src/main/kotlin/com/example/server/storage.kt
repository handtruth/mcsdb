package com.example.server

import com.example.events.*
import com.handtruth.docker.DockerClient
import com.handtruth.kommon.LogFactory
import com.handtruth.mcsdb.*
import com.handtruth.mcsdb.context.*
import com.handtruth.mcsdb.schema.Schema
import com.handtruth.mcsdb.space.Space
import com.handtruth.mcsdb.time.*
import io.ktor.config.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.launch
import kotlin.coroutines.EmptyCoroutineContext

suspend fun initializeMCSDb(docker: DockerClient, logFactory: LogFactory, config: ApplicationConfig) {
    val log = logFactory.log("mcsdb")
    Schema.register(
        SystemActorContextEvent,
        UserContextEvent,
        UserActorEvent,
        UserNameEvent,
        PasswordEvent,
        ServerContextEvent,
        ServerDescriptionEvent,
        UserRegistrationEvent,
        SessionContextEvent,
        AddUserClientActionEvent,
        RemoveUserClientActionEvent,
        ChangeUserNameClientActionEvent,
        VolumeContextEvent
    )

    // Добавление пароля пользователю
    Schema.reactAfter<PasswordEvent> { event ->
        if (event.direction) {
            event.copy(password = encryptPassword(event.password))
        } else {
            event
        }
    }

    // Создание/уничтожение контейнера
    Schema.reactAfter<ServerContextEvent> { event ->
        if (event.direction) {
            val message = docker.images.pull(event.image).last()
            log.info { message }
            docker.containers.create("example-${event.server}") {
                image = event.image
            }
        } else {
            docker.containers.remove("example-${event.server}")
        }
        event
    }

    // Регистрация пользователя актором
    Schema.reactAfter<UserRegistrationEvent> { event ->
        val userContext = Time.raise(UserContextEvent(actor = event.actor, direction = true))
        val userName = Time.raise(
            UserNameEvent(userName = event.userName, user = userContext.user, direction = true)
        )
        Time.raise(UserActorEvent(user = userContext.user, direction = true))
        val passwordMethod = Time.raise(
            PasswordEvent(password = event.password, user = userContext.user, actor = event.actor, direction = true)
        )
        event.copy(userName = userName.userName, password = passwordMethod.password)
    }

    // Регистрация пользователя сессией
    Schema.reactAfter<AddUserClientActionEvent> { event ->
        val registration = Time.raise(
            UserRegistrationEvent(userName = event.userName, password = event.password, actor = event.actor)
        )
        event.copy(userName = registration.userName, password = registration.password)
    }

    // Удаление пользователя сессией
    Schema.reactAfter<RemoveUserClientActionEvent> { event ->
        Time.raise(UserContextEvent(user = event.user, actor = event.actor, direction = false))
        event
    }

    // Изменение имени пользователя сессией
    Schema.reactAfter<ChangeUserNameClientActionEvent> { event ->
        val currentUserName = Time.listen<UserNameEvent>(actual = true, stream = false) {
            m(UserEvent::user) eq event.user
        }.first()
        Time.raise(cancel(currentUserName))
        val newUserName = Time.raise(currentUserName.copy(userName = event.newName))
        event.copy(newName = newUserName.userName)
    }

    // Изменение пароля пользователя сессией
    Schema.reactAfter<ChangePasswordClientActionEvent> { event ->
        val currentUserPassword = Time.listen<PasswordEvent>(actual = true, stream = false) {
            m(UserEvent::user) eq event.user
        }.first()
        Time.raise(cancel(currentUserPassword))
        val newPasswordEvent = Time.raise(currentUserPassword.copy(password = event.newPassword))
        event.copy(newPassword = newPasswordEvent.password)
    }

    // Создание контейнера и атрибутов сессией
    Schema.reactAfter<CreateServerClientActionEvent> { event ->
        val serverEvent = Time.raise(
            ServerContextEvent(image = event.image, server = event.server, actor = event.actor, direction = true)
        )
        val description = Time.raise(
            ServerDescriptionEvent(description = event.description, server = serverEvent.server, direction = true)
        )
        event.copy(image = serverEvent.image, server = serverEvent.server, description = description.description)
    }

    // Изменение описания контейнера сессией
    Schema.reactAfter<ChangeServerDescriptionEvent> { event ->
        val serverDescription = Time.listen<ServerDescriptionEvent>(actual = true, stream = false) {
            m(ServerEvent::server) eq event.server
        }.first()
        Time.raise(cancel(serverDescription))
        Time.raise(serverDescription.copy(description = event.newDescription))
        event
    }

    // Создание контейнера сессией
    Schema.reactAfter<DeleteServerClientActionEvent> { event ->
        val server = Time.listen<ServerContextEvent>(actual = true, stream = false) {
            m(ServerEvent::server) eq event.server
        }.first()
        Time.raise(server.copy(actor = event.actor, direction = false))
        event
    }

    // Получение системного актора
    val system = try {
        Time.raise(SystemActorContextEvent(direction = true))
    } catch (e: TimeException) {
        log.info { "system actor created" }
        Time.ask { direction eq true and success }.first() as SystemActorContextEvent
    }

    val adminName = config.propertyOrNull("example-server.admin.name")?.getString() ?: "admin"
    val adminPassword = config.propertyOrNull("example-server.admin.password")?.getString() ?: "password"

    // Создание администратора системы
    val adminSet = Space.ask { select { m(UserNameEvent::userName) eq adminName } }
    if (adminSet.isEmpty()) {
        Time.raise(UserRegistrationEvent(userName = adminName, password = adminPassword, actor = system.actor))
    }

    // Remove previous active sessions
    Time.listen<SessionContextEvent>(actual = true, stream = false) { U }.collect { event ->
        Time.raise(cancel(event))
    }

    CoroutineScope(EmptyCoroutineContext).launch {
        // Добавление атрибута серверу сторонним компонентом приложения
        Time.listen<ServerContextEvent> { U }.collect { event ->
            if (event.direction && infoOf(event).succeeded) {
                val image = docker.images.find(event.image)?.inspect()
                if (image != null) {
                    for (volume in image.config.volumes.keys) {
                        Time.raise(VolumeContextEvent(server = event.server, volume = volume, direction = true))
                    }
                }
            }
        }
    }
}

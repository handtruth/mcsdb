package com.example.test

import com.example.events.SystemActorContextEvent
import com.handtruth.mcsdb.Database
import com.handtruth.mcsdb.asStatic
import com.handtruth.mcsdb.context.Context
import com.handtruth.mcsdb.context.raise
import com.handtruth.mcsdb.open
import com.handtruth.mcsdb.time.Time
import com.handtruth.mcsdb.time.TimeException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class SystemUniquenessTest {

    @Test
    fun systemUniq() = runBlocking<Unit> {
        val database = Database.open("sqlite:file:test?mode=memory").asStatic()
        withContext(Context(database)) {
            Time.raise(SystemActorContextEvent(direction = true))
            val error = assertFailsWith<TimeException> {
                Time.raise(SystemActorContextEvent(direction = true))
            }
            assertEquals(TimeException.Codes.ExplicitForeignKey, error.code)
        }
    }
}

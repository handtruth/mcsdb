package com.example.test

import com.example.server.*
import org.junit.Test
import kotlin.test.assertTrue

class PasswordHashTest {

    @Test
    fun checkPasswordTest() {
        val password = "1234565789"

        val encryptedPassword = encryptPassword(password)
        assertTrue { checkPassword(password, encryptedPassword) }
    }
}

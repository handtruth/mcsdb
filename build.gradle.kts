@file:Suppress("UNUSED_VARIABLE")

import org.jetbrains.dokka.gradle.DokkaMultiModuleTask
import org.jetbrains.dokka.gradle.DokkaPlugin
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinMultiplatformPluginWrapper
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jlleitschuh.gradle.ktlint.KtlintExtension
import org.jlleitschuh.gradle.ktlint.KtlintPlugin
import org.jlleitschuh.gradle.ktlint.reporter.ReporterType

plugins {
    base
    id("com.gladed.androidgitversion")
    id("org.jetbrains.dokka")
    kotlin("multiplatform") apply false
    id("org.jlleitschuh.gradle.ktlint") apply false
    id("info.solidsoft.pitest") apply false
    jacoco
}

androidGitVersion {
    prefix = "v"
}

val groupString = "com.handtruth.mcsdb"
val versionString: String = androidGitVersion.name()

allprojects {
    group = groupString
    version = versionString

    repositories {
        maven("https://mvn.handtruth.com")
        maven("https://kotlin.bintray.com/kotlinx/")
        jcenter()
    }
}

val kotlinProjects: List<String> by extra

val platformVersion: String by project

val coverageSourceDirsNames = arrayOf(
    "src/commonMain/kotlin/",
    "src/jvmMain/kotlin/"
)

fun KotlinSourceSet.collectSources(): Iterable<File> {
    return kotlin.srcDirs.filter { it.exists() } + dependsOn.flatMap { it.collectSources() }
}

fun KotlinSourceSet.collectSourceFiles(): ConfigurableFileCollection {
    return files(collectSources().map { fileTree(it) })
}

fun Project.kotlinProject() {
    apply<KotlinMultiplatformPluginWrapper>()
    apply<JacocoPlugin>()
    apply<MavenPublishPlugin>()
    apply<DokkaPlugin>()
    apply<KtlintPlugin>()

    configure<PublishingExtension> {
        if (!System.getenv("CI").isNullOrEmpty()) repositories {
            maven {
                val ciServerHost = System.getenv("CI_SERVER_HOST")!!
                val ciServer = if (ciServerHost == "git.handtruth.com") "http://gitlab" else "http://gitlab.com"
                val projectId = System.getenv("CI_PROJECT_ID")!!
                val token = System.getenv("CI_JOB_TOKEN")!!
                url = uri("$ciServer/api/v4/projects/$projectId/packages/maven")
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = token
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }

    lateinit var jvmMainSourceSet: KotlinSourceSet

    configure<KotlinMultiplatformExtension> {
        jvm {
            withJava()
            compilations.all {
                kotlinOptions {
                    jvmTarget = "1.8"
                }
            }
        }
        sourceSets {
            all {
                with(languageSettings) {
                    enableLanguageFeature("InlineClasses")
                    useExperimentalAnnotation("com.handtruth.mc.paket.ExperimentalPaketApi")
                    useExperimentalAnnotation("com.handtruth.mcsdb.InternalMcsdbApi")
                    useExperimentalAnnotation("kotlin.ExperimentalUnsignedTypes")
                    useExperimentalAnnotation("kotlin.contracts.ExperimentalContracts")
                    useExperimentalAnnotation("kotlin.RequiresOptIn")
                    useExperimentalAnnotation("kotlinx.serialization.ExperimentalSerializationApi")
                    useExperimentalAnnotation("kotlin.time.ExperimentalTime")
                    useExperimentalAnnotation("kotlin.ExperimentalStdlibApi")
                    useExperimentalAnnotation("kotlinx.coroutines.ExperimentalCoroutinesApi")
                }
                dependencies {
                    val handtruthPlatform = dependencies.platform("com.handtruth.internal:platform:$platformVersion")
                    implementation(handtruthPlatform)
                    api(handtruthPlatform)
                    runtimeOnly(handtruthPlatform)
                }
            }
            val commonMain by getting {
                dependencies {
                    implementation(kotlin("stdlib"))
                }
            }
            val commonTest by getting {
                dependencies {
                    implementation(kotlin("test-common"))
                    implementation(kotlin("test-annotations-common"))
                }
            }
            val jvmMain by getting
            jvmMainSourceSet = jvmMain
            val jvmTest by getting {
                dependencies {
                    implementation(kotlin("test-junit5"))
                    runtimeOnly("org.junit.jupiter:junit-jupiter-engine")
                }
            }
        }
    }

    configure<KtlintExtension> {
        version.set("0.41.0")
        verbose.set(true)
        outputToConsole.set(true)
        enableExperimentalRules.set(true)
        outputColorName.set("RED")
        disabledRules.add("no-wildcard-imports")
        disabledRules.add("experimental:argument-list-wrapping")

        reporters {
            reporter(ReporterType.PLAIN)
            reporter(ReporterType.CHECKSTYLE)
        }
    }

    configure<JacocoPluginExtension> {
        toolVersion = "0.8.6"
    }

    val jvmTest by tasks.getting(Test::class) {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }
    }

    val testCoverageReport by tasks.creating(JacocoReport::class) {
        dependsOn(jvmTest)
        group = "Reporting"
        description = "Generate Jacoco coverage reports."

        val classFiles = Callable {
            File(buildDir, "classes/kotlin/jvm/main")
                .walkBottomUp()
                .toList()
        }

        val sourceDirs = files(*coverageSourceDirsNames)

        classDirectories.setFrom(classFiles)
        sourceDirectories.setFrom(jvmMainSourceSet.collectSources())

        executionData.setFrom(files("$buildDir/jacoco/jvmTest.exec"))
    }
}

configure<JacocoPluginExtension> {
    toolVersion = "0.8.6"
}

val thisProjects = kotlinProjects.map { project(":$name-$it") }

thisProjects.forEach {
    it.kotlinProject()
}

tasks {
    val mergeTestCoverageReport by creating(JacocoMerge::class) {
        group = "Reporting"
        val pTasks = Callable { thisProjects.map { it.tasks["jvmTest"] } }
        dependsOn(pTasks)
        executionData(pTasks)
    }
    val rootTestCoverageReport by creating(JacocoReport::class) {
        dependsOn(mergeTestCoverageReport)
        group = "Reporting"
        description = "Generate Jacoco coverage reports."
        val coverageSourceDirs = thisProjects.map {
            it.tasks.getByName<JacocoReport>("testCoverageReport").sourceDirectories
        }

        val classFiles = Callable {
            thisProjects.map {
                it.tasks.getByName<JacocoReport>("testCoverageReport").classDirectories
            }
        }

        classDirectories.setFrom(classFiles)
        sourceDirectories.setFrom(coverageSourceDirs)

        executionData.setFrom(mergeTestCoverageReport)

        reports {
            xml.isEnabled = true
            html.isEnabled = true
        }
    }
    val dokkaHtmlMultiModule by getting(DokkaMultiModuleTask::class)
    val pagesDest = File(projectDir, "public")
    val gitlabPagesCreateDocs by creating(Copy::class) {
        group = "Documentation"
        dependsOn(dokkaHtmlMultiModule)
        from(dokkaHtmlMultiModule)
        into(File(pagesDest, "docs"))
    }
    val gitlabPagesCreateCoverage by creating(Copy::class) {
        group = "Reporting"
        dependsOn(rootTestCoverageReport)
        from(rootTestCoverageReport)
        into(File(pagesDest, "coverage"))
    }
    val gitlabPagesCreate by creating(Copy::class) {
        group = "Reporting"
        dependsOn(gitlabPagesCreateDocs, gitlabPagesCreateCoverage)
        File(projectDir, "pages").listFiles()!!.forEach {
            from(it)
        }
        destinationDir = pagesDest
    }
    val gitlabPagesClear by creating(Delete::class) {
        delete = setOf(pagesDest)
    }
    val clean by getting {
        dependsOn(gitlabPagesClear)
    }
}

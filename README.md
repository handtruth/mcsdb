# Mizen Constellation System Data Base

> NOTE:
> MCSDB is working of course, but you should treat it as an experimental project.

This is an event-driven framework for state controlling using causal-relationship.
This is all you wanted so badly but was afraid to ask or implement by your self.

## Installation

Add this in your Gradle script:

```kotlin

plugins {
    kotlin("jvm")
}

repositories {
    mavenCentral()
    maven("https://mvn.handtruth.com") // MCSDB will be moved "eventually" (pan intended)
}

depemdencies {
    val mcsdbVersion = "..."
    implementation(platform("com.handtruth.mcsdb:mcsdb-bom:$mcsdbVersion"))

    implementation("com.handtruth.mcsdb:mcsdb-static-api")
    runtimeOnly("com.handtruth.mcsdb:mcsdb-embedded")
}
```

## Using

I'm not in the mood to describe this now, but I will *eventually*.

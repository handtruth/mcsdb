pluginManagement {
    repositories {
        gradlePluginPortal()
    }
    val kotlinVersion: String by settings
    val atomicfuVersion: String by settings
    resolutionStrategy {
        eachPlugin {
            when {
                requested.id.id.startsWith("org.jetbrains.kotlin") -> useVersion(kotlinVersion)
                requested.id.id == "kotlinx-atomicfu" ->
                    useModule("org.jetbrains.kotlinx:atomicfu-gradle-plugin:$atomicfuVersion")
            }
        }
    }
    val gitVersionPlugin: String by settings
    val dokkaVersion: String by settings
    val ktlintVersion: String by settings
    val pitestPluginVersion: String by settings
    plugins {
        id("com.gladed.androidgitversion") version gitVersionPlugin
        id("org.jetbrains.dokka") version dokkaVersion
        id("org.jlleitschuh.gradle.ktlint") version ktlintVersion
        id("info.solidsoft.pitest") version pitestPluginVersion
    }
}

rootProject.name = "mcsdb"

val kotlinProjects = listOf(
    "base",
    "embedded",
    "storage",
    "encoding",
    //"orm",
    //"static-orm",
    //"protocol",
    "api",
    "static-api",
    //"server",
    "test-schema"
)

fun subproject(name: String) {
    include(":${rootProject.name}-$name")
    project(":${rootProject.name}-$name").projectDir = file("subprojects/$name")
}

subproject("bom")

val ci: String? by settings

if (ci == null) {
    subproject("viewer")
}
kotlinProjects.forEach { subproject(it) }

gradle.allprojects {
    extra["kotlinProjects"] = kotlinProjects
}
